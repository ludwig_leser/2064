const fs = require('fs');

const readChapters = async directory => {
    return new Promise((resolve, reject) => {
        fs.readdir(directory, (err, files) => {
            if (err) {
                reject(err);
                return;
            }

            files = files.filter(file => {
                const page = parseInt(file);
                if (!page) return false;
                return true;
            }).sort();

            resolve(files);
        });
    });
}

module.exports = readChapters;