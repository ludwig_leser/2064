## Ein Albatros
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Vor einer Berghütte</span> standen zwei Gelände-Motorräder.
Davor saß Roger, ein braun gebrannter Mitvierziger mit Bart, auf einer Bank an eine Hauswand gelehnt, die Hände hinter dem Kopf verschränkt, Zigarette im Mund, Beine lang ausgestreckt.
Im Haus konnte man Lasse an seinem Laptop sehen.
Er trug Kopfhörer und hatte einen großen Joystick in der Hand.
Vor ihm auf dem Bildschirm sah man ein Steuerungsprogramm für einen fliegenden Kampfroboter.
Es war aktiv.
Der Kampfroboter flog über eine Steppen-Landschaft.
Vor ihm kam ein weiterer Kampfroboter ins Blickfeld.

<div class="terminal">
Lasse: „Sigur?
Ich bin da.
Hinten rechts hinter dir.“
<br /><br />
Sigur: „Geil!
Das Team ist wieder zusammen.
Fühlt sich gut an.
Du weißt, das Letzte, was wir so zusammen gemacht haben, war den Fünfstern vom Himmel zu holen.
Der letzte Looping, wir beide zusammen, weißt du noch?“
<br /><br />
Lasse: „Klar, weiß ich das noch.
Und ich komme gerne auch mal, um dich raus zu boxen, wenn du Schwierigkeiten hast.
Wo sind die Hubschrauber?“
<br /><br />
Sigur: „Die habe ich schon erledigt.
Sie liegen irgendwo im Wüstensand, vielleicht 15 km westlich von uns.
Es war ganz einfach.
Die XJ-11 hat wirklich einiges mehr an Kampfkraft als die Vorgänger, und ist super beweglich.
Ich habe mich zu keinem Zeitpunkt in Gefahr gefühlt.
Deswegen habe ich nicht auf dich gewartet.
Ich habe sie alle drei innerhalb von 30 Sekunden runtergeholt.
Es war echt easy.
Allerdings habe ich natürlich jetzt keine Hellfire-Rakete mehr.“
<br /><br />
Lasse: „SHIT!
Sigur!
Wir wollten sie verjagen und dann verschwinden.
Nicht gleich vom Himmel holen.
Da sitzen Menschen drin.
Du kannst nicht einfach auf alles schießen, was dir in die Quere kommt.
Er gibt Regeln.
Es gilt: Nur zur Abwendung unmittelbarer Lebensgefahr.
Oder besser noch die Cypherpunk-Regel: Nur wenn der oder die andere ein Leben unmittelbar bedroht, und dann auch nur sie, nicht noch ein paar Leute um sie herum.“
<br /><br />
Sigur: „Ich bin kein Cypherpunk.
Und trotzdem: Ist das keine unmittelbare Gefahr?
Drei Apache-Hubschrauber, mit wärmegesteuerten Lenk-Raketen und 30mm-Bordkanonen.
Das sind keine Spielzeuge.
Ich war in Gefahr.“
<br /><br />
Lasse: „Sie haben gegen dich keine Chance.
Da sitzen Menschen drin, die müssen auch noch auf ihr Leben achten.
Du verlierst im schlimmsten Fall einen Kampfroboter.“
<br /><br />
Sigur: „Hej, das sind Leute, die für ein totalitäres Regime arbeiten, die müssen damit rechnen, dass etwas als Antwort zurück kommt, bei all dem, was sie machen.
Ich habe eine Blausäurekapsel im Bauch.
Die würden die jetzt aktivieren, ohne mit der Wimper zu zucken, wenn sie könnten.
Die machen auf jeden Fall schlimmere Sachen als ich.
Und du hast so ein Ding im Hirn, wovon du noch nicht einmal weißt, was es genau ist.“
<br /><br />
Lasse: „Du kannst nicht sagen, weil die das machen, machen wir das auch.
Wenn jemand einen Freund von dir foltert, dann gehst du ja auch nicht hin und folterst einen seiner Freunde.“
<br /><br />
Sigur: „Arschloch.“
<br /><br />
Lasse: „Okay, sorry, ich ziehe das Beispiel zurück.“
<br /><br />
Sigur: „SCHEISSE!“
<br /><br />
Lasse: „Nee.
Ist gut.
Es war vielleicht zu drastisch.
Aber andererseits, du hast Menschen umgebracht ...“
<br /><br />
Sigur hektisch: „Nee, nee.
Sie haben Großalarm ausgelöst!
Das ganze Kampfrobotergeschwader rückt aus.
Shit.
Alle 15 ... 20 ... 24 ... 27 gehen gerade in die Luft ...
und nehmen Kurs auf uns.
Shit.
Shit.
Wir sind aufgeflogen.
Sie wissen, wo wir sind.“
<br /><br />
Lasse: „Was machen wir jetzt?
Wir hauen die Dinger jetzt auf den Boden!“
<br /><br />
Sigur: „Geh in Multimode. Schnell!“
<br /><br />
Lasse: „Multimode?“
<br /><br />
Sigur: „mm --activate.
mm --flymode albatross.“
<br /><br />
Lasse: „Okay, habe ich gemacht.
Was ist das?“
<br /><br />
Sigur: „Ich schicke dir meinen fliegenden Kampfroboter rüber.
Er gliedert sich im Albatros-Modus automatisch in Keilform hinter dir ein.
Das sind experimentelle Funktionen für Formationsflüge und Formationsangriffe.“
Sigurs Kampfroboter schloss auf Lasses auf.
<br /><br />
Lasse: „Und was machst du?“
<br /><br />
Sigur: „Ich kümmere mich um die anderen ...“
<br /><br />
Lasse: „WAS?
Lass das!
Lass uns die Dinger auf den Boden hauen und dann weiterziehen.
Sigur!“
<br /><br />
Sigur: „Kleinen Moment.“
</div>

Lasse hört über seinen Kopfhörer hektische Bewegungen von Sigur, Knopf- und Tastendrücke.
Dann ein Tastaturgewitter.
Ab und zu ein Fluch.

<div class="terminal">
Lasse: „Sigur?
... Sigur?
... Was machst du?“
</div>
Sigur reagierte nicht.

<div class="terminal">
Lasse: „Sigur, wir haben einen Auftrag.
Wir sollen die Machenschaften des LBI ans Licht bringen, nicht in einen Krieg mit ihm ziehen.“
<br /><br />
Lasse: „Ich fliege mit meinen beiden Kampfrobotern jetzt in die Wüste und schmeiße sie auf den Boden.“
</div>

Lasse drehte mit beiden Kampfrobotern ab.

<div class="terminal">
Sigur: „Warte kurz.
Ich bin gleich fertig.“
<br /><br />
Lasse: „Fertig mit was?“
<br /><br />
Sigur: „So, jetzt habe ich sie!“
<br /><br />
Lasse: „Du hast sie alle vom Himmel geholt?“
<br /><br />
Sigur: „Nee.“
</div>
Er lachte.

<div class="terminal">
„Nee, sie habe sie alle zu mir geholt, alle!
Alle 27.
Das ist so geil, wenn du mit Software umgehst, die nicht Open-Source ist.
Das sind manchmal so billige Fehler drin, über die du das Programm komplett kontrollieren kannst.
Die sind selbst schuld.
Wenn die Software Open-Source wäre, dann wäre das nicht passiert.
Ich hab einen nach dem anderen abgepflückt, ganz automatisch, und bin jetzt im Albatros-Formationsflug auf dem Weg zu dir.
27 Kampfroboter, in Keilform, vorne mein Leit-Albatros.
Es funktioniert super.
Geil, was die da eingebaut haben.“
<br /><br />
Lasse: „BIST DU VERRÜCKT GEWORDEN?“
<br /><br />
Sigur: „Hej, was ist los mit dir?
Ich habe ein paar Kampfroboter ausgeliehen.
Das war für dich bisher nie ein Problem.“
<br /><br />
Lasse: „Aber 27 auf einmal!
Ein ganzes LBI-Geschwader.
Jetzt sehen wir gleich ein paar F22 aufsteigen.“
<br /><br />
Sigur: „Kein Problem.
Es gibt einen F22-Fighting-Mode.
Ich kann die Kampfroboter wie einen Haufen Hornissen über eine F22 herfallen lassen, oder über zwei oder drei gleichzeitig.
Erst bei vier bräuchte ich mehr Kampfroboter.
Ich habe die Simulationen gesehen.
Das ist so geil gemacht.“
<br /><br />
Lasse: „Du bist total verrückt!“
<br /><br />
Sigur: „Hej, Lasse.
Krieg dich ein.
Ich bin nur dabei, unsere Mission zu verstehen.
Die Weltöffentlichkeit muss glaubwürdig von uns erfahren, dass es das LBI gibt.
Wie wollen wir denn für genügend Aufmerksamkeit und genügend Glaubwürdigkeit sorgen, wenn wir nicht einen Riesenzauber veranstalten?
Wir können nicht mit großen Zeitungen kooperieren, wie WikiLeaks 2010, oder wie Laura Poitras einen Film drehen, der dann einen Oscar bekommt.
Wir haben 2028, da müssen wir andere Geschütze auffahren.
Und jetzt haben wir gute Geschütze.
Super Geschütze.
Mit denen können wir etwas machen, das auffällt.
29 Kampfroboter, voll bewaffnet, die es eigentlich gar nicht geben dürfte und die auch in No-Fly-Zonen fliegen können.“
</div>

Lasse schwieg.

<div class="terminal">
Sigur: „Kommst du mit deinen beiden zu mir?
Wir können jeden Kampfroboter gebrauchen.
Ich habe einen Plan.“
</div>

Lasse stand auf, legte sein Headset auf den Tisch und ging aus der Hütte.

<div class="terminal">
Sigur: „Lasse?
Bist du da?
Lasse?“
</div>
