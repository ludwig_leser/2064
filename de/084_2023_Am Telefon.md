## Am Telefon
<div class="_202x">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Luigi an einem Telefon</span> in einem Internet-Café in Lecco am Garda-See, etwa zwei Autostunden von Luino entfernt.

Luigi: „_Sie_ schicken jemanden zu uns, der in der Öffentlichkeit steht, jemand Bekanntes.
Am besten mit Kamerateam.
Nein, auf jeden Fall mit Kamerateam.
Flughafen Bozen.
Morgen früh um halb 11 ... Ja, das ist notwendig ...


Amelie? Out of Berlin? Ja, das wäre super! 


Sie wird da sein, auf jeden Fall, darauf können Sie sich verlassen ... rufen Sie bei der Courage Foundation an und lassen Sie sich eine Bestätigung geben, dass das hier echt ist.
Die sind bei Ihnen in Berlin gleich um die Ecke ... ja, ich komme auch mit.
Sonst niemand ... Bis morgen.“
