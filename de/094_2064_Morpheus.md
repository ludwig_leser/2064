## Morpheus
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Morpheus schaute erschroken und breitete die Arme aus.</span>
Marlene und Lilly schauten ihn an.

Morpheus: „Ich ... ähm ... ich ... ähm ... Ich kann das erklären.“

Lilly: „Du kannst erklären, dass die Akademie mit einem von deinen Admin-Schlüsseln angegriffen wird?
Du weißt, was das heißt!
Jedes dieser Programme kann unendlich viele Dinge auf unseren Systemen hier machen.
14 Millionen kleine Admins.
Wie willst du das erklären?“

Marlene: „Ja, wie?“

Morpheus setzte sich auf einen der Stehhocker und schaute zu Boden.
Ohne aufzublicken, meinte er: „Sie nutzen die Admin-Rechte ja kaum aus.“

Lilly schnaubte: „DAS IST TOTAL EGAL!“

Marlene: „Wenn so ein Schlüssel in die falschen Hände kommt ...“

Lilly: „Wie kannst du überhaupt einen Admin-Schlüssel nach außen geben?
Das geht doch gar nicht.
Fuck!“

Morpheus atmete hörbar aus.

Marlene: „Wir müssen jetzt alle deine Zugänge sperren.
Zumindest bis das hier geklärt ist.
Die Akademie-Verwalterin muss jetzt herausfinden, was wir weiter machen.
Du kannst wegen so etwas dein Arbeitsrecht als Computerverwalter verlieren.“

Morpheus biss auf die Zähne und kniff die Augen zusammen.

Lilly: „Ich hoffe, dass du es verlierst.
Du kannst einen Admin-Schlüssel nicht rausgeben.“

Marlene schaute Morpheus an: „Du hast gesagt, dass du das erklären kannst.
Meine Güte!
So ein Risiko.
Warum?
Wir hätten das doch irgendwann rausgefunden.
Oder wolltest du die Akademie zerstören?“

Morpheus laut: „NEIN! Das wollte ich nicht.
Gar nicht.“ 

Marlene: „Was wolltest du dann?“

Lilly: „Du kannst das jetzt ruhig sagen.
Ich schaue mir sowieso alles an, was du hier gemacht hast.
Und wenn das Monate dauert.“

Morpheus: „Du findest das nicht in den Logs.
Auch nicht in den Journalen.“

Er atmete laut aus und zeigte dann auf einen Laptop, der neben seinen Monitoren stand.

Lilly: „Dein Tails-Laptop.
Fuck.
Wo ist der Stick?“

Marlene: „Und das Passwort!“

Morpheus schüttelte den Kopf.

Marlene: „Morpheus!“

Lilly zu Marlene: „Du kannst nicht das Passwort von ihm verlangen.
Das ist seins.“

Marlene: „Er verliert sein Arbeitsrecht, wenn er nicht alles zeigt, was passiert ist.“

Morpheus: „Damit hatte ich gerechnet.“

Lilly und Marlene schauten ihn fassungslos an.

Marlene: „Du riskierst dein Arbeitsrecht auf einem Gebiet, auf dem du sichtbar Talent hast?
Das ist verrückt.
Für was?“

Lilly: „Für was?“

Marlene: „Die Cypherpunk-Akademie ist ein Platz, wo junge Menschen lernen, was sie suchen.
Sie sind gerne da.
Sie lernen programmieren und hacken.
Sie lernen die Grundgedankenfehler unserer Kultur kennen.
Sie lernen mit ihren Gefühlen umzugehen.
Sie lernen Meditieren.
Sie lernen Mut und Spontaneität und Vertrauen und Hilfsbereitschaft.
Warum willst du das zerstören?
Warum willst du ...“

Morpheus: „Das ist doch nur die Außenseite!
Das ist so wie ihr die Akademie darstellt.
Deswegen wollen alle Jugendlichen da hin.
Aber so ist es nicht!
Innendrin ist das ganz anders.
Vielleicht seht ihr das nicht.
Vielleicht seid ihr zu nah dran.
Aber es gibt einige Leute, die das sehen.
Die sehen, was die Akademie mit den Jugendlichen macht.
Manchmal sogar mit Kindern.
Auch mit meinem ...“

Marlene: „Rebecca?
Was ist ihr passiert?“

Lilly: „Und was sehe ich nicht?
Wo macht irgendwer etwas mit mir, was ich nicht will?
Wie krass ist denn die Verschwörungstheorie?“

Morpheus: „Ja, meine Rebecca.
Sie hat sich total verändert, seid sie in die Akademie will.
Sie ist noch gar nicht drin, aber kurz davor.“

Lilly: Und das hast du als Admin in ihrem Konto nachgeschaut ...
Du bist als Admin mit persönlichem Interesse in ein Konto gegangen.
Arschloch!“

Morpheus: „Bin ich nicht.
Sie hat mir gesagt, dass sie den Ballon gesehen hat.
Ich gehe nicht in private Konten!“

Lilly schaute ihn mit offenem Mund an.

Marlene: „Du gehst nicht in private Konten.
Was war dann das hier?“

Morpheus: „Das ist nicht persönlich, was ich gemacht habe.
Die Akademie verdreht den Jugendlichen den Kopf.
Sie werden da zu früh in etwas hineingeführt.
Sie müssen selber entscheiden, ob sie so denken wollen, wie die Akadmie das vorgibt.
Das ist wie in den Militär-Akademien am Anfang des Jahrhunderts.
Eigenes Denken?
Nein, danke!
So denken wie die Akademie das will.
Zum Beispiel bei den kulturellen Gedankenfehlern.
Rebecca sucht sie überall im Spiel und wenn sie einen Hinweis findet, dann probiert sie das aus ...“

Lilly kochte innerlich: „Fuck!
Ich habe kein eigenes Denken?
Du kennst mich nicht einmal und sagst das.
Ist das <em>dein</em> eigenes Denken?“

Marlene legte Lilly die Hand auf die Schulter: „Du hast Wut.“

Lilly nickte, drehte sich um und machte ein paar Schritte zur Wand.
Sie blieb dort stehen und schloss die Augen.

Marlene: „Welchen Gedanken meinst du?“

Morpheus schaute sie erstaunt an.

Morpheus: „Da gibt es einige gefährliche.
Aber der, dass man Menschen nicht helfen soll, die unterdrückt werden, dass war ganz schön schwierig zu Hause mit Rebecca und ihrer Mutter.
Aber das ist nur ein Beispiel von vielen.“

Marlene: „Du meinst den Gedanken, dass ich ein Recht auf Hilfe habe, wenn mich jemand unterdrückt?
Und dass man stattdessen denken kann, dass der andere, der einen unterdrückt Hilfe braucht, weil er er es nicht schafft Mensch zu sein.
Und das ist eines der schlimmsten Dinge, die einem im Leben passieren können, dass man den Kontakt dazu verliert, Mensch zu sein und den anderen als Menschen zu sehen.“

Morpheus: „Ihr das drückt immer so aus, dass man meint, das würde schon stimmen.
Fakt ist, dass Rebecca zu ihrer Mutter immer wieder gesagt hat, dass sie sich als Opfer sieht und gar keines ist.
Aber wenn du die Geschichte meiner Frau kennst ...
Meine Güte!
So will niemand aufwachsen.
Niemals.
Sie ist danach in eine Depression gerutscht.
Es ist ihre einzige Tochter, die das sagt.“

Marlene: „Und Rebecca ist Schuld, dass das passiert ist, weil sie als 13-Jährige so etwas sagt?“

Morpheus: „Es geht doch nicht um Schuld!
Ich weiß schon, du willst mir sagen, dass meine Frau als Erwachsene selbst dafür verantwortlich ist, wie sie auf das reagiert, was andere sagen.
Scheiße!
Depression ist eine Krankheit.
Wenn Rebecca das mit dem Opfer nicht gesagt hätte, immer wieder, dann wäre die Depression nicht wieder hochgekommen.
Das ist ein Fakt!
Und das macht sie, weil ihr ihr die Gedanken umdreht.“

Marlene: „Und deswegen wolltest du die Cypherpunk-Akademie lahmlegen?“

Lilly drehte sich von der Wand zu den beiden und sagte trocken: „Nein, er wollte die kulturellen Gedankenfehler leaken.
Er wollte sie hacken und veröffentlichen.
Dass sie alle sehen können, nicht nur die, die das Spiel spielen.“

Morpheus: „Ja.
Genau!
Ihr tut immer so scheinheilig, dass alles offen sein soll.
Spätestens nach 6 Wochen ...
Und dann macht ihr diese Scheiße mit den Gedankenfehlern.
Alle suchen wie wild danach.
Keiner weiß, was da alles dabei ist.
Ich bekomme nur zufällig von einigen über Rebecca mit und einer davon ist schon so verdreht, dass meine Frau davon krank wird.
Die müssen alle raus, an die Öffentlichkeit!
Alle haben ein Recht, sie zu sehen und sich selbst eine Meinung zu bilden.
Oder etwa nicht?“

Lilly schloss ihre Augen und schüttelte den Kopf.

Lilly leise: „Und weil wir eine Whistleblower-Amnestie haben, hast du gedacht, dass du da einfach so wieder rauskommst.“

Marlene: „Morpheus!
Was du willst geht gar nicht.
Du kannst die Gedankenfehler der Akademie nicht bekommen.“

Morpheus: „Genau das hat mir die Akademie-Verwaltung auch gesagt, als ich sie vor einigen Wochen sehen wollte.
Du bekommst sie nur von Spielern, im Spiel, hieß es.
Und hier in der Datenbank ist alles verschlüsselt, mit Spielerschlüsseln.
Das ist Geheimhaltung.
Das ist gegen unsere Ethik.“

Marlene: „Nein!
Das ist nicht gegen unsere Ethik.
Jeder hat seine eigene Version.
Du musst sie dir zusammenspielen.
Es gibt keine offizielle Liste der Gedankenfehler.
Die werden durch das Spiel erst sichtbar gemacht, individuell für jeden anders.
Aber du kannst gerne meine haben.
Die kann ich dir schicken.
Aber das sind meine.
Nicht die der Akademie.
Oder irgendwelche allgemeinen.
Das würde gar nicht gehen.
Eine allgmeine Beschreibung unserer kulturellen Gedankenfehler?
Das wäre der größte Quatsch.
Bevor wir nicht ein paar Hundert Spieler hatten, waren die Gedankenfehler nicht viel wert.“

Morpheus leise: „Es gibt gar keine Liste?“

Lilly: „Natürlich nicht!“

Marlene: „Jeder muss selbst Stellung nehmen und das Spiel und die Reaktionen der anderen Spieler zeigen dir dann, wie deine Version der Gedankenfehler anderen weiterhilft.
Du kannst irgendwann sogar neue hinzufügen.“

Morpheus: „Fuck!
Das wusste ich nicht.“

Lilly: „Fuck! Ja!“

Morpheus: „Und welche hat Rebecca dann erwischt?“

Marlene: „Das musst du sie fragen.“

Morpheus: „Aber wie verhindere ich, dass Rebecca an so etwas heran kommt?“

Marlene: „Sie will lernen, wie die Welt wirklich ist.
Was willst du da verhindern?
Dass sie Erfahrungen macht?
Du kannst ihr sagen, dass sie jederzeit zu dir kommen kann, wenn sie etwas erlebt, was ihr zu schaffen macht.
Aber so wie ich Rebecca kenne, kennt sie mindestens drei Erwachsene, zu denen sie jederzeit mit allem kommen ...
Und ich sehe auch nicht, dass sie es damit schwer hat, was sie in TRON aufsammelt.
Deine Frau hat es schwer.“

Morpheus: „Ja, weil Rebecca ...“

Marlene: „... in ihr Gefühle erzeugt hat?
Morpheus?
Echt?
Ein Taurus-Shirt kann in dir Gefühle erzeugen.
Zumindest indirekt.
Aber doch nicht ein anderer Mensch.“

Morpheus: „Ja, ich weiß das.
Aber das hier ist anders.
Ich selbst kann das meistens außen lassen, was mir andere entgegenbringen.
Und mit dem Rest kann ich meditieren.
Aber meine Frau kann das nicht.
Ich muss meine ..., ich will meine Frau so nehmen wie sie ist.
Und das packt sie nicht, wenn jemand von außen sagt, dass sie sich für ein Opfer hält, aber gar keines ist.
Und schon gar nicht, wenn das Rebecca macht.“

Marlene: „Dann hilf ihr.
Ich glaube, sie kann Hilfe gut gebrauchen.
Rebecca geht es ganz gut.“

Morpheus: „Fuck.
Das hört sich immer so schön an.
Aber ich habe ... ich fühle eben ...“

Marlene schaute ihn an: „Du hast Angst.“

Morpheus schloss die Augen und nickte.

Marlene: „Und Angst ist in Wirklichkeit ...“

Morpheus: „Ich weiß!“

Marlene: „Das ist einer der gefährlichsten Gedankenfehler.“

Morpheus: „Ich weiß.
Fuck!
Jah.“

Er atmete aus.

Marlene, Lilly und Morpheus schauten sich eine Weile lang an.

Morpheus: „Was jetzt?“

Marlene: „Wegen des Alarms gibt es eine Mitschrift von dem, was hier passiert ist und was wir gesprochen haben.
Wir schicken es an die Gemeinschaft.
Ohne Kommentar.
Du hängst die Logs dran, was du alles gemacht hast.
Und dann sehen wir weiter.“

Morpheus nickte.

