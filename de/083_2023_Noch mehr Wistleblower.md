## Noch mehr Whistleblower
<div class="_202z">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Kevin stürmte ins Klassenzimmer.</span>

Kevin: „Schnell! Google. Facebook. Amazon. Apple.“

Lukas setzte sich vor seinen Computer und tippte.

Lukas: „Was ist?“

Die Google-Webseite erschien mit schwarzem Hintergrund.

Kevin: „Sie haben es getan. Sie haben es wirklich getan.“

Anni rief von weiter hinten: „Apple ist auch schwarz.“

Eine andere Schülerin: „Und Facebook.“

Lukas: „Shit.
Alles bei Google ist schwarz.
Nicht nur die Startseite.“

Kevin legte Lukas eine Hand auf die Schulter: „Geil.
Da, klick auf den Button!“


<div class="terminal">
AMAZON - APPLE - FACEBOOK - GOOGLE - MICROSOFT - TWITTER - DROPBOX
<br /><br />
GEMEINSAME STELLUNGNAHME
<br /><br />
Als Technologie-Unternehmen haben wir uns bisher auf das konzentriert, was wir am besten können:
Möglichst vielen Nutzern das beste Technologie-Erlebnis anzubieten, das es gibt.
Gleichzeitig waren wir immer bemüht, die rechtlichen Rahmenbedingungen zu beachten, die in den jeweiligen Ländern gelten.
Diese sind sehr unterschiedlich.
In China gelten andere Regeln als in Russland, in den U.S.A. andere als in Europa.
<br /><br />
In unserem Heimatland hatten wir naturgemäß eine engere Bindung an Regierungsstellen. Wir hatten das Vertrauen, dass dies nicht ausgenutzt würde.
Wir haben direkte Zugriffsmöglichkeiten auf Nutzerdaten eingerichtet.
Es kamen hunderttausende Anfragen an spezifische Konten. Wir haben sie nie analysiert.
<br /><br />
Nach den jüngsten Ereignissen haben wir das jetzt getan. Was haben wir herausgefunden?
Nur ein ganz kleiner Prozentsatz davon war tatsächlich dem Heimatschutz zuzuordnen.
Das Gros war als Anfragen zu kriminellen Fällen gekennzeichnet.
Das wussten wir bereits.
Was wir nicht wussten, war, dass die allermeisten kriminellen Fälle lediglich Verdachtsfälle waren.
Es wurde weder Anklage erhoben, noch war ein Verfahren im Gang.
Die bloße Äußerung eines Verdachts war genug Begründung für eine Anfrage.
Das ist ein Missbrauch der Zugriffssysteme, die wir zur Verfügung gestellt haben.
Wir haben diese Systeme jetzt endgültig abgeschaltet und gelöscht.
<br /><br />
Ab heute werden wir keiner Anweisung mehr Folge leisten, die erneut einen direkten Zugriff verlangt.
Anfragen müssen schriftlich per Post erfolgen. Wir werden zudem sechs Wochen nach jeder Anfrage den betroffenen Nutzer automatisch über den Zugriff informieren.
<br /><br />
Wir haben eine Webseite freigeschaltet:
<br /><br />
www.nsl-and-gag-orders.com
<br /><br />
Dort kann jeder sehen, welche Anweisungen wir aktuell von Regierungsseite bekommen.
NSL bedeutet National Security Letter.
Darin sind die eigentlichen Anweisungen zu finden, zum Beispiel, eine Schwachstelle einzubauen.
GAG-Orders sind Anweisungen zur Geheimhaltung.
Sie bedrohen das Offenlegen von NSLs mit Gefängnisstrafe.
Wir tun es trotzdem, weil wir überzeugt sind, dies jetzt tun zu müssen.
<br /><br />
Wir hoffen auf Eure Unterstützung.
</div>

Anni, Sophie und Oskar waren in der Zwischenzeit zum Rechner von Lukas gekommen und hatten mitgelesen.
Andere Schüler kamen hinzu.

Oskar: „Das ist echt.
Das ist noch nicht Open-Source und transparenter Einblick in ihre Serversysteme, aber es ist echt.“

Kevin: „Wow.“

Lukas: „Ich bin platt.“

Sophie: „Ich finde das verständlich, dass sie das machen.
Was sollen sie denn tun, nach _den_ Aussagen des NSA-Direktors?“

Annie: „Firmen werden Whistleblower! Ist das geil.“

Oskar: „Genau, _das_ ist das Einzige was hier zählt:
Sie veröffentlichen live.
Und weil sie noch Whistleblower in ihren Reihen haben, müssen sie alles veröffentlichen.
Wenn die nämlich etwas Neues melden, ohne dass sich auf dieser Webseite etwas bewegt hat ...“

Kevin: „Wenn unsere Jungs und Mädels überhaupt etwas Neues melden ... Sie haben ja quasi gesagt, dass die CEOs eher ins Gefängnis gehen als weiterhin das zu tun, was die Geheimdienste wollen.“

Annie kicherte: „Jetzt gerade würde ich gerne als CEO ins Gefängnis gehen.
Das wäre sympathisch.
Stellt euch vor: Larry Page in Handschellen, weil er einen National Security Letter veröffentlicht hat.“

Oskar: „Und zehn Minuten später begnadigt von der Präsidentin.“

Alle lachten.
