## Angriff aus dem Internet
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Lilly stand leicht verträumt</span> an ihrem Stehpult in ihrem Teamraum.
Ihre Finger flogen über die Tastatur, während sie zwischen zwei Monitoren vor sich hin und her wechselte.
Morpheus legte ihr seine Hand auf die Schulter.

Morpheus: „Und findest du was?“

Lilly schüttelte leicht mit dem Kopf.

Morpheus: „Vielleicht hat er aufgegeben.
Ich meine, so groß wie er das angelegt hatte, kann er nicht denken, dass er noch einmal eine Chance bekommt, das nachzubauen:
Er hatte vier Schattenkonten, den Benutzer-Manager manipuliert, dass er verdächtige Bewegungen nicht mehr meldet und so weiter ...“

Lilly schwieg und tippte.

Morpheus: „Es sind jetzt drei Wochen und wir haben nichts Neues von ihm gefunden.“

Lilly: „Vielleicht wartet er, bis wir aufhören zu suchen.“

Morpheus: „Wie soll er wissen, dass wir immer noch nach ihm suchen?
Und wie soll er wissen, dass wir dann aufhören zu suchen?
Komm, wir haben viel vor in den nächsten Tagen.
Die Cypherpunk-Akademie bekommt die Ebenen-Insel 6 dazu.
Drei Spieler sind schon auf Ebene 4.
Dein Bruder Lasse zum Beispiel.“

Lilly: „Ja, ich muss da auch noch etwas dafür machen.
Aber das hier nervt mich.
Das ist unlogisch.
Ich weiß nicht, wie der Kerl überhaupt in unser System hineingekommen ist.
Ich verstehe einfach nicht, wie er den Anfang gemacht hat.
Du musst ja einen Fuß in der Tür haben, um so etwas zu machen.“

Morpheus zuckte mit den Schultern.

Morpheus: „Du wirst nicht immer alles verstehen, was in unserem System vor sich geht.“

Lilly warf ihm einen scharfen Blick zu.

In diesem Augenblick schalteten alle großen Monitore ihr Bild um und zeigten über ihnen einen roten Balken.

Morpheus: „Das sind meine Konsolen!
Sie zeigen meine Konsolen!“

Er rannte zu seinem Tisch und gab ein paar Befehle ein.

Morpheus laut: „Wir haben einen DDoS-Angriff!
Die Cypherpunk-Akademie ist runter auf 8% Verfügbarkeit ... 7 ... 5 ... 4 ... ganz weg.
Zur Zeit greifen 3.2 Millionen verschiedene Server auf die Akademie zu, die hauptsächlich vier verschiedene Dienste anfragen.“

<div class="infobox" id="ddos"></div>

Lilly rief: „2.9 Millionen sind feindlich, 300 000 normale Nutzer.
Ich versuche sie zu isolieren ...
Die schießen wie blöd ...
Sie ändern ihre Strategie ...
Ich kann sie nicht weghalten!“

Morpheus: „Jetzt kommen mehr!!
Shiiiit!
5 Millionen ... 6 ... 8 ... 13 ...
Das hört nicht auf.“

Von einem anderen Tisch: „Ich nehme die Akademie vom Netz!“

Morpheus: „Nein!
Wir haben Tausende von Kämpfen offen, Dialoge ... 300 versuchen gerade Gedankenfehler-Rätsel zu lösen.
Das kannst du nicht mitten drin abbrechen.
Wir müssen die Daten schützen.
Und wir wollen seine Strategie sehen.“

Lilly: „Er ändert sie die ganze Zeit.
Wenn ich umleite, dann reagiert er sofort darauf.
Wie macht er das?
Ich schnapp' mir einen von den Servern und schau mal, was da auf der anderen Seite ist.“

Lilly schickte nebenbei eine Nachricht an Marlene:

<div class="terminal">
Marlene! Komm in meinen Teamraum.
Wir haben gerade einen höchst merkwürdigen DDoS.
</div>

Morpheus: „Was willst du machen?
Die kommen aus dem Tor-Netz.
Da kommst du nicht ran.“

Lilly holte einen Stick aus ihrer Tasche, zeigte ihn Morpheus und steckte ihn dann in den Computer.

Lilly: „14 Millionen verschiedene Antwort-Pakete, jede versucht auf ihre Weise eine Schwachstelle auszunutzen.
Wir haben gerade etwas über 14 Millionen Server die in die Cypherpunk-Akademie hineinwollen.
Die schicke ich jetzt den Angriffen mal hinterher.
Mal sehen, was passiert ...“

Morpheus: „14 Millionen, wo hast du die her?“

Lilly tippte in ihrer Geschwindigkeit, wie versunken in einer anderen Welt.

Von einem anderen Tisch: „Oh, das macht den Angreifern Probleme.
Sie werden 20 Prozent langsamer.“

Lilly: „Langsamer interessiert mich nicht.
Ich will, dass nur ein oder zwei von dem abstürzen, was ich ihnen da zurückschicke.“

Alle schauten gebannt auf die großen Monitore.
Die Zahl der Angreifer nahm nicht ab.

Morpheus: „Woran siehst du, ob ein Server abstürzt.“

Lilly: „Warte ...“

Morpheus: „Sie ändern ihre Internet-Adresse, da kannst du nicht sehen, ob ein Angreifer nicht mehr kommt, weil er abgestürzt ist.“

Lilly: „Warte ...“

Morpheus: „Sie schicken auch unregelmäßig ...“

Marlene betrat den Raum und stellte sich neben Lilly.
Sie nickten sich kurz zu.

Lilly rief: „DA!“

Sie schaltete die großen Monitor auf ihren um.

Lilly: „Das ist von einem Angreifer.
Das ist der Quelltext von dem Angreifer.“

Alle schauten konzentriert auf die Monitore.

Lilly schaute plötzlich erstaunt auf, dann zu Marlene, die ihr einen ebenso fragenden Blick zurückwarf.
Dann schauten beide zusammen zu Morpheus.

Morpheus erschrak.

