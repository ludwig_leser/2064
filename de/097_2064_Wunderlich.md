## Wunderlich
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Sigur schaute Wunderlich trotzig an</span> „Wo habe ich Mist gebaut?“

[//]: # (# ########################### 1 #############################################################)
[//]: # (# - Was ist der Unterschied zwischen Fehler machen und Mist bauen?)
[//]: # (# - Was hat Sigur nicht gehört?)
[//]: # (# - Was war „Mist“?)

Wunderlich zuckte mit den Schultern: „Keine Ahnung.
Aber du hast, sonst wärst du nicht hier bei mir, sondern bei Lasse im Café.
Er hat sich gerade einen Eis-Shake bestellt.“

Wunderlich grinste Sigur vorsichtig an.

Sigur setzte sich im Schneidersitz vor ihn: „Das kann nicht sein!
Wir haben die Mission geschafft.
Ich habe die Mission geschafft.
Ich habe gerade zwei Ebenen geschenkt bekommen. Zwei!“

Wunderlich zuckte wieder mit den Schultern.
Wunderlich: „Und?“

Sigur: „Wie, und?“

Wunderlich: „Hmm.
Du bist dir sicher, dass du nicht verdient hast bei mir zu landen, nachdem du sooooo toll warst und eine Mission geschafft hast.
Aber du bist hier.“

Sigur: „Das macht keinen Sinn!
Zu dir kommt man nur, wenn man etwas falsch gemacht hat, nicht wenn man eine Mission geschafft hat.“

Wunderlich: „Falsch!
Zu mir kommen viele, die gerade eine Mission geschafft haben.
Und niemand kommt zu mir, weil er oder sie etwas falsch gemacht hat.
Du weiß noch nicht so viel von der Weisheit der Eule, hm?
Dinge falsch machen ist der Sinn jeder Schule.
Der <em>Sinn</em>, verstehst du?
Nur bei Dingen, die man falsch macht, kann man etwas lernen.
Und Schule ist zum Lernen da, nicht um etwas zu können.
Überhaupt: Wenn man etwas richtig macht, was man vorher nicht falsch gemacht hat, weiß man gar nicht, was daran richtig ist.
Wenn man etwas auf Anhieb richtig macht, dann bringt das gar nichts.
Das ist alles Eulenweisheit.
Hast du scheinbar keine Ahnung davon.“
Wunderlich schüttelte den Kopf.
„Und du willst in Ebene 4, ha!“

Sigur: „Und wann kommt man dann zu dir, wenn nicht, wenn man etwas falsch gemacht hat?“

Wunderlich: „Wenn man Mist gebaut hat.
Wie du.“

Sigur atmete laut aus: „Und WO ist da der Unterschied, Mist bauen und etwas falsch machen.“

Wunderlich rollte seine Augen nach oben: „Oh mein Gott!
Das weißt du nicht?
Wirklich?“

Sigur schaute Wunderlich streng an: „Nein, ich weiß das nicht ...“

Wunderlich: „Du bist hier, weil du genau das nicht unterscheiden kannst, du blinde Wurst!“

Sigur schluckte.

Wunderlich: „Mist ist, wenn du die Eule zum ersten Mal triffst und ihr nicht einmal zuhörst.
Der magische Moment, wenn du jemanden zu ersten Mal triffst ...
Auch nie davon gehört?“

„ICH HABE ZUGEHÖRT!“, brauste Sigur auf.
„Ich habe genau zugehört.
Ich habe lange darauf gewartet, Tin zu treffen.
Ich weiß wie wichtig ist, was sie sagt.
Außerdem höre ich sogar dir zu.“

Wunderlich: „Nur, weil du unbedingt hier raus willst ...
Sonst würdest du mir nicht zuhören.
Wie du der Eule nicht zugehört hast, du Schlafmütze.“

Sigur schaute ihn mit großen Augen an.
Sigur langsam: „WAS HABE ICH NICHT GEHÖRT?
WO HABE ICH MIST GEBAUT?“

[//]: # (# ########################### 2 #############################################################)
[//]: # (# - Menschen umbringen nur bei unmittelbarer Gefahr für einen anderen Menschen, Gegeneinander ist nicht möglich ab Ebene 3)
[//]: # (# - Es gibt keine endgültigen Sackgassen im Leben)
[//]: # (# - Gesetz und Recht sind nicht deckungsgleich)

Wunderlich: „Du hast der Eule gesagt, dass du auf die Insel-Ebene 4 willst.
Das ist eine starke Aussage, für so einen Grünschnabel wie dich.
Und die kannst du nicht so leicht wieder zurück nehmen.“

Sigur: „Aber ich <emn>will</em> das!
Ich will auf Ebene 4.
Lasse ist auch dort!“

Wunderlich: „Du kannst nicht so einfach auf Ebene 4.
Du hast gar nicht die Voraussetzungen dafür.“

Wunderlich schaute auf eine winzige Armbanduhr.
„Du bist für Ebene 2 eingetragen.
Du hast illegal einen Menschen umgebracht, der keine unmittelbare Bedrohung für irgendwen war.
Das ist total gegen jede Regel.“

Sigur schaute ihn verwirrt an.

Wunderlich las von seiner Uhr und hob dabei einen Finger: „Notwehr ... Rettungsschuss ... nur bei unmittelbarer Bedrohung für das Leben eines anderen Menschen.
Gegen so etwas kannst du auf Ebene 3 nicht verstoßen und hundert Mal nicht auf Ebene 4.
Ich sage dir eins: Du hast verschissen!“

Sigur wütend: „WO?
Wo habe ich einen Menschen umgebracht, der niemanden unmittelbar bedroht hat?“

Wunderlich: „Und du kannst nicht einmal mehr zurück gehen auf Ebene 2 oder so, oder ganz raus aus der Akademie.
Du hast gesagt, dass du in Ebene 4 willst.
Und das gilt.
Die Eule hat dich gewarnt.“

Sigur: „Hat sie nicht!“

Wunderlich: „Hat sie doch!“

Sigur: „Hat sie NICHT!“

Wunderlich: „Sie hat gesagt, dass du nicht bereit bist.
Und du hast das weggewischt und gesagt: Du bist bereit.
Deutlicher wird die Eule nicht.
Was die Eule sagt, das stimmt - normalerweise.
Über mich sagt sie ein paar Sachen, die sind nicht überall so ganz vollständig richtig, glaube ich.“

Wunderlich schaute kurz nachdenklich zur Seite.

Wunderling: „Die Eule sagt dir niemals, was du machen sollst.
Sie sagt immer nur was gerade ist und wie alles so gekommen ist, wie wir das jetzt haben.
Vergangenheit und Weisheit und so.
Hast du das wenigstens verstanden?
Das ist doch gaaaaaaanz einfach.“

Wunderlich schaute Sigur mitleidig an.
„Du bist jetzt Cypherpunk.
Du bist selber groß.
Du bist frei, das anzustreben, was du anstreben willst.
Selbst wenn es totaler Blödsinn ist, und du mit den bescheidenen Möglichkeiten, die du hast, niemals dahin kommen kannst.“

Sigur schloss die Augen und ballte beide Fäuste.

Sigur: „WO habe ich einen Menschen getötet?“

Wunderlich: „Einen?
Drei.
Im ersten der Hubschrauber, die deinen fliegenden Kampfroboter verfolgt haben.“

Sigur: „Das war FBI!
Sie haben mich gejagt.
Auf Leben und Tod.
Die wollten mit Raketen auf mich schießen.“

Wunderlich: „Auf dich?
Pah!
Auf deinen Kampfroboter wollten sie schießen, nicht auf dich.
Wenn du drin gewesen wärest, und sie gerade auf dich gezielt hätten, dann hättest du, glaube ich, auf sie schießen können.
Oder vielleicht doch nicht.
Ich bin mir nicht sicher.
Die Regelmeister der Akademie haben manchmal seltsame Ansichten.
Da fragst du am besten die Eule selbst.
Aber _du_ warst ja überhaupt nicht drin in dem Kampfroboter.
Und Lasse war weit weg.
Du warst in einem Bunker und hast Knöpfe gedrückt, mehr nicht.“

Sigur atmete durch.
Er dachte an die anderen beiden Hubschrauber, die er kurz nach dem ersten auch vom Himmel geholt hatte.

Sigur: „Aber das ist Krieg.
Das war 2028.
Da gab es andere Regeln als heute.
Zu der Zeit war das normal.
Im Krieg sterben Menschen.
Das waren keine Zivilisten.
Die haben damals einen Unterschied gemacht zwischen Zivilisten und Soldaten.“

Wunderlich: „Man darf keine Menschen umbringen.
Und wenn die Regeln damals so waren, dann waren sie eben falsch.
Die Gesetze damals hatten jede Menge Bugs, wir fixen heute noch Bugs aus der Zeit.
Wenn du einen Menschen umbringst, dann ist doch wurscht, ob du ihn als Zivilisten siehst oder als Soldaten.
Er ist ein Mensch.
Und Menschen darf man nicht umbringen.“

Sigur schaute ihn verwirrt an: „Im Krieg darf man keine Menschen umbringen?
Lasse hat auch ...“

Wunderlich unterbrach mit einer abfälligen Handbewegung: „Mit so einer Einstellung wirst du hier noch lange sitzen.
Ebene 4?
Pah.
Ich lache mich tot.
Ebene 3?
Vergiss es.“

Wunderlich kam mit seinem Kopf näher: „Du hast ver...schissen!“

Sigur lauter: „HABE ICH NICHT!“

Wunderlich: „Auf Ebene 3 geht darum, das Dagegensein zu überwinden.
Kapierst du das?
Das Einzige gegen das du da sein kannst, ist das Gegeneinander selbst.
Dagegen sein ist nicht für Cypherpunks, nicht ab Ebene 3.
Je mehr ich mir das hier anschaue:
Du kannst das Ganze knicken!
In der Akademie wird das nichts mehr mit dir.“
Er grinste ihn breit an.

Sigur: „Es gibt immer einen Ausweg in der Cypherpunk-Akademie.
Es gibt hier keine endgültigen Sackgassen.
Überhaupt nicht.
Nirgendwo!“

[//]: # (# ########################### 3 #############################################################)
[//]: # (# - Keine Gewalt, auch bei Provokation)
[//]: # (# - Grenzen testen fördert das Selbstbewusstsein)
[//]: # (# -)

Wunderlich: „Bitte schön.
Dann schau dich um!
Steinwände, etwa ein Meter dick, Gitterstäbe, keine Feile, kein Hammer oder sonst was.
Wie willst du hier raus?
Vielleicht könnte ich dich aufessen und stückchenweise durch die Gitterstäbe raustragen.“

Sigur versuchte nach Wunderlich zu schlagen, aber der huschte schnell zur Seite und versteckte sich hinter einem Stein.

Wunderlich zitterte am ganzen Körper: „Hej!
Was machst du?
Ich bin der einzige Freund, den du noch hast.
Wo sind die anderen alle?
Wenn du sie brauchst, sind sie nicht da.
Und überhaupt, du gehst schon wieder gegen jemanden, der keine unmittelbare Bedrohung ist.
Das scheint dein Sport zu sein.
Jetzt fass dich mal, sonst kommst du hier wirklich nicht mehr raus, du ... Schwachkopf, du ... Opfer.“

Sigurs Augen wurden dunkler.
Er rutschte zur Wand hinter ihm und lehnte sich an.

Sigur: „Shit ...
Ja, du hast Recht.“

Eine Träne drückte in sein Auge.
Er schaute eine Zeit lang starr vor sich.

Sigur: „Okay.
Ich habe einen Fehler gemacht.“

Wunderlich unterbrach: „Keinen Fehler.
Du hast Mist gebaut!
Du bist Programmierer.
Du kennst den Unterschied zwischen Maschinen und Menschen.
Und wo du nichts lernen kannst, kannst du auch keine Fehler machen.
Hmm, entschuldige, so etwas verstehst du ja gar nicht.“

Sigur: „Doch, ich verstehe das.
Sie sind tot.
Was soll ich machen?
Ok.
Das war Mist.
Soll ich jetzt bis ans Ende des Spiels hier bleiben?
Was für einen Sinn würde das machen.“

Wunderlich: „Viel Sinn.
Dann wäre einer weniger unterwegs, der einfach so andere Menschen umbringt.
Es macht schon Sinn, wenn du ab jetzt gar nichts mehr machst.
Du kannst einfach hier bleiben und auf das Spielende warten, du Versager.“

Sigur griff sich einen Stein und drohte ihn zu werfen.

Wunderlich duckte sich in übertriebener Panik: „Halt!
Halt!
Bist du verrückt?
Ich bin der Einzige, der dir hier raushelfen kann.
Und außerdem: Cypherpunks bedrohen niemanden mit Gewalt.
Gewalt, das ist Dagegen sein hoch 10.“

Sigur stutzte.
Er legte den Stein wieder auf den Boden: „Also kannst du mir doch helfen?
Du weißt doch einen Ausweg?“

Wunderlich hielt sich erschrocken den Mund zu und stammelte: „Hmmm.
Vielleicht.
Eher nein.
Wenn ich dich so anschaue.“

Sigur: „Es gibt einen Ausweg.
Ich weiß das.“

Wunderlich: „Dann zeig mir wo, du Schlaumeier. Oder besser <em>Dumm</em>meier.“

Sigur ballte seine Faust und hob sie an, nahm sie dann aber wieder herunter und schaute sie an.

Sigur: „Okay.
Keine Gewalt mehr.“

Wunderlich atmete erleichtert aus.
„Und wie willst du das schaffen?
In dir ist so viel kaputtes Gegeneinander.
Du bist voll davon, du Pappnase, du Esel, du Kleindenker!“

Sigur leise: „Wunderlich!
Sag mir, was ich tun kann.
Du hast von mir nichts mehr zu befürchten.“

Wunderlich: „Das weiß man nie.
Erst eitel Sonnenschein und gleich drauf kriegt man wieder einen Stein nachgeschmissen.“

Sigur: „Nein, echt!
Ich hab's begriffen.
Lasse hat mir das mal über dich gesagt:
Du testest gerade meine Grenzen, oder?“

Wunderlich: „Und das ist ziemlich einfach bei dir.“
Er schaute auf seine Armbanduhr.
„Du bist bei minus 120 Punkten.
Das hat hier im Kerker noch niemand so schnell geschafft.“

Sigur schaute einsichtig zu Boden.

Wunderlich wartete eine Weile und kam etwas näher: „Gut!
Du hast gefragt, was du tun kannst?
Du kannst zum Beispiel hier Anlauf nehmen und gegen diesen Stein dort springen, den ganz großen.
Nimm die ganze Zelle Anlauf, vom Gitter ab, und dann voll dagegen.
Und versuche richtig in der Luft sein, wenn du auf den Stein auftriffst.“

Sigur ging zu dem Stein und begutachtete ihn.
Er konnte nichts Besonderes erkennen.

Sigur: „Dieser Stein hier?“

Wunderlich nickte: „Oder ... vielleicht der andere daneben.“ 
Er schaute zweifelnd, grinste dann aber gleich und sagte: „Nein, der, auf den du zeigst.“

Sigur rüttelte am Stein.
Er bewegte sich nicht.
Er schlug mit der Hand darauf.
Es schien ein normaler Stein zu sein.
Dann ging er zum Gittertor, wollte Anlauf nehmen, aber zögerte.
Er winkte ab.

Sigur: „Das macht doch keinen Sinn.
Das ist ein ganz normaler Stein.
Ein kleiner Hinweis müsste da sein, wenn das ein Ausgang wäre.“

Wunderlich hatte plötzlich einen verbeulten Löffel in der Hand und zeigte ihn Sigur.
„Schau, du kennst doch Neo aus Zion?
Schau, das hier ist kein Löffel!
Es gibt keinen Löffel.
Na?
Das ist kein Stein.
Verstehst du?“

Sigur stöhnte leise und nickte.
Er nahm Anlauf, sprang weit und krachte mit voller Wucht mit Körper und dann Kopf auf den Stein.
Er prallte zurück und schlug mit seinem Arm auf den Steinboden.
Sein Arm brannte vor Schmerzen, sein Kopf dröhnte, ihm war schwindelig.
Der Stein hatte sich keinen Millimeter bewegt.
Er bekam pochende Kopfschmerzen und es wurde ihm leicht übel.

Sigur hielt sich den Kopf und rief: „Bist du sicher, ... dass es <em>der</em> Stein war?“

Wunderlich: „Ja, sicher.
Das ist der Stein, den ich gemeint habe.
Vielleicht warst du nicht schnell genug.“

Sigur schaute ihn ungläubig an.

Wunderlich: „Andererseits, warum sollte er sich auch bewegen.
Es ist ein Stein, eingemauert in eine Gefängnismauer.
Das machen die normalerweise so gut, dass du ihn nicht so einfach rausstoßen kannst, indem du mit Anlauf dagegen springst.“

Sigur: „Aber es ist doch ein ... versteckter ... Ausgang?“

Wunderlich: „Das habe ich nicht gesagt.
Ich denke, es ist ein normaler Stein.“

In Sigur wallte Wut hoch.
Er ging auf Wunderlich zu.
Der verkroch sich in eine Ecke.

Sigur: „DU HAST GESAGT, es ist der Stein, und wenn ich dagegenspringe dann ...“

Wunderlich: „Dann?“

Sigur: „Dann ...“

Wunderlich: „Dann?“

Sigur: „Ich hatte dich gefragt, was ich tun könnte ...“

Wunderlich: „Hast du es tun können, oder nicht?
Du hast es getan.
Du bist mit der vollen Breitseite gegen die Mauer gerauscht.
Du hattest nicht gefragt, was du tun kannst, um hier herauszukommen.
Nur was du tun könntest.“

Sigur schnappte nach Luft.
Er ballte seine Faust.
Lies sie wieder los.
Dann ging mit seinem Gesicht nahe an Wunderlichs und hielt den Atem.
Wunderlich drückte sich gegen die Wand, wurde panisch und auf einmal spuckte er Sigur mitten in Gesicht und flüchtete unter ihm hindurch in die gegenüberliegende Ecke.

Sigur wischte sich angewidert die Spucke aus dem Gesicht, sprang auf, drehte sich um und rief mit deutlich röterem Gesicht: „DU ...“
Dann blieb er stehen, atmete aus und schaute Wunderlich fragend an.
Er schaute einige Zeit.
Wunderlich schaute zurück.
Er blinzelte mit den Augen.
Sigur schaute weiter.
Er atmete ruhig.
Eine Minute, zwei Minuten.

[//]: # (# ########################### 4 #############################################################)
[//]: # (# - Jeder ist selbst für seine Gefühle verantwortlich)
[//]: # (# -)
[//]: # (# -)

Plötzlich ertönte ein leises „Pling!“ am Ende des Ganges hinter dem Gitter.
Eine etwa handgroße, bunt-glitzernd bemalte Glaskugel sprang ihnen entgegen, wie selbstverständlich sprang sie ohne anzustoßen durch die Gitterstäbe und auf Sigur zu.
Der öffnete instinktiv die Hand und die Kugel sprang hinein.
Sie leuchtete in den verschiedensten Blau- und Grüntönen.
Sigur betrachtete sie staunend.

Sigur: „Was ist das?“

Wunderlich: „Mei, mei.
Die sind heute aber mal gnädig mit dir.
Das ging ja verdammt schnell.“

Sigur: „Was ist das?“

Wunderlich unbeeindruckt: „Eine Zauberkugel, was soll das sonst sein?“

Sigur: „Und was kann man damit machen?“

Wunderlich: „Gegen die Wand werfen, zum Beispiel.“

Sigur schaute ihn böse an.

Wunderlich: „Nein, echt jetzt.
Du musst sie an eine Stelle werfen, wo sie Bedeutung haben könnte und dann wird sie die Stelle in etwas Sinnvolles verwandeln.“

Sigur: „In was?“

Wunderlich: „In ein Fenster, zum Beispiel.“

Sigur holte aus, stoppte dann aber und schaute Wunderlich an.

Sigur: „Ein Fenster?
Warum sollte ich dir glauben?“

Wunderlich sagte langsam: „In ein Fenster.“

Auf der Kugel erschien für einen Moment ein Fenster, mit einer weiten Aussicht.

Sigur schaute die Kugel an: „Na dann ...“
Er warf sie gegen die Wand.

In einem Augenblick zerstäubte an der Stelle, wo Sigur die Kugel hingeworfen hatte, alles in Millionen kleiner, leuchtender Staubpunkte, die sich langsam zu Boden senkten und dort in nichts verschwanden.
Hinter Ihnen erschien ein vergittertes Fenster mit einem herrlichen Ausblick über eine sonnendurchflutete weitläufige Landschaft.

Sigur: „Wow!“

Er ging zum Fenster, umfasste die Gitterstäbe und schaute nach draußen.
Er spürte kühlen Wind hineinwehen und lächelte Wunderlich zu.
„Na, das ist doch schon einmal ein erster Schritt.
Hier tut sich etwas.
Ich hätte die andere Seite nehmen sollen, dann hätten wir jetzt die Sonne im Raum.“

Wunderlich: „Ja, das war mir klar.
Aber du wolltest die andere Seite.“

Sigur ohne Anspannung: „Warum hast du das nicht gesagt, wenn du es gewusst hast?“

Wunderlich: „Weil du nicht _gefragt_ hast.
Ihr Menschen meint immer, ihr wärt die Schlausten, die es gibt im ganzen Universum.
Ich kann dir sagen: Nee, das ist nicht so.
Ich sage dir: löchriges Denken, wohin man schaut.
Ich habe schon Menschen hier erlebt, die haben noch langsamer gedacht als du.
Echt.
Und andere waren noch doofer, noch ungenauer im Denken.“

Sigur schloss die Augen und atmete ruhig.
Die Wut stieg wieder in seiner Brust an.
Wie konnte diese kleine Ratte ihn immer wieder so schnell an den Rand bringen?
Seine Brust pochte.
Er legte seine Hand darauf.

Wunderlich: „Weißt du, du bist das einfach nicht gewöhnt.
Ich bin eine Operratte, die einzige Tierart, die im Buch "Cypherpunks" erwähnt wird.
Menschen können sich nichts ausdenken, was wir Operratten nicht überwinden können.
Wir finden immer einen Weg hinein oder hinaus.
Und das ist schwer für sie zu ertragen.
Weil sie denken, dass sie uns überlegen sind.
Sie sind es aber nicht.
Und deswegen reagierst du auch so heftig.
Du bist ja richtig wütend.
Du wolltest mich ermorden.
Und alles nur, weil du es nicht erträgst, dass ich mehr weiß als du.“

Sigur schaute Wunderlich ernst und ruhig an.
Dann ging er wieder zum Fenster und prüfte Gitterstäbe und Fugen des Fensters.
Alles schien fest zu sein.

Sigur: „Ich weiß jetzt was du machst.
Ich weiß jetzt worum es hier geht.“

Wunderlich: „Ich bin gespannt.“

Sigur: „Du beschimpfst mich, du legst mich rein, du bewertest, was ich mache.
Und ich bekomme Gefühle deswegen ...
Und dann will ich dasselbe machen ...“

Wunderlich mit Lehrermiene: „Deswegen?“

Sigur: „Ich bekomme Gefühle, daraufhin.
Und das sind meine Gefühle.
Das hat mit dir nichts zu tun.
Das sagt mir etwas über mich.“

Wunderlich nickte mit erstaunter Miene.

Sigur: „Du kannst keine Gefühl in mir erzeugen.
Die entstehen in mir, weil ich so bin wie ich bin.
Lasse hätte ganz andere Gefühle.“

[//]: # (# ########################### 5 #############################################################)
[//]: # (# - )
[//]: # (# -)
[//]: # (# -)

„Pling“, kam es wieder vom Ende des Ganges und eine weitere Kugel sprang auf sie zu.
Sigur öffnete seine Hand und sie sprang hinein.

Sigur lächelte.

Sigur: „Und was jetzt?“

Wunderlich: „Jetzt haben wir gleich Sonne im Zimmer!
Wirf sie hier hin!
Sonne!“

In der Zauberkugel leuchtete eine Sonne auf.

Sigur schaute auf die Kugel.

Wunderlich: „Sonne.
Vitamin D.
Das ist Nahrung.
Das ist wichtig in einem solchen Kerker.
Wer weiß wie lange wir noch hier drin sind.
Und es wird garantiert Sonnenschein geben.
Und das ist auch gut für die Stimmung und für deine Haut.“

Sigur nickte und warf die Kugel an die gegenüberliegende Wand und auf die gleiche Weise erschien ein vergittertes Fenster.
Die Sonne blendete ihn unmittelbar.
Er hielt sich die Arme vor die Augen.

Sigur: „Ah, schön.
Das ist gut in dem dunklen Loch.“

Wunderlich: „Habe ich dir gesagt: Die Sonne kommt.“
Er grinste breit.

Sigur schaute zur Gittertür: „Aber eigentlich hätte ich die Kugel auch zur Gittertür werfen können, oder?“

Wunderlich nickte: „Dann wäre sie wahrscheinlich aufgesprungen und du hättest hinaus gehen können.
Ich verstehe nicht, warum du das nicht getan hast.
Türen sind normalerweise der Ort, wo man heraus- und hineingeht.
Und eine Zauberkugel macht immer etwas Sinnvolles, etwas, was weiterführt.
Das habe ich dir sogar gesagt.
Also ich habe beim ersten Mal gleich die Zauberkugel auf die Tür geworfen und nicht erst ein paar nutzlose Fenster gemacht.“

Sigur lachte: „Nein, sie wäre nicht aufgegangen.
Ich musste erst noch den Fehler machen, sie nicht an die Tür zu werfen, sonst wüsste ich gar nicht, was genau passiert, wenn ich sie an die Wand werfe.“

[//]: # (# ########################### 6 #############################################################)
[//]: # (# - )
[//]: # (# -)
[//]: # (# -)

„Pling“, ertönte es vom Ende des Ganges und eine rot-goldene Kugel sprang den Gang entlang, durch die Gitterstäbe, in Sigurs Hand.

Sigur grinste zu Wunderlich herüber und zeigte die Kugel.

Wunderlich: „Dann wirf sie jetzt auf den Boden.
Ich glaube, da ist ein Geheimgang mit dem du dann rauskommst.“

Sigur lächelte.

Wunderlich: „Du musst alles probieren, damit du weißt, was passiert.
Das hast du gerade gesagt.
Wenn du die Kugel jetzt zur Gittertür wirfst und rausgehst, weißt du nicht, was passiert, wenn du sie auf den Boden wirfst, oder an die Decke, oder in eines der Fenster.“

Sigur lächelte wieder und schüttelte den Kopf.

Sigur: „Nein, das muss ich nicht.“

Wunderlich: „Und warum nicht?“

Sigur: „Ich verspüre keinerlei Drang, das zu tun.
Kein Impuls.“

Wunderlich: „Aber du weißt dann nicht, ob es einen Geheimgang gibt, oder aus der Decke ein Seil herunterkommt oder das Gitterfenster aufgeht ...
wenn du das nicht ausprobierst.“

Sigur: „Und ich will es nicht wissen.
Das wäre eine Antwort auf eine Frage, die ich nicht habe.
Eulenweisheit.
Schon mal davon gehört?“

Wunderlich reckte seine spitze Nase nach oben und sagte: „Kann sein“.

Sigur lachte und warf die Kugel zur Gittertür.

[//]: # (# ########################### 7 #############################################################)
[//]: # (# - )
[//]: # (# -)
[//]: # (# -)

Sie zerstieb wie die ersten beiden und in der Wolke verwandelten sich die Gitterstäbe in blau glimmernde Lichtstäbe.
Sigur ging zu den Stäben und prüfte sie mit dem Finger vorsichtig.
Er wurde sanft zurückgestoßen.
Er stieß heftiger und wurde härter zurückgestoßen.
Er drehte sich um und schaute Wunderlich an.
Der zuckte mit den Schultern und schaute mit unschuldigen Augen zurück.

Sigur: „Komm, ich weiß, dass du weißt, wie man da durch kommt.“

Wunderlich nickt: „Ja, das weiß ich.
Aber so wie du drauf bist, wirst du mir das nicht glauben.
Und auf gar keinen Fall tun.
Aber du bist darauf vorbereitet.“

Sigur: „Sag schon.
Und keine Spiele mehr.
Ich will jetzt hier raus.“

Wunderlich ein wenig verschämt: „Okay.
Dann sage ich es dir.
Aber du sollst nicht mit dem Stein nach mir werfen.“

Sigur: „Mache ich nicht.“

Wunderlich: „Versprich es!“

Sigur: „Versprochen.“

Wunderlich: „Du musst von hier aus Anlauf nehmen und dann mit voller Geschwindigkeit und Kraft durchspringen.
Und du musst in der Luft sein, wenn du auf die Stäbe triffst.“

Sigur schloss die Augen.
Er öffnete sie, schaute auf Wunderlich: „Ich fasse es nicht!“

Wunderlich: „Ich meine das ernst, was ich sage, ich meine alles ernst, was ich sage.“

Sigur: „Wenn ich das tue, komme ich dann aus diesem Kerker hier raus, oder haut es mich nur wieder zurück?“

Wunderlich: „Durch!“

Sigur: „Raus oder zurück?“

Wunderlich: „Raus.“

Sigur nahm sich zusammen, ging ans andere Ende des Kerkers.
Er schaute Wunderlich an: „Und was ist dahinter?“

Wunderlich: „Ich weiß nicht.
Es ist spät.
Ein bisschen früher wäre dahinter die Sphinx vor der dritten Ebene.
Aber es ist spät.“

Sigur: „Was heißt spät?“

Wunderlich: „Wenn du die erste Kugel richtig geworfen hättest, dann wäre da wohl die Sphinx gewesen.“

Sigur: „Nein, wäre sie nicht.
Ohne die beiden falschen Würfe wäre ich hier nicht rausgekommen.“

Sigur nahm Anlauf und sprang mit aller Kraft, die er hatte, auf das Gitter.
Ein Lichtblitz umhüllte ihn, er fühlte sich für den Bruchteil einer Sekunde wie schwerelos, er flog und landete im Cypherpunk Café auf dem Boden, direkt vor dem Tisch, an dem Lasse saß.
Der beugte sich herunter, klopfte Sigur auf die Schulter und sagte: „Hi!
Na, wo warst du denn so lange?“
