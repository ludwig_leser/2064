

## **2023** Linda and Marlene

<span style="font-variant:small-caps;">Linda expected Marlene</span> behind the curtain. Robert was standing next to her.

She reached out her hand: "Linda Meyer".
She looked at her seriously.

Robert: "Robert Foster" You shook hands.

Linda: "I would exaggerate if I said: 'It is a pleasure to meet you'.
There's already been a lot of excitement about you.
And this will only be the beginning.
But all right, we'll make the best of it."

Marlene nodded, "I can well imagine it making waves."

From behind Julian Assange pushed himself towards the group and laid his hand on Marlene's shoulder.
She turned to him.

Julian: "Hej.
Great thing.
You've got this under control.
I can go home now with peace of mind.
Don't let it get you down.
It's just the United States..."

He nodded towards Linda and grinned at her.
To Marlene: "Nah, really, go on with that: That's real Cypherpunk."

Marlene laughed at him and they hugged each other for a long time.

Marlene: "Thank you for everything!
And sorry I stole your show today."

Julian grinned mischievously.
"Ciao!" He turned around and left.

Linda: "Okay.
We'll do it this way. You come with us to Washington tomorrow morning on the President's plane."

Marlene looked at her in astonishment: "How? Really?" She exhaled.

Linda: "There's no other way.
I know you have a plan of your own, but we don't want to risk it.
In Germany you are still officially wanted with an arrest warrant, here you are known nationwide.
This is putting the authorities in trouble.
In the United States, maybe one percent of people know her.
We can put that under the Wistleblower amnesty.
It is also not suitable for the masses to make a scandal report now about a president who brings a hacker with her to Washington.
It's not a good time.
That's not in the flow of news for a few weeks.
Maybe a few right-wing fringe groups, but nothing of any importance.
We'll have the prosecution suspend the warrant tomorrow and then you can fly back free with your brother."

Marlene: "If Amelie can fly with her team, that's fine with me."

Linda: "Amelie?"

Robert: "A blogger.
Pretty well known here."

Linda thought, looked at Robert. He shrugged his shoulders.

Linda breathed out and in again: "Okay!"

Marlene smiled, Linda didn't.

Robert: "We see the material before anything goes out..."

Linda: "That's how we do it.
We'll contact the team.
Then that's a yes?"
She looked at Marlene.

Marlene: "Yes."

Linda: "But tweet about it only in the airplane.
We leave tomorrow at 7:30.
You will be picked up by our security service.
You can stay here in any rooms as long as I understand.
Robert will show them."

Marlene nodded and Robert took her with him.


