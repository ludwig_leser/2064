

## **2023** The Manifesto (1)

<span style="font-variant:small-caps;">Marwin walked up and down the meeting room with </span> a pen in his mouth.
Suddenly he made a big step towards the wall, stopped in front of it and bobbed on his feet: "Encryption, encryption ... Hmm.
And then ... Yes! He snapped his fingers.
"Yes, exactly!" He sat down at the table, turned the pages and wrote:

<div style="background-color: #f8f8f8; color: darkblue; padding: 20px; margin: 20px 0; width: 650px;">
"2 -- The key ideas of our time, economic growth, money gain and security, no longer have strength. We cannot carry them with enthusiasm in our hearts.
They create opposition, distrust and fear everywhere.

We need new ideas, living ideas, ideas that can unhinge everything that is old and encrusted. And we already know these ideas, we already carry them in our hearts, but they do not yet determine our culture. For example: trust in people, courage, all seven kinds of it, creative working together, protecting the environment, a
transparent state, transparent companies, overcoming oppression..."
</div>

"Oppression..." he inhaled and turned the page.

<div style="background-color: #f8f8f8; color: darkblue; padding: 20px; margin: 20px 0; width: 650px;">
"3 - We must overcome our opposition. There's only one humanity. All people are part of it. Not a single one can be missing.

Being against each other separates what is actually connected. We have a world economy, but not just a world heart and a world thinking. There are no terrorists, no anarchists, no Nazis, no politicians, no investment bankers or soldiers. There are only people who are blinded by the opposition, who kill, rape, intimidate, abuse, oppress, manipulate or set shitstorms in motion because they themselves are afraid, because they are blind ... with fear. "Fear can be cured, but only one's own."
</div>
He turned the page.

<div style="background-color: #f8f8f8; color: darkblue; padding: 20px; margin: 20px 0; width: 650px;">
"4 -- We must keep holy the initiative, the impulses in every human being, also those in ourselves. Our strength to live comes from our own initiative. We need them like food and drink.

We can create spaces where we can meet, meet others, invent things together, be creative, freely follow our own impulses. We must learn where the borders to the other are, from when I hurt an impulse of the other, and learn to hold back there and to ... to let me live."
</div>

Marwin: "Let him live ... and understand how he thinks."
Marwin was thinking.
Then turned the page and continued writing:

<div style="background-color: #f8f8f8; color: darkblue; padding: 20px; margin: 20px 0; width: 650px;">
"5 -- We don't listen to each other.
We hear the words that someone else says and make our own thoughts out of them.
We don't have what the other guy was thinking.
What the other has thought, he has transformed into words: First filter.
Then we heard the words, transformed them into what we understand by the words: Second filter.
Then we select one or more thoughts from these twice transformed words: Third filter.
Then we won't have what the other guy was thinking.
Not what he said anymore.
Not even what we heard.
We only have what we think about what we heard.
Everything we talk to each other goes through these three filters.
Think - say, say - hear, hear - think.
And often my thinking differs from the other's thinking in a grotesque way, sometimes in things that determine life and death.
Wars are waged, murders committed, people excluded - only because we misunderstand each other.
We need to understand what our words mean for us and for others if we want to talk to others.
We can do a counter-test: Repeat each other's thoughts.
Then the other can tell if that's what he meant.

We have to stop judging what someone else is saying.
What he says is not meant to evaluate it as right or wrong, but only to understand what he is thinking.
And the better we can transform our thoughts into words, the better the other can understand us ... and add new thoughts to ours."
</div>

He laid the pen on the table and straightened up and said out loud: "New thoughts ... keep going ... "and be surprised by the other."

He turned to the mirror wall, behind which people were probably sitting and watching him.
Marwin: "And you? What do you think?"


