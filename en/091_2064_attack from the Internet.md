

## **2064** Inferno

<span style="font-variant:small-caps;">Lilly stood slightly dreamy</span> at her lectern in her team room.
Her fingers flew over the keyboard as she moved back and forth between two monitors.
Morpheus put his hand on her shoulder.

Morpheus: "And do you find anything?"

Lilly shook her head slightly.

Morpheus: "Perhaps he has given up.
I mean, as big as he put it on, he can't think he'll get another chance to rebuild it:
He had four shadow accounts, manipulated the user manager into not reporting suspicious movements and so on..."

Lilly kept quiet and typed.

Morpheus: "It's been three weeks and we haven't found anything new from him."

Lilly: "Maybe he's waiting for us to stop looking."

Morpheus: "How can he know we're still looking for him?
And how does he know we'll stop looking?
Come on, we have a lot to do in the next few days.
The Cypherpunk-Academy gets the Plain-Island 6 in addition.
Three players are already on level 4.
Your brother Lasse, for example."

Lilly: "Yes, I have to do something for it, too.
But this is bugging me.
It doesn't make sense.
I don't know how that guy even got into our system.
I just don't understand how he got started.
You must have one foot in the door to do something like this."

Morpheus shrugged his shoulders.

Morpheus: "You won't always understand everything that goes on in our system."

Lilly gave him a sharp look.

At that moment, all the big monitors switched their picture and showed a red bar above them.

Morpheus: "These are my consoles!
They're showing my consoles!"

He ran to his table and gave a few orders.

Morpheus loudly: "We have a DDoS attack!
The Cypherpunk Academy is down to 8% availability ... 7 ... 5 ... 4 ... all gone.
Currently, 3.2 million different servers access the Academy, mainly requesting four different services."

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
A <em>DDoS attack</em>, Distributed Denial of Service attack is an attack where the attacker has control over a large number of Internet computers and calls certain services of the attacked computer from each of them.
The attacked computer is overloaded and cannot answer normal requests anymore or only with delay.
This attack is often used to distract from another attack.
</div>

Lilly shouted: "2.9 million are hostile, 300 000 are normal users.
I'm trying to isolate them ...
They're shooting like crazy ...
They change their strategy ...
I can't keep her away!"

Morpheus: "More is coming now!
Shiiiit!
5 million ... 6 ... 8 ... 13 ...
It won't stop."

From another table: "I'm taking the Academy off the grid!"

Morpheus: "No!
We have thousands of fights open, dialogues ... 300 are trying to solve thought error puzzles right now.
You can't break that off in the middle of it.
We need to protect the data.
And we want to see his strategy."

Lilly: "He changes them all the time.
If I reroute, he'll respond immediately.
How does he do it?
I'm gonna grab one of those servers and see what's on the other side."

Lilly sent a message to Marlene by the way:

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
Marlene! Come to my team room.
We just have a highly odd DDoS.
</div>

Morpheus: "What do you want to do?
They're coming from the gate network.
You can't reach it."

Lilly took a stick out of her pocket, showed it to Morpheus and then put it into the computer.

Lilly: "14 million different response packets, each trying to exploit a vulnerability in its own way.
We just have a little over 14 million servers trying to get into Cypherpunk Academy.
Send him back to the attacks now, let's see what happens..."

Morpheus: "14 million, where did you get it?"

Lilly typed at her speed, as if lost in another world.

From another table: "Oh, that makes problems for the attackers.
They're getting 20 percent slower."

Lilly: "I don't care any more slowly.
I want only one or two of what I'm sending back to them to crash."

Everyone looked spellbound at the big monitors.
The number of attackers did not decrease.

Morpheus: "How can you tell if a server is crashing?"

Lilly: "Wait..."

Morpheus: "They change their Internet address, you can't see if an attacker has stopped coming because he crashed."

Lilly: "Wait..."

Morpheus: "They also send irregularly..."

Marlene entered the room and stood next to Lilly.
They nodded to each other for a second.

Lilly shouted, "DA!"

She switched the big monitor to hers.

Lilly: "This is from an attacker.
This is the source code of the attacker."

Everyone was watching the monitors with their eyes on them.

Lilly suddenly looked up in astonishment, then to Marlene, who gave her an equally questioning look back.
Then they both looked at Morpheus together.

Morpheus scared.



