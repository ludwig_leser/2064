

## **2064** Wonderful

<span style="font-variant:small-caps;">Sigur looked at Wunderlich defiantly</span> "Where did I screw up?"

 
 # What's the difference between making mistakes and messing up?
 # What didn't Sigur hear?
 # What was "crap"?

Wunderlich shrugged his shoulders: "No idea.
But you did, otherwise you wouldn't be here with me, you'd be with Lasse at the café.
He just ordered himself an ice shake."

Wunderlich grinned carefully at Sigur.

Sigur sat cross-legged in front of him: "That can't be!
We've accomplished the mission.
I've accomplished the mission.
I just got two levels for free. Two!

Wunderlich shrugged his shoulders again.
Wunderlich: "And?"

Sigur: "So what?"

Wunderlich: "Hmm.
You're sure you didn't deserve to end up with me after you were sooooo great and accomplished a mission.
But you're here."

Sigur: "That doesn't make any sense!
You only come to when you've done something wrong, not when you've accomplished a mission."

Wunderlich: "Wrong!
I have many who have just accomplished a mission.
And nobody comes to me because he or she did something wrong.
You don't know that much about the wisdom of the owl yet, do you?
Doing things wrong is the purpose of every school.
The <em>sense</em>, you know?
Only things you do wrong can teach you something.
And school is for learning, not for doing anything.
Anyway, if you do something right that you didn't do wrong before, you don't even know what's right about it.
If you do something right the first time, it doesn't do anything.
It's all owl wisdom.
You don't seem to know much about it."
Wunderlich shook his head.
"And you want to go to level 4, ha!"

Sigur: "And when do people come to you if they haven't, if they've done something wrong?"

Wunderlich: "When you've screwed up.
Like you."

Sigur breathed out loud: "And WHERE is the difference, screw up and do something wrong."

Wunderlich rolled his eyes upwards: "Oh my God!
You don't know?
Really?"

Sigur looked at Wunderlich very closely.
Sigur: "No, I don't know..."

Wunderlich: "You're here because you can't tell the difference, you blind sausage!"

Sigur swallowed.

It's crap when you meet the owl for the first time and don't even listen to it.
The magical moment when you meet someone for the first time ...
Never heard of it?"

"I HAD ACCESSED TO ACCESS!" Sigur blew up.
"I listened carefully.
I've waited a long time to meet Tin.
I know how important what she says is.
Besides, I'm even listening to you."

Wunderlich: "Only because you really want to get out of here ...
Otherwise you wouldn't listen to me.
How you didn't listen to the owl, you sleepyhead."

Sigur looked at him with big eyes.
Sigur slowly: "WHAT HAVE I NOT HEARD?
WHERE DID I SCREW UP?"

 
 # Killing people only in immediate danger for another person, against each other is not possible starting from level 3
 # - There are no final dead ends in life
 # - Law and right are not congruent

Wunderlich: "You told the owl that you wanted to go to island level 4.
That's a strong statement for a greenhorn like you.
And you can't take it back so easily."

Sigur: "But I <emn>will</em> that!
I want level 4.
Lasse is there too!"

Wunderlich: "You can't just go to level 4.
You don't have the qualifications."

Wunderlich looked at a tiny wristwatch.
"You're registered to Level 2.
You illegally killed a man who wasn't an immediate threat to anyone.
It's totally against the rules."

Sigur looked at him confused.

Wunderlich read about his watch and lifted a finger: "Self-defence ... escape shot ... only when there is an immediate threat to another person's life.
You can't break something like that on level 3 and you can't break it a hundred times on level 4.
I'll tell you one thing, you fucked!"

Sigur angry: "WHERE?
Where did I kill a man who didn't directly threaten anyone?"

Wunderlich: "And you can't even go back to level 2 or something, or all the way out of the academy.
You said you wanted to go to level 4.
And that goes for it.
The owl warned you."

Sigur: "She didn't!"

Wonderful: "She did!"

Sigur: "She didn't!"

Wunderlich: "She said that you are not ready.
And you wiped that out and said, "You're ready.
The owl does not become clearer.
What the owl says is true, usually.
She says a few things about me that aren't completely true everywhere, I think."

Wunderlich looked briefly and thoughtfully to the side.

Wunderling: "The owl never tells you what to do.
She always says only what is straight and how everything came to be like we have it now.
Past and wisdom and all that.
Did you at least understand that?
That's very simple."

Wunderlich looked at Sigur pitifully.
"You're cypher punk now.
You're big yourself.
You're free to aspire to what you want.
Even if it's total nonsense, and you can never get there with the modest possibilities you have."

Sigur closed his eyes and clenched both fists.

Sigur: "WHERE did I kill a man?"

Wunderlich: "One?
Three.
In the first of the helicopters that chased your flying combat robot."

Sigur: "That was FBI!
They were chasing me.
To the death.
They tried to shoot me with rockets."

Wunderlich: "To you?
Pah!
They were gonna shoot your robot, not you.
If you'd been in there and they'd been aiming at you, I think you could have shot them.
Or maybe not.
I'm not sure.
The academy's rule masters sometimes have strange views.
The best thing to do is ask the owl himself.
But you weren't in the fighting robot at all.
And Lasse was far away.
You were in a bunker pressing buttons, that's all."

Sigur took a breath.
He thought of the other two helicopters, which he had fetched from the sky shortly after the first one.

Sigur: "But this is war.
That was 2028.
There were different rules than today.
That was normal at the time.
People die in war.
They weren't civilians.
They made a difference between civilians and soldiers."

Wunderlich: "One must not kill people.
And if the rules were like that back then, then they were wrong.
The laws at that time had a lot of bugs, we still fix bugs from that time today.
When you kill a person, it doesn't matter whether you see him as a civilian or a soldier.
He's human.
And you can't kill people."

Sigur looked at him confused: "You can't kill people in war?
Lasse has also..."

Wunderlich interrupted with a snide hand movement: "With such an attitude you will sit here for a long time to come.
Level 4?
Pah.
I'm laughing myself to death.
Level 3?
Forget it."

Wunderlich came closer with his head and said, "You've fucked..."

Sigur louder: "HAVE I NOT!"

Wunderlich: "Level 3 is about overcoming opposition.
Do you understand?
The only thing you can be against is the opposition itself.
Being against it is not for Cypherpunks, not from level 3.
The more I look at this:
You can fold the whole thing!
In the academy, it's not gonna work out with you anymore."
He grinned at him broadly.

Sigur: "There is always a way out in the Cypherpunk Academy.
There are no final dead ends here.
Not at all. Not at all.
Nowhere!"

 
 # - No violence, even provocation
 # - Testing limits promotes self-confidence
 # -

Wunderlich: "Here you go.
Then look around you!
Stone walls, about a meter thick, bars, no file, no hammer or anything.
How are you gonna get out of here?
Maybe I could eat you up and carry you out through the bars piece by piece."

Sigur tried to beat Wunderlich, but he quickly scurried to the side and hid behind a stone.

Wunderlich trembled all over his body: "Hej!
What are you doing, man?
I'm the only friend you have left.
Where's everybody else?
If you need them, they're not there.
And anyway, you're going against someone who's not an immediate threat again.
This seems to be your sport.
Now grab a hold of yourself, or you're really not getting out of here, you ... You moron, you ... Victims."

Sigur's eyes got darker.
He slipped to the wall behind him and leaned against it.
Sigur: "Shit ...
Yeah, you're right."

A tear pressed in his eye.
He stared at himself for a while.

Sigur: "Okay.
I made a mistake."

Wunderlich interrupted: "No mistake.
You fucked up!
You're a programmer.
You know the difference between machines and people.
And where you can't learn, you can't make mistakes.
Hmm, excuse me, you don't understand that."

Sigur: "Yes, I understand that.
They're dead.
What do you want me to do?
Okay. Okay.
That was bullshit.
You want me to stay here till the end of the game?
What sense would that make?"

Wunderlich: "A lot of sense.
There'd be one less guy on the road killing other people just like that.
It makes sense if you don't do anything anymore.
You can just stay here and wait for the game to end, you loser."

Sigur grabbed a stone and threatened to throw it.

Wunderlich ducked in exaggerated panic: "Stop!
Stop! Stop!
Are you out of your mind?
I'm the only one who can help you out here.
And besides, Cypherpunks don't threaten anyone with violence.
Violence, that's opposed to being high 10."

Sigur truncated.
He put the stone back on the ground: "So can you help me?
You know a way out, don't you?"

Wunderlich held his mouth shut in fright and stammered: "Hmmm.
I might.
More like no.
When I look at you like that."

Sigur: "There is a way out.
I know that."

Wunderlich: "Then show me where, you wise guy. Or better yet, <em>Dumm</em>meier."

Sigur clenched his fist and lifted it, but then took it down again and looked at it.

Sigur: "Okay.
No more violence."

Wunderlich exhaled with relief.
"And how are you gonna do that?
There's so much broken opposition in you.
You're full of it, you fool, you donkey, you little man!"

Sigur quietly: "Wonderful!
Tell me what I can do.
You have nothing more to fear from me."

Wunderlich: "You never know.
"Only vain sunshine and right after that you get another stone thrown in."

Sigur: "No, really!
I get it.
Lasse told me about you once:
You're testing my limits right now, aren't you?"

Wunderlich: "And that's pretty easy with you."
He was looking at his watch.
"You're at minus 120 points.
No one has ever done that so quickly here in the dungeon."

Sigur looked down with insight.

Wunderlich waited a while and came a little closer: "Good!
You asked what you could do?
For example, you can make a running jump here and jump against this stone there, the very big one.
Take the whole cell approach, take it off the grid, and then take it all out.
And try to be really in the air when you hit the stone."

Sigur went to the stone and examined it.
He couldn't see anything special.

Sigur: "This stone here?"

Wunderlich nodded: "Or ... maybe the other one next to it."
He looked doubtfully, but then immediately grinned and said, "No, the one you're pointing at."

Sigur shook the stone.
He didn't move.
He hit it with his hand.
It seemed like a normal stone.
Then he went to the gate, wanted to take a run up, but hesitated.
He waved off.

Sigur: "That doesn't make any sense.
It's just a normal stone.
There should be a little clue, if that were an exit."

Wunderlich suddenly had a dented spoon in his hand and showed it to Sigur.
"Look, you know Neo from Zion, don't you?
Look, this isn't a spoon!
There's no spoon.
Well?
It's not a stone.
Do you understand?"

Sigur moaned softly and nodded.
He took a running jump, jumped far and crashed with full force with body and then head on the stone.
He bounced back and hit the stone floor with his arm.
His arm was burning with pain, his head was booming, he was dizzy.
The stone hadn't moved a millimeter.
He got a throbbing headache and it made him a little sick.

Sigur held his head and shouted, "Are you sure... that it was <em>the</em> stone?"

Wunderlich: "Yeah, sure.
That's the stone I meant.
Maybe you weren't fast enough."

Sigur looked at him in disbelief.

Wunderlich: "On the other hand, why should he move.
It's a stone walled into a prison wall.
They usually do it so well that you can't just push it out by jumping at it."

Sigur: "But it is a ... hidden ... Exit?"

Wunderlich: "I didn't say that.
I think it's a normal stone."

In Sigur, anger surged.
He walked up to Wunderlich.
He hid in a corner.

Sigur: "YOU SAMED, it's the stone, and if I jump against it..."

Wunderlich: "Then?"

Sigur: "Then..."

Wunderlich: "Then?"

Sigur: "I asked you what I could do..."

Wunderlich: "Could you do it or not?
You did it.
You bumped into the wall with your broadside full.
You didn't ask what you could do to get out of here.
Only what you could do."

Sigur gasped for air.
He clenched his fist.
Read them off again.
Then his face went close to Wunderlichs and held his breath.
Wunderlich pressed himself against the wall, panicked and suddenly he spat Sigur right in his face and fled under him into the opposite corner.

Sigur wiped the spit from his face in disgust, jumped up, turned around and shouted with a clearly redder face: "YOU ...".
Then he stopped, breathed out and looked at Wunderlich questioningly.
He looked for some time.
Wunderlich looked back.
He blinked his eyes.
Sigur kept looking.
He breathed quietly.
One minute, two minutes.

 
 # - Everyone is responsible for his own feelings
 # -
 # -

Suddenly a quiet "Pling!" sounded at the end of the corridor behind the bars.
A hand-sized, colorfully glittering painted glass ball jumped towards them, how naturally she jumped without bumping through the bars and towards Sigur.
He instinctively opened his hand and the ball jumped in.
It shone in various shades of blue and green.
Sigur looked at her in wonder.

Sigur: "What is that?"

Wunderlich: "Mei, mei.
They're gracious to you today.
That went pretty damn fast."

Sigur: "What is that?"

"A magic ball, what else is it?"

Sigur: "And what can you do with it?"

Wunderlich: "Throw against the wall, for example."

Sigur looked at him badly.

Wunderlich: "No, really now.
You have to throw it in a place where it could have meaning and then it will turn that place into something meaningful."

Sigur: "In what?"

Wunderlich: "In a window, for example."

Sigur pulled out, but then stopped and looked at Wunderlich.

Sigur: "A window?
Why should I believe you?"

Wunderlich slowly said, "Into a window."

A window appeared on the sphere for a moment, with a wide view.

Sigur looked at the bullet: "Well then..."
He threw her against the wall.

In a moment, at the place where Sigur had thrown the ball, everything atomized into millions of small, glowing dust points that slowly sank to the ground and disappeared into nothing there.
Behind you appeared a barred window with a magnificent view over a sun-flooded landscape.

Sigur: "Wow!"

He went to the window, grabbed the bars and looked outside.
He felt a cool wind blowing in and smiled miraculously.
"Well, that's a first step.
Something's going on here.
I should have taken the other side, we'd have the sun in the room by now."

Wunderlich: "Yes, I knew that.
But you wanted the other side."

Sigur without tension: "Why didn't you say so when you knew?"

Wunderlich: "Because you didn't _question_.
You people always think you're the smartest people in the universe.
I can tell you: Nah, it's not like that.
I'm telling you, holey thinking wherever you look.
I've seen people around here think slower than you do.
I'm serious.
And others were even sillier, even more imprecise in their thinking."

Sigur closed her eyes and breathed calmly.
The anger rose again in his chest.
How did that little rat keep bringing him to the edge so fast?
His chest pounded.
He put his hand on it.

Wunderlich: "You know, you're just not used to it.
I'm an opera rat, the only species mentioned in the book Cypherpunks.
People can't think of anything we opera rats can't overcome.
We always find a way in or out.
And that's hard for her to bear.
Because they think they're superior to us.
But you're not.
And that's why you're reacting so violently.
You're really angry.
You tried to murder me.
And all because you can't stand the fact that I know more than you do."

Sigur looked at Wunderlich seriously and calmly.
Then he went back to the window and checked the bars and joints of the window.
Everything seemed solid.

Sigur: "I know now you do.
I know what this is about now."

Wunderlich: "I'm curious."

Sigur: "You insult me, you fool me, you judge what I do.
And I get a feeling about it..."

Wonderful with a teacher's face: "That's why?"

Sigur: "I get feelings, then.
And those are my feelings.
This has nothing to do with you.
That tells me something about me."

Wunderlich nodded with astonished expression.

Sigur: "You can't create a feeling in me.
They arise in me because I am who I am.
Lasse would have very different feelings."

 
 # -
 # -
 # -

"Pling," it came back from the end of the corridor and another bullet jumped at her.
Sigur opened his hand and she jumped in.

Sigur smiled.
Sigur: "Now what?"

Wunderlich: "Now we have sun in our room!
Throw it here!
Sun!

A sun was shining in the magic ball.

Sigur looked at the bullet.

Wunderlich: "Sun.
Vitamin D.
It's food.
That's important in a dungeon like this.
Who knows how much longer we'll be in here.
And there will be guaranteed sunshine.
And it's good for your mood and your skin, too."

Sigur nodded and threw the ball to the opposite wall and in the same way a barred window appeared.
The sun dazzled him immediately.
He held his arms in front of his eyes.

Sigur: "Ah, nice.
That's good in the dark hole."

Wunderlich: "Did I tell you the sun is coming?"
He grinned broadly.

Sigur looked at the lattice door: "But actually I could have thrown the ball at the lattice door, couldn't I?"

Wunderlich nodded: "Then she probably would have jumped up and you could have walked out.
I don't understand why you didn't do this.
Doors are usually the place to go out and in.
And a magic ball always does something useful, something that goes on.
I even told you that.
So the first time I threw the magic ball at the door and didn't just make a few useless windows."

Sigur laughed: "No, it wouldn't have risen.
I had to make the mistake of not throwing them at the door or I wouldn't know what exactly would happen if I threw them at the wall."

 
 # -
 # -
 # -

"Pling", it sounded from the end of the corridor and a red-gold ball jumped along the corridor, through the bars, into Sigur's hand.

Sigur grinned at Wunderlich and showed the bullet.

Wunderlich: "Then throw them to the ground now.
I think there's a secret passage you'll come out with."

Sigur smiled.

Wunderlich: "You must try everything so that you know what happens.
That's what you just said.
If you now throw the bullet to the barred door and go out, you don't know what will happen if you throw it on the floor, or on the ceiling, or in one of the windows."

Sigur smiled again and shook his head.
Sigur: "No, I don't have to."

Wunderlich: "And why not?"

Sigur: "I don't feel any urge to do that.
No pulse."

Wunderlich: "But then you don't know whether there is a secret passage, or a rope coming down from the ceiling or the barred window opening ...
if you don't try that."

Sigur: "And I don't want to know.
That would be an answer to a question I don't have.
Owl wisdom.
Ever heard of it?"

Wunderlich raised his pointed nose and said, "Could be."

Sigur laughed and threw the bullet at the barred door.

 
 # -
 # -
 # -

She destroyed like the first two and in the cloud the bars turned into blue shimmering light bars.
Sigur went to the bars and carefully checked them with his finger.
He was gently pushed back.
He pushed harder and was pushed back harder.
He turned around and looked at Wunderlich.
He shrugged his shoulders and looked back with innocent eyes.

Sigur: "Come on, I know you know how to get through."

Wunderlich nods: "Yes, I know that.
But the way you're on it, you're not gonna believe me.
And don't do anything at all.
But you're prepared."

Sigur: "Tell me.
And no more games.
I want to get out of here now."

A little shamefaced, wonderfully: "Okay.
Then I'll tell you.
But you shall not throw the stone at me."

Sigur: "I'm not."

Wunderlich: "Promise!"

Sigur: "Promise."

Wunderlich: "You have to take a run-up from here and then jump through with full speed and power.
And you must be in the air when you hit the bars."

Sigur closed his eyes.
He opened it, looked at Wunderlich and said, "I can't believe it!"

Wunderlich: "I mean what I say, I mean everything I say."

Sigur: "If I do that, will I get out of this dungeon or will it just knock me back?"

Wunderlich: "Through!"

Sigur: "Out or back?"

Wunderlich: "Out."

Sigur pulled himself together, went to the other end of the dungeon.
He looked at Wunderlich and said, "And what's behind it?"

Wunderlich: "I don't know.
It's late.
A little earlier behind it would be the Sphinx before the third level.
But it's late."

Sigur: "What does late mean?"

Wunderlich: "If you had thrown the first bullet correctly, there would have been the sphinx."

Sigur: "No, she wouldn't be.
I wouldn't have gotten out of here without the two wrong throws."

Sigur took a running jump and jumped on the grate with all his strength.
A flash of light enveloped him, he felt weightless for a fraction of a second, he flew and landed in the Cypherpunk Café on the floor, directly in front of the table where Lasse was sitting.
He bent down, patted Sigur on the shoulder and said, "Hi!
What took you so long?"


