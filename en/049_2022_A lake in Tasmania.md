

## **2022** By a lake in Tasmania

<span style="font-variant:small-caps;">Marlene sat on </span> a jetty at a seaside beach in the north of Tasmania.
Her feet let her dangle in the water.
She was wearing a big sun hat.
The evening sun bathed everything in a warm light.
In front of her, a boat tied to the jetty rocked on the waves.
From the hill behind her, two men came up to her.

Hans: "Come on! This way, this way ... Fred? Was that Fred?"

Fred nodded.

Hans pointed down the small hill towards the lake where Marlene was sitting.
A beaten track led right there.

Fred: "That's Marina?"

Hans nodded.

Fred: "Can I surprise her alone?
We haven't seen each other in a long time."

"Sure," Hans said.
"Then I'll start with dinner once already.
Today we make ... let's think about it ...
Casserole with aubergines, courgettes, sweet potatoes and cheese.
All own cultivation.
You'll like it."

He turned around and disappeared up the hill.

Fred sneaked quietly down the beaten path.
He looked at Marlene the whole time and did not want to betray himself with any noise.
When he was three meters behind her, he stopped.
She suddenly turned around, but was not frightened, as Fred had suspected, but smiled, swallowed, smiled again and held her hand in front of her mouth.

Marlene: "Fred!!!"
She jumped up and fell around his neck.
"Fred!
You're here, you're here!
Great!"
They hugged each other for a long time and then sat down together on the jetty.

Fred: "And, tell me... how about you right now?"

Marlene: "Totally nice here.
Hans is so great.
He's lived in this spot for forty years.
He's from Germany.
His father thought in the 70's that a catastrophe was imminent in Europe and moved here with the whole family.
He has a mini hostel here and together with his wife he grows almost everything they eat.
All organic, of course.
And I'm helping him now, officially.
I'm an exchange student.
I've learnt a lot of things: how to hack properly with a real hack, you know?"
She moved her hand.
"I can stay here as long as I want.
I work here, get food, a bed, internet and a little pocket money.
A lot of people do that here in Tasmania.
They come from all over the world and spend a few weeks, a few months or a year here."

Fred: "You're not afraid?"

Marlene shook her head.
"Not at all.
I had a good way of getting here.
I got out of Saudi Arabia good as a second wife in burka on a ship.
A distant relative of Achmed, in the 18th century they have a common great-great-great-great-great grandfather, took me along with his real wife to Indonesia.
There I became a backpack tourist, with a passport that I still had from Marwin, and then I hitchhiked to Tasmania.
Hans here is a distant uncle of a Darknet hacker who has an office job in his normal life, nothing at all with computer programming.
You've seen each other once in the last forty years.
But best of all, Tasmania.
The island is about as big as Bavaria, same climate, but has only half a million inhabitants, 7 inhabitants per square kilometre.
Everybody knows everybody here.
And not much is happening here.
If anyone's looking for anyone here, they all know it right away.
This is exciting for the people here.
So if CIA guys show up around here, I know that before they even get to the house.
And I told Hans that I was being followed by a jealous friend who should never find out that I was here.
He said, "No problem, he'll tell me everything he hears right away."

Fred shook his head slowly: "Typically Marlene.
You hide in public where everyone can see you.
That's your thing.
I would have thought of a big city, Mexico City, anonymous skyscraper development.
But you go where everyone sees you.
Great!"

Marlene: "Hey, and once a week Hans and I go out by boat and get oysters.
Fresh oysters with lemon, you know what that tastes like?
Hmmmm.
You have to try that ...
But you say now.
I'm so excited, it's tearing me apart.
Did you bring anything?
You just wanted my address in the last pond message."

Fred: "I only wanted your address because I wanted to talk to you directly about everything else."

Marlene looked at him expectantly, "So you got it?"

Fred nodded and pulled a USB stick out of his pocket.

Marlene put her hands in front of her face and squeaked "Jiiiiiiiiiihhhhhhaaaa."
She jumped up and danced around Fred, dropped next to him, kissed him on the cheek and took the stick: "On here?"
She looked at him.

Fred nodded again.

Marlene held the stick by hand and pressed it against her chest: "How much?"

Fred: "A terabyte, like you said."

Marlene rolled a tear down her cheeks: "Cool.
So awesome.
Have you seen what's inside?"

Fred nodded.

Marlene: "And?"

Fred: "I've already sorted a lot.
It's a disaster.
Unbelievable.
MacOSX has seventeen different backdoors in all current versions.
It's a piece of cake to get anywhere with it.
Perfect for script kiddies.
Each student can hack into the biggest companies with the codes and read and delete everything and steal and change.
There are things for all operating systems: Windows, Linux, for all programs, for all computers.
They have their own department for hard disk rear doors, which are already on the manufacturer's list, every major manufacturer.
Almost every hard drive you can buy comes with a back door that works no matter what operating system you install.
It's a bit like AMT by Plugable TransportsIntel, just for hard drives and secret.
<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
<em>AMT</em>, Active Management Technologies, is available in every modern Intel processor, the heart of every computer.
It is an add-on processor that can completely remote control the main processor.
You can switch on a computer from anywhere in the world, boot it up, install a program and then switch it off again.
Or install a new operating system.
Or just copy some files.
If you have the access codes, you can do almost anything from a distance that you can do with the right password on site.
For Intel itself, AMT is a tool for IT managers to maintain a company's computers from a central location.
But if you have the right password, it's a general backdoor to a computer that doesn't set many limits.
</div>
I tell you, the Internet is a giant Swiss cheese, and the rats climb all over it and have a great life."

Marlene: "Tails?
Goal?
What about them?"

Fred nodded.

Marlene: "Shit.
Bad?"

Fred nodded again: "... not as bad as Windows and MacOS encryption, but there's something to do..."

Marlene: "Pond, Ricochet, VeraCrypt?"

Fred: "Pond and Ricochet actually seem to be difficult for them, VeraCrypt a few little things, but nothing really critical.
It is clear that the NSA wanted to shut down the predecessor Truecrypt.
It's really good."

Marlene: "Okay.
I've thought a lot about it.
First, we send the relevant vulnerabilities to the Tails team, the Tor team, the GPG project, Veracrypt, and so on.
We need secure channels for the rest of things.
They will then, if necessary, also pass some on to the Linux teams.
So, first we close down our software, then we can start to inaugurate larger circles so they can patch the gaps.
Next, everything in the Linux and BSD worlds.
It will be an incredible amount of work, but at least now everyone knows what to look for.
If you have a template, it's a hundred times easier to fill a gap.
We need to figure out how much time we're gonna give them.
We can't wait too long."

Fred: "And MacOSX and Windows?
Smartphones?
IOS, Android?"

Marlene: "No advance warning!
They'll see the loopholes if we get this out there.
It's not free software.
They put in a lot of back doors themselves."

Fred: "No warning?
You can't do that.
Most computers and smartphones use these operating systems.
Billions of people.
If you don't give the developers time to fix the holes, you get chaos, then ... Puhhh.
No!
I'm serious.
Then any 12-year-old with a small program can rein at the Federal Intelligence Service and delete files.
Imagine that.
Every ex-girlfriend can look at the ex-girlfriend on her smartphone, read everything, delete, change, send messages in her name ... And vice versa.
You can hack all Facebook and WhatsApp accounts, do foreign online banking, go shopping ... And there are enough people out there who will do that."

Marlene shook her head: "I had plenty of time to think about it.
The way they took the Snowden releases.
Most data have still not been published.
And there's hammers in it.
No.
No warning!
Not a minute.
Only if they first publish a list of all the backdoors they have built in themselves, take them out, and publish the complete source code of their programs."

Fred shook his head: "They never do that!
You have no idea what..."

Marlene interrupted: "We need chaos.
Otherwise it won't work.
We can't get through if we put the thread in their hands.
Not even a piece of it.
You hate chaos."

Fred set out to say something, but pulled back.

Marlene: "I will tell you what plan I have developed since Saudi Arabia.
It's a risk, but it'll work.
But first of all we need peace and quiet.
We'll make enough mistakes, so we need calm, clear thinking.
The plan has to be great.
But we're not starting that today.
Today you have arrived here, today there is casserole, I guess, today is a beautiful sunset, for the first time with you.
And I don't even know how it went, how you got the stick, how Anita is doing.
Is she still with NSA?
How did she get the data out?
How did she get the Raspberry Pi into the data center?
And the stick out again?"

Fred started to say something again, raised his finger, stopped and laughed: "You are unbelievable!
We've got the biggest thing since Snowden, much more dangerous than Snowden, the Collateral Murder video and the war logs of Chelsea Manning combined, and you're thinking casserole."

"Yes," she nodded.
"That's important, too.
And dessert.
Hans makes a great Tiramisu.
"Let's go, then I'll show you the garden where the things we'll eat are from."


