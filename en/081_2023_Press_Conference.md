

## **2023** Press conference

<span style="font-variant:small-caps;">The classroom was </span> full of rows of chairs.
In front of the desk stood three tables with chairs behind them and one name card each.
Kevin and Sophie sat next to each other at the middle table.
Two camera teams positioned themselves in the room.
Lukas worked with another student on a computer.
Anni and a student checked press passes at the entrance.
The room filled slowly.

A journalist shouted from the middle of the room: "Kevin!
What's new today?"

Kevin: "A little moment, Christoph.
There's really something new.
Breaking news.
Don't you want a better seat up here?
It's worth it.
And open Twitter..."
He smiled at him and then turned to Luke: "Are you ready?"

Lukas: "In a moment.
"Almost everything's fine."

A journalist shouted from the back row: "How many classes are on strike now?"

Kevin: "Equal, equal.
It's all here on the note."

Anni shouted from the door: "1220."

The journalist shouted, "Wow!" and typed something into her iPad.

Somewhat to her right sat Amelie, in front of her Joe with camera and Sven with microphone.

Amelie quiet, a little conspiratorial: "Welcome to the special edition of Amelie's Out of Berlin!
We crept in here today because we learned from well-informed hackers that something was happening here today.
A world premiere.
A game changer it's supposed to be.
We can't stay away there, can we?
Ah, here comes our tipster..."

Joe turned to the door with his camera.

Oskar entered the room and looked around.
He waved at Amelie.
Then he went to Kevin, put a USB stick on his table and said: "The transcript.
The stick's clean.
You can take it right away."
Kevin got up and took the stick to Lukas.
He nodded.

Lukas: "Okay, then we're done.
I'll upload this, and then we can get started."

Kevin stood in front of his table.
He looked into the room.
He was packed by now.
Some journalists had to stand.

Kevin: "So, I think we can get started..." He cleared his throat.
"Welcome!
The procedure for today is that first we have a whole new thing for you, then we say something about the strike network and then you can ask questions.
Anyone don't have Internet?
- Good, good, good. Okay. He was looking at a little monitor next to him.
"Wow, we have over 600,000 Internet viewers.
All right, then.
Point one.
Luke!"

The beamer went on and the face of the director of the NSA appeared.

Kevin: "You know this gentleman, don't you?" A soft laugh went through the room.

Kevin: "Okay.
This is what he said a week ago in Congress.
The so-called cyber-terrorism report."

Luke pressed a button.

Director's voice: "We must step up our efforts.
In the wake of last year's major attack, we have seen a radical increase in attacks on our infrastructure.
This blatantly refutes the cyber-terrorists' claim that publishing vulnerabilities leads to greater security on the Internet.
The opposite is true.
Hundreds of thousands, perhaps millions of criminals have used the publications to invade people's privacy, cheat, blackmail and destroy their data.
And terrorists now communicate freely wherever they want to communicate.
"Without us being able to intervene."

Luke pressed a button.

Kevin: "So far the NSA chief two weeks ago.
I don't want to comment on the content here.
That's not what it's about today.
It's about his voice today.
How he speaks, how he sounds.
Because there is a second recording that we want to play for you.
And it was made five days ago, here in Berlin,
shortly after the President of the United States announced the amnesty for Wistleblower.
The conversation was at headquarters on Chausseestraße."

Luke pressed a button.

Voice of the director: "The next time I meet the little poison noodle from the white house, I'll button it up.
We have responsibility here for the greatest nation in the world, and for our friends too.
We cannot accept that she castrates us in such a way.

We've, damn it, built the best news system the world has ever known in recent years.
Together.
We were so close that we could look live into every computer in the world, that we could see the tiniest changes in political movements around the world from the screens at Fort Meade, that we could follow terrorists everywhere and at every second of their lives.
And we had kept the population relatively calm, even in Europe.
Relatively quiet.
Protest: Yes.
Real consequences: No!
Not even one percent was really interested.
We were so close to it ...
And then this shit now.
I don't care what she said in that speech."

Kevin: "The little poison noodle, you noticed, is the president of the United States.
His top boss.
And he doesn't care what she says.
He thus also whistles at the legislation of the USA, at the legal system, at the political system, he shows America the middle finger."

Murmurs and conversations in the room.

Kevin raised his arms: "There's more to it.
Does that mean anything to him that has been reality since yesterday?"

Luke pressed a button.

Voice of the director: "We continue with vigour as before.
And if we get more headwind from the companies now, good!
Then we'll make more wind.
We can.
We can do that with American companies, too.
They think they have immunity under the President's protective hand.
Fuck it!
They'll see what it means to mess with the NSA."

Murmuring louder.

Kevin moved his hand.
The mumbling ended.

Kevin: "One more thing.
We've got one more thing." He took a break and looked into the room.
"And that's what's dear to our hearts.
The director's talking about Marlene."

Luke squeezed again.

Director's voice: "I'm going to get the little bitch myself now.
An 18-year-old girl with no school-leaving qualifications.
She got it rolling, and she's still on the loose.
And the CIA clown noses can't find them.
I only hear Northern Italy, Croatia, Montenegro.
We're at the country level.
I want house numbers.
I'm gonna put her on the death list this afternoon, high up.
It's overdue.
There are far fewer dangerous people on it.
And I don't care what kind of fuss it makes.
When it is done, the President will have to confess: "Is she on the side of the terrorists or on our side?"

Kevin: "This is incitement to murder.
On German soil.
And a state conspiracy in his own country."
He looked seriously into the audience.
"I now expect, and I mean it seriously, an investigation and then an indictment by the Federal Prosecutor's Office against the NSA director and the BND president, because he did nothing about it.
Marlene is a German citizen.
The director must be arrested the next time he steps on German soil."

Lukas: "The recording of the whole conversation is on our website and also a transcript of it.
It's about three times as long as the clippings you heard."

Reporter: "Where'd you get that?"

Kevin: "Unfortunately, we cannot say anything about the source at this time.
Just this much: She works at the BND and is already in contact with the Courage Foundation.
Your legal defense will be prepared.
I don't know where she is right now either."

Sophie got up.
She looked into the audience and waited a short time.
Then she said in a calm, serious tone: "Actually, I actually hope that we don't need this any more.
The source should not have to defend itself.
She dared a lot.
It's a personal statement she leaked there, but it's everybody's business.
It uncovers crimes.
It is a drawing of a broken justice system when someone can make death threats and get away with it, and someone who brings it to the public is persecuted.
But there are probably still such corrupt prosecutors who pursue political goals and use their independence from other state agencies to achieve them.
Just like Marlene Ny did in Sweden for many years with Julian Assange.
She could have waived the warrant for Julian at any time.
She didn't do it.
Even when the UN Human Rights Council said it was illegal to detain Julian, she did not take it back.
Only when it was too late did she do it.
And in the meantime she has retired normally without ever having to answer for it.
I hope we're further along in Germany."

Kevin: "Yeah, I hope so, too."

Amelie in the last row in the camera: "Yes, we all hope so here.
Let Marlene celebrate with us here.
We all need to look at what's happening to the NSA director now.
If we look at it here and make a fuss every now and then, then something will move.
That's why I have another action for you.
And you're getting it on a lot of YouTube channels today.
I want us all to go out tonight at 8:00, point 8, to the balcony, the street or the roof garden and shout I AM MARLENE! as loud as you can!
And if you can find a megaphone, take this.
We've got some great boxes on the street outside the studio.
Everyone's allowed to call through there tonight.
Come over!
Or shout from you wherever you are!"

Oskar turned around, looked at Amelie and raised his thumb.
He shouted to her, "I'll say that in our transmission here.
This is going to be a party tonight in Berlin and other cities."


