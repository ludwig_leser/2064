

## **2064** A flying combat robot

<span style="font-variant:small-caps;">Lasse sat up</span> the back seat of a van and worked on his laptop.
They were driving on some highway through Arizona, through the middle of the desert.
Far away from the road, there were mountains to see.
Driver and co-driver were engaged in a conversation.

Lasse launched Pond.
He looked spellbound at the screen.
First, the usual message:

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
>>> There are no messages waiting to be transmitted.
</div>
He typed "log."

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
- &lt;font color="#ffff00"&gt;-==- proudly presents
</div>
"Wait five minutes. Okay!" he thought.
Pond did not fetch and send messages immediately, but at random times.
It made it harder for secret services to figure out if someone was using Pond.

Lasse: "No, not okay."
He typed "transact now."
After a few moments:

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
(Jan 14 10:40) Message received from sig
</div>
"Shit. Shit!
It's here: The news from Sig," he thought, "Now it's going to be exciting.
Is he free?"
He typed "show."

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
Hi, Lasse,

I'm with Tim!!! Yes! I'm out! Everything worked out. It's such a super turd over there.
I really can't believe it. I have a fucking rage. When I meet Fortunato in the dark,
then I stick a cyanide capsule up his ass. But one with a water-soluble shell. And in
the last 30 minutes of his life, I tell him Monty Python sketches one by one.
Such a super asshole.

But I'm out for now. That's good. That's good. And without a tail. I'm sitting here in a 60 year old
old nuclear shelter under Tim's house. His grandmother built it. They were really afraid of
a nuclear war. You think so? Just as later everyone was afraid of terrorists. But now
that to us. No signals coming out or in here. All tight.

Joe built me a little clock for my hydrocyanic acid capsule so she'd always think that the
to be connected to the outside world and not release the acid at some point ... my dear little one. Was cooked
not so hard to make, Joe modified the capsule's system program. And it works
actually, because: I'm still alive. It's been more than five hours. Only when I walk out of
here I have to put on a super fun full-body aluminum costume under my clothes: So that the love
Little one doesn't somehow radio home.

Slowly I wake up too and my head no longer turns in waves. It's crazy what you do.
they give you everything so you can't think. I'd rather have handcuffs than pills like that. I think,
I need a real rehab, or better Lilly's chili-ginger-garlic diet: for two weeks!
Remember that? In the morning after waking up: two freshly picked and peeled garlic cloves slowly
chew while keeping your composure ...

I came here with the hitchhiking trick you're probably using right now. I've only had three random
Driver, then Joe, Luis and finally Tim. Six stations. They can't handle it, not if Joe, Luis and Tim
drive. And since Joe, I've had the costume on and the clock.

Now I'm going to chill. Mate's standing right in front of me. When are you coming?

sig
</div>
Lasse wrote, "reply."

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
Hi Sig!

Wow, great! I'm on the third station with the Hitchhiking right now. I don't think the driver's LBI.
Is a courier driver and obviously has a colleague with him. He's taking me to Phoenix. That's where Trevor waits.
I'll see you tomorrow morning, I think, I hope...

Super, super, super.
</div>
He saved the message and entered "send".
Pond answered:

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
>>> There is one message waiting to be transmitted.
</div>
Lasse exhaled.
Two hours to Phoenix.
"Okay," he thought, "it's time to disarm the first bomb and send their Bitcoins back."

He opened the terminal program and connected to the computer in the Tor network where the first bomb was located.
He looked at her: 25 minutes to detonation.
Then he typed:

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
bbomb --disarm
</div>
The program responded:

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
Passphrase:
</div>
He breathed deeply.

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
KvTsS#Fwf+w/~pOIet19

Bitcoin bomb disarmed.
</div>
"It's that simple," he thought. "Okay, now wire it back."

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
bbomb --transfer-all 3J98t1WpEZ73CNmQviecrnyiWrnqRhWNLy
</div>
The moment he wanted to press the enter button with his finger, the car was driving over a bump.
Lasse made a sentence up together with the laptop.

"Sorrrryyyyy!" shouted the driver from the front.
"I didn't see them."

The command in the terminal program had disappeared.
Lasse presses the up arrow button to retrieve it.
He wasn't there anymore.
Lasse looked around the car irritated.
He typed the arrow key a few more times: nothing.
He didn't understand.
Orders couldn't just disappear like that.
He looked around, his gaze went outside, then forward.
They were heading for a bend with a hill behind them.

"OOOHH SHIT!" he shouted out loud. Behind the hill, directly in front of them, a flying combat robot floated, slowly turning towards them.
He immediately recognized the model, Reaper MQ-11, at that time brand new, he could stand in the air.
Lasse knew him very well, he was one of his favorite robots, not the fastest, but crazy agile.
His heart began to race.
Twice a short twitch of a fireball on the combat robot.
Lasse instinctively ripped up his arms to cover his face.
Half a second later, two rockets whizzed past to the left and right of the car and hit the ground 50 metres behind them next to the highway.
The shock wave almost pushed the car to the other side of the road.
Lasse held his laptop, the driver somehow made the car stop.
He and the co-driver turned back and stared in a bewildered manner in the direction of the explosions.

"Fuck!" screamed Lasse, "Fuck! Fuck!"
He jumped out of the car, ran back and saw some burning cacti.
Two cars were tipped over on the side of the road.
His heart was racing.
"Why?" he shouted, and: "Shiiit! You know where I am."

He turned towards combat robots.
He had no more missiles and turned around.
Then Lasse looked to the left towards the sky above the horizon.
"Oh!" he thought.
He opened his eyes.
A white giant balloon hovered over the steppe next to the highway, the same as in San Diego over the sea.
Only this time much closer.
It was probably twenty times bigger than normal hot-air balloons.
And it was written on him all over the place:

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
AK768-X5P

sZEf\~ofUA+
</div>
Lasse stared at him and nodded slowly.
He knew about the balloon.
It came from TRON.
But what was he doing here?
Now? Now?
It was a clue for him.
No one else would see him but him.
He was thinking.
The bump had prevented him from sending the Bitcoins.
The combat robot was an intimidation from the LBI, they had probably become nervous.
It was a little greeting: "We are with you and ready for everything."

"Shit!" he thought, "for that, they risk killing people..."
He grabbed his belly.
"But what is TRON trying to tell me?
sZEf\~ofUA+ is a password, sure.
But AK768-X5P? What the hell was that?
I know that from somewhere."

He closed his eyes.
"Wait! Ahh..."

He jumped in the car and typed something on his laptop.
He took no notice of the two drivers on the phone who were excited.

Let him say to himself: "The five-star logs.
We have the logs of one of the combat robots we shot down."
He typed on his laptop.
Lasse: "There!
The combat robot is transmitting to AK768-X5P ... and this is ...
Ha!
That's something.
A note from Sigur:"

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
AK768-X5P - Control Satellite?
</div>

Lasse thought, "Yeah, sure.
This is the LBI control satellite.
Must be.

We got him!
Finally! And an access to it right away.
Sigur must know that.
Right now.
Then we can turn the game around.
It's gonna be a thing.
Yeah, they showed up.
Now we find their combat robots.
This is gonna be fun."

He smiled and typed.

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
Hi Sig,

The AK768-X5P controls the LBI combat robots.
One was just at 34°12'01.8 "N 113°03'40.9 "W, a new Reaper MQ-11.
Can you find him?
The password is sZEf\~ofUA+
I'll get to it when I change cars in Phoenix.
You remember where I am.
But I'll get rid of her in Phoenix.

Lasse.
</div>
He waited until Pond had sent the message and closed his laptop.

Meanwhile, police cars and firefighters were everywhere.
A policeman waved a trowel in front of them.
You should keep going.
Shortly afterwards they passed a sign with the inscription:
"PHOENIX Arizona, 75 Miles."


