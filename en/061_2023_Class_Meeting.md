

## **2023** Class meeting

<span style="font-variant:small-caps;">The whole class</span> discussed lively in small groups throughout the classroom.
Kevin gesticulated wildly in front of some classmates.
Anni and Sophie showed other Annis smartphones and explained excitedly.
A teacher came into the room, stood in the middle in front of the blackboard, looked around and waited for rest.
After a while he shouted into the room: "Now there is peace here!
Sit down.
Take your places.
Everybody!
And that's NOW!"

None of the students responded.
The teacher took a deep breath, waited, looked, then shouted louder: "RUN!
QUIET NOW! GO TO YOUR SEATS!
What's the matter with you LOS?"

No response again.

Teacher says out loud: "I know ... I know what happened.
I know it! I know it!
Marlene was in your class.
I understand you're upset about this.
And that's something to discuss.
But there's a time for everything.
You can continue after class in the big break."

Kevin turned to the teacher: "No, we can't do that only in the big break.
That's it now.
That's more important than teaching."
The class was calmer.
Some turned forward.

The teacher stood with his legs apart in the direction of Kevin:
"I decide happened here, not you, Kevin!
And we're going to class now!
Everybody take your places.
Right now!
I won't say that again."

In the room it became quite quiet and everyone looked to the teacher and to Kevin.

Kevin replied calmly, "No, that's not for you to decide.
This is more important.
Marlene's life is at stake.
And we speak until we know what we're doing for them."

Now it had become eerily quiet.
The teacher and Kevin looked each other in the eye.
Kevin crossed his arms.

Teacher: "Kevin, you risk a reprimand.
You already have one.
There's nothing we can do for Marlene.
She got herself into this and now she has to see how she gets out.
It is forbidden to hack into other people's computers and steal secret data.
No matter what it is.
She wasn't an insider, she didn't work there.
So she's not a Wistleblower either.
We live in a constitutional state and there are laws that have to be obeyed.
Marlene obviously didn't do that.
And he's breaking into the world's biggest secret service.
Besides, people have died."

Anni excitedly, loudly: "I FASSE IT NOT!
I don't believe this.
They were on a school trip with us and Marlene.
You've known her almost 10 years, you know how Marlene thinks.
You know why she does something like that ...
And now you're chattering the media shit.
That's pure propaganda.
They teach us to make up our own minds.
And you fall for some cheap manipulation yourself.
I don't teach here anymore.
No.
Not today and not tomorrow."
She crossed her arms.

"I don't do class anymore either," Sophie said with a horrified expression.
She shook her head.
"It's about Marlene.
No more lessons for me.
No.
Until that's cleared up."
28 students looked at the teacher.

Teacher: "I, um, I..."

Kevin calmly: "I don't do classes anymore either.
"Until Marlene's picture is right again and we can't hear any more shit like that."

Teacher: "KEVIN! ... Kevin!"
He looked around the classroom.
"It's... in fourteen months is high school."

Kevin loudly: "To the shit with the ABITUR!
It's about Marlene's life.
Don't you understand?
They have the death penalty in the United States.
Germany wants to extradite them.
Fuck the high school diploma!"

Teacher: "Kevin!
You don't know what you're talking about."

Kevin pointed the finger at the teacher:
"Yes, I do. I know that very well.
And I mean it.
And you let the media shit your brains out.
It smells like this."

Teacher breathed out and inhaled: "NO! OUT!"
He pointed his finger at the door.

Kevin didn't move.
The students around him stood demonstratively in front of him.
The teacher looked them in the faces one by one.
Everyone looked back calmly and resolutely.
After a while he turned to the door and hurried out of the room.
She fell into the castle behind him with a loud crash.

Kevin stood in the middle of the class and looked like absent towards the blackboard.
His classmates looked at him.
After infinitely long, silent one or two minutes, he said resolutely:
"We're on strike!
We're taking this seriously now.
Who's in?"

"Are you sure about that?" Tatjana, the best in her class, asked excitedly.
"I can't go along with this.
I can't do this."

"Absolutely." Kevin replied.
"But nobody has to go along with it, it's perfectly okay for me if someone doesn't want to go along with it.
But me, I can't help it."

"I'll do it," Sophie shouted.
"For Marlene!"

"Me too," Anni said.
"To Marlene!
But I want to celebrate Marlene too!
For what she did for us!"

Kevin: "Yes!
That's good. That's good.
Marlene and strike."

Lukas: "Horny!"

In the meantime Tatjana had packed her bag and was on her way to the door.
She turned around once more and looked into the class.
Then she put her bag down and said, "Fuck it, I'm staying, too."
Kevin ran up to her and hugged her.

Anni stood in the middle in front of the blackboard: "People!
I've got an idea.
I got it when I read the article."
It got quiet.
"We're sending Marlene a message.
We're making a video.
In it, we tell the world what Marlene really is.
People have to believe the media because they have nothing else.
Nobody knows what Marlene's really like.
We know that.
So we make the video and send it to anyone who wants to help us.
My neighbor's blogging.
That would definitely do the trick."

Lukas: "We send this to WikiLeaks and to the Courage Founndation.
I'm sure they'll tweet that.
You have almost six million followers."

Kevin: "Good idea.
I'm sure she knows Marlene well if she doesn't even go along with it.
Who's doing the script?"

"Sophie!" said a boy.
"Yes, Sophie," another one.

Sophie: "Okay, but you have to help me."
She took her bag, sat down at the table in the back corner of the window, unpacked her stationery and started.
Kevin followed her and sat down in front of her on the table.

The door bursts open.
The headmaster stood with wide legs and crossed arms in the entrance.
Behind him, half hidden, the teacher.

The headmaster thundered: "What is this place like, a pigsty?
This class again!
I told you I had my eye on you.
I told you that last time."
Three or four students turned to him.
"After the irresponsible tree climbing, where the whole class was together on one tree, and then after a teacher quit because of you..."

A student said, "She was totally incompetent."

Principal: "Shut up!
I'm talking now!
After a teacher quit because of you and we spoke, I thought that peace would come.
And now this!
Mr. Brunner has given you a clear instruction.
You have disobeyed them..."

A student: "That was Kevin, Kevin disobeyed her, not us."

Principal: "This is not possible at our school, not in this time.
The teachers bear a great responsibility, and therefore they must be able to say here what happens.
And that must be followed.
Absolutely.
Otherwise, chaos will break out."

A second pupil: "Do they also have responsibility for Marlene?
There are people who want to kill her."

Principal: "Hell!
Marlene Farras is no longer at this school!
We cannot be responsible for everyone, especially not for someone who enjoys himself somewhere in the world and then just hacks into military databases.
She knew what she was doing.
It didn't just happen to her."
He looked in the corner where Sophie wrote the letter with Kevin.
"KEVIN! Will you please come over here?"

Kevin took no notice.
He was engaged in a conversation with Sophie and did not care about what was otherwise going on in the classroom.

A second student to the rector: "Short update: She wasn't amused, she was on the run.
They've been telling us all along that she's having a good time.
That was a great crap.
Extreme shit."

Second student: "For me, everything you say about Marlene now is super implausible."

The rector first looked irritated at the students, then at Kevin and Sophie: "KEVIN!
ARE YOU COMING HERE NOW?
I won't say that again."

Anni: "You don't have to say that again.
He's not coming anyway."

The principal was gasping for air.
He looked briefly at the teacher, then into class.
Then he wanted to go to Kevin, but Anni and some other students stood in his way.

Principal: "Let me through!"

Anni positioned herself directly in front of him: "No!
He's working with Sophie.
You can see that.
He's doing something important.
And you're not disturbing him now!"
Behind her, the group moved closer together.
The rector opened his eyes, looked threateningly at Anni, then at Kevin, then into the group, then again at Anni.

Lukas from the side: "You once told us that we should learn to take responsibility.
That's what we're doing now.
Because the teachers don't seem to.
Marlene's life is in danger.
Don't you get it?
It's not a game anymore!"

The headmaster grabbed Anni's shoulder with anger and tried to push her aside.
She blocked it.

The principal shouted a hand's breadth away from her face, "Go I FROM THE WAY!
This is already having consequences for you.
Don't make it worse."

Anni stood up and looked him in the eye, "No!" She trembled slightly.

Luke took the hand of the rector with power from Annis shoulder: "That does not work.
"Teachers are not allowed to reach out to schoolgirls at this school."

The headmaster pulled out and slapped Luke in the face so that his head flew to the side.
Lukas held his cheek, gasped for air and looked at the headmaster with astonished eyes.
He held his breath and looked back.
Luke took a step forward, turned his head and said calmly: "Do you also want to hit the other side? Wanker!"

The principal turned red.
He tried to say something, moved his hand, but then turned around and left the room, the teacher followed him.


