

## **2023** On Lake Maggiore

<span style="font-variant:small-caps;">Marlene sat with</span> a large, round straw hat and sunglasses on the terrace of a holiday apartment on Lake Maggiore, in northern Italy.
She looked over the lake, to the mountains on the opposite side.
The terrace was overgrown with agaves from left and right.
There were also pots with large plants everywhere on the terrace itself.
She sat in the shade cross-legged on a basket chair, her hands in a meditation gesture, her eyes closed.
After a while she opened her eyes, put earplugs and headphones in her ears, put her legs on a small table, put the laptop on her lap and started typing.

Marlene at half volume: "Hej!"

She listened.

Marlene: "Everything is very quiet here.
I can work.
The terrace is theoretically visible from the other bank, but that's four and a half kilometres.
It's okay."

She listened.

Marlene: "Yes, of course I have heard about the new leaks on Google.
I know those two guys that do that.
It's really unbelievable: The Android people actually managed to clean the back doors for the third time.
For the third time!
And swore twice before never to do it again.
What are they doing?
They completely lose their credibility.
I know some regular people who are going through the ceiling now."

She listened.

"No, that's bullshit, they didn't find those two.
They're still here and active."

She listened.

"Of course I'm sure.
I just got a Ricochet message.
They've found others, maybe they need some scapegoats.
Maybe it didn't run quite clean, maybe they hacked into other accounts internally and sent the leaks out over them.
But definitely, they're still there, not blown out and wildly determined to report new vulnerabilities as long as they're built in."

She listened.

"Yes, that's right.
I agree.
They're heroes.
These are the heroes of modern times.
We need them.
These are the revolutionaries of whom pictures will later be hung on the wall.
First you want to hang them yourself, later you want to hang their pictures.
Jesus, Gandhi, Martin Luther King, Mandela."

She listened.

"Yeah, yeah, sure.
I don't know what I'd rather be.
I don't want to end up as a picture on any wall either.
But it will be.
People love to admire others.
That's more convenient than being a hero yourself.
Actually, we could all be heroes..."

Marlene heard a blow against the metal gate at the entrance and turned around in shock.
She looked.

Marlene: "Wait a minute."

She saw Luigi come in with a big bag.

Marlene: "I'll call you back.
See you soon."
She turned around.
"Luigi!
I didn't know you were coming now."

He smiled at her, put the bag on the table and kissed Marlene on the right and left cheeks.
She smiled and reached for the bag.

Marlene: "Ahhh, Italian cookies!
Mmmhhh.
Benissima."

Luigi was in his mid thirties and had lived for 20 years in Luino, in the Italian part of Lake Maggiore, near the border with Switzerland.
He had created a local hackerspace, not the biggest one, there were only 5 or 6 members, but they had one or two good hacks behind them.
For example, they had built the coffee machine, which first had to be persuaded by encrypted Jabber chat that you now need coffee.
The coffee machine analyzed how active they were in open source projects on Github.
If it wasn't enough for her, then you had to find one of the tricks to get her to cook.
They presented this two years ago at the Chaos Communication Congress in Berlin.

The request to house Marlene had come from an old friend, and he had immediately agreed: How could he not help the woman who had laid the foundation to repair the Internet from scratch, the woman who had shown them the potholes in the data highways?

Luigi: "Did you see?
Your class sent you a video via WikiLeaks."

Marlene: "No.
You did?
My old class?
Now? Now?
What are they saying?
That I should come back and face a fair trial?"

Luigi: "No, not at all.
I think what they say is super cool.
They want to celebrate you and strike until your public image is corrected again.
They're celebrating that you hacked the vulnerability database and are on strike against propaganda in Germany."

"What?!?" Marlene took her legs off the table and started typing something into her laptop.
Then he looked spellbound at the screen.
Luigi stood next to her and put her hand on her shoulder.
He smiled.

Luigi: "It's good to hear something like that, isn't it?"

Marlene looked at Luigi briefly from the corner of her eye.
After a while she put the laptop away, got up and went to the terrace railing.

"Not there!" Luigi shouted, "not to the railing.
"That's visible."

Marlene stopped.
Tears rolled down her cheeks.
She grabbed the railing with both hands and closed her eyes.
Luigi came and put his arms around her from behind.
So they stood for a few minutes.
Then he gently pulled her back.

Luigi: "Come on! Out of sight."

Marlene detached herself from him and sat back in her basket chair, took the laptop and watched the video again.

Marlene: "Do you have a piece of paper and a pencil?"

Luigi: "What?"

Marlene: "Paper, pencil, just normal, like in school."

Luigi: "Yes, I think so."
He went away and came back with a pad and a pen.
Marlene put the block on her laptop and started writing:

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
Dear Sophie, Anni, Kevin, Aisha, Lukas, Janis, Lisa, Leonie,
Felix, Berem, Jannik, Jannick, Philip, Moritz, Antonia, Aylin,
Johanna, Tom, Jonas, Devin, Marie, Lena, Nils, Marwin, Emilie,
Viviane, Tatjana,

I'm sitting here with tears in my eyes because I just saw your
I saw on video. It's so good to hear from you. I knew
really not whether I lost you or not. And you
seems to be the only one from my time in Berlin
really know - except for a few hacker friends, who stayed with me.
are. Some have also turned their backs on this. We have powerful
Opponents.



Your video will be seen by the public. I think you guys
you're gonna get a lot of headwind. In
Internet rages a full war, with dead, wounded, prisoners
and tortured. You've all been separated for two years.
is monitored. All your cell phones, all your accounts, Facebook, WhatsApp, e-mail,
also Threema, Telegram, even Signal and Xabber. That makes
a small program directly on the smartphones. On smartphones
nothing is safe if you're under direct supervision of a large
of the Secret Service. And you do. The best thing you can do
to secure laptops. I'll ask somebody if they'd like to join you.
and help you set up secure communications.


Prepare for much, much headwind. What you're doing is
great, but it's a real danger to some powerful people that
they definitely want to get rid of. You're dangerous because you're
Attention creates, arouses interest. That's their biggest problem.
They don't want us to notice what's happening in the world right now.
But you can also be sure that from now on.
will look out for you on the Internet who want to protect you.
They'll do things that will give you a greater chance of being safe
to get out. Your adversaries, our adversaries, are also frightened
not back from murder. That's why: Get out in the open, make a public statement.
Noise, people seeing you. You need TV, press, Twitter.
... They've got respect for the public, they've got a hell of a
Fear of the anger of ordinary people. That's their real danger.
They need the normal people, they need you, because somebody
I have to do the work, pay taxes and buy things.


I think you have one big advantage. You'll be surprised,
that a whole class is closed to more publicity.
Truth, strikes. That's strong. They'll be here for a few days, or even days.
It'll take them weeks to learn how to handle it. This is your
Chance. Until then, the world must know you. Then you can go through-
come. They'll want to separate you, try to make each other
to make enemies of their parents, siblings, friends, teachers.
rer. Don't let them scare you, and don't let them promise you hope.
to make a change. And use the best weapons there are: PGP, gate,
OTR, Tails, Cubes, Pond, Ricochet and Veracrypt. And Linux or BSD
of course, only Linux or BSD!


All the best.
I hope to see you again soon ...


Marlene
</div>
Luigi: "Hello!
You're right, I did.
You're in great danger now.
But we can do that.
I'll take your letter, scan it and send it to WikiLeaks.
Your famous mailbox is working again."

"Can you do it from here?" Marlene asked.
"I don't want the leaf to leave the apartment."

Luigi: "Okay."
He went away and came back with an older digital camera.
He showed it to Marlene.
She nodded.
Then he took a picture of the letter.
Marlene took it, went to the patio fireplace and set it on fire.
Then she went to her laptop, plugged in the camera's SD card and started typing.


