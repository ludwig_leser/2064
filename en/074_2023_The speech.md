

## **2023** The address

<span style="font-variant:small-caps;">The picture in the school class</span> had changed.
Everywhere now lay black, somewhat clumsy laptops on the tables, many mini computer boards, cables, power supplies, USB sticks, screwdrivers, a soldering iron.
Everywhere small groups sat around one or more laptops.
They typed, discussed and gestured.
There were Marlene pictures all over the windows.
Kevin stood at the front of the table with Sophie and gave an interview to a journalist in front of a television camera.

Kevin into the microphone of a reporter: "Yes, a lot has changed for us!
We have so much attention.
A lot of school classes are looking at us.
It's good for Marlene.
But I also know what Marlene would say if she were here:
"What do we do now?
Waiting is not enough."
Attention is one thing.
Strikes and celebrations?
Yes.
But that's waiting.
And that's not enough.
And that's why something else has come to the fore for us.
We want a new form of school.
We want to change school.
Look around you!"

He pointed into the room.

"We have all learned more together in the last three weeks than in the three years before.
And I really mean it.
Not only technically.
Also human.
Working together, trusting each other, helping.
And that's because it's serious for the first time, because it has to do with real life.
Most people here did not know how easy it is to work with hundreds of thousands of people on the Internet all over the world.
To work properly together.
We did not know that there was a whole culture of knowledge sharing and mutual help.
Open Source, Git ...
It's sooo much fun to help others.
And get help.
And to see how when what you do is used by others.
We haven't had any of this in our school in recent years.
No git!
The whole school without git.
None of it.
And Git is at least as important as Linux.

If you compare this to a normal situation in any class:
Most of the time we are not fully involved in the lessons, that is a lot of waiting.
Here we're always at it, here we live.
Everyone's wide awake.
And we are visited by super people, really well-known people, especially from the hacker environment, but also more and more others.
They tell us what they know about computers, programming, computer management, our legal system, our political system or whatever.
The one they are excited about and know more than others.
We also had lawyers, policemen and entrepreneurs here.

It's a super trusting atmosphere.
When someone doesn't understand something, they ask.
When someone doesn't care about something, they go somewhere else.
Instead of the many imaginary examinations of teachers, we now have real life as an examination.
This is real.
Exams are even fun.
We have attacks on our computers here.
We need to communicate undercover.
Can we make our computers so secure that they won't be disturbed?

We've learned a lot, for example, how to turn a mini-computer like this into a high-security web server in twenty minutes, and nobody outside knows where it is in the world.
We learned how to make a laptop pretty tap-proof.

When this action is over here, I definitely want to continue with school, and that's what the others here want as well.
Personally, I don't give a damn about my high school diploma.
It's all a fake, it's all made up.
There's no real value in it.
Which means graduating from high school.
That doesn't say anything.
And that's not really important to anyone - just a sham."

Reporter: "Aylin, Jonas and Devin have left the group.
What do you say to that?
Are there any other students who are considering aborting?"

Kevin: "Aylin's mother had a nervous breakdown.
She is a single parent and has just lost her job, so Aylin didn't want to stay here.
She'll be back as soon as she can.
Jonas and Devin, the pressure was too high.
The parents didn't really go along with that either.

Our opponents are NSA, BND, other intelligence agencies.
For them, this is an unpredictable mess.
Marlene's right, really dangerous for her.
And we're in on it now.
Because when Marlene becomes known, more people look at what is actually going on, and no secret service wants that.
There are now over 200 classes in Germany celebrating and striking with us.
And there are more every day.
Now imagine what will happen in the secret services if we manage to straighten Marlene's image in the public eye ...
If most people understand that she was all a spectacle."

Reporter: "They have other goals in the meantime. They demand the abolition of compulsory education and free choice of teachers."

Kevin: "These are not "demands".
It's common sense.
Do we need compulsory schooling?
No one has to force me to learn.
I like to learn.
Just not pointless stuff from people who haven't seen life.
Look around you!
Does any student here need compulsory schooling?
Should any student here spend even one lesson with a teacher he is not interested in?
That's bullshit!
We are all old enough here to choose our teachers ourselves, and the teachers are also old enough to choose their students.
We learn here at a speed that is breathtaking.
Real talents suddenly show up here.
It's a great learning environment.
When I imagine what we would have done and learned if we had had such a school all these years ...

This cannot be the case in a hierarchically structured school system in which it is specified what we are to learn, when we are to learn it, from whom we are to learn it, even how.
People who have been teachers their whole lives and don't know anything about life.
That's ancient nonsense.
Nobody here wants that anymore.
I don't think you'll find a class in the whole of Germany with five students who would prefer that here."

Reporter: "You say you want to straighten Marlene's picture."
But you can also see this as something illegal if you penetrate military computers and steal documents there.
We need agreements in a democracy as to what is permitted and what is not.
You can't just ignore that then..."

Sophie turned to the two of them.

Sophie: "The documents that Marlene handed over to the public belong to everyone!
They concern us all.
That's why they belong to all of us.
We live in a democracy and without these documents I cannot form a democratic opinion.
I can't just say of anything that belongs to everyone that it's classified now.
And then keep the reasons for that secret.
If someone kidnaps your child, do you have a right to break into the house it is in and get it out?
Of course you did!
Democracy is more important than tactical secrecy."

Reporter: "Wait a minute!
You don't have the right to break the law to get your child back."

Sophie: "If you don't harm anybody with it?"

Reporter: "I think you have to tell the police, and they can break into the house."

Sophie: "Then this is a mistake in our laws that has to be corrected.
A bow.
Think about it: There's the house, you don't harm anyone, maybe break a window and get your child out.
That must be allowed.
And if laws allow the secrecy of documents that have significance for the public, then that is also an error, like an error in a computer program.
We need to correct such laws and publish new law versions.

And in the secret services there are apparently so many undemocratic and anti-democratic activities that it would be the worst thing for them if these activities came to light.
And that's why they're trying everything to stop us.
They're dependent on secrecy.
Imagine if we could prove that the intelligence services were responsible for Aylin's mother being bullied and released.
And that they put even more pressure on her after that, scared her.
That she collapsed because of it."

Reporter: "Other countries see it differently.
In England, Marlene is seen in the media as a terrorist."

Sophie and Kevin looked at each other.
Kevin shrugged his shoulders: "This is England.
I don't know the situation there."

Reporter: "What are your next steps?"

Sophie: "We are networking better and better with the other classes, now throughout Germany.
There are 217 classes on strike.
And even more consider whether they also take this step.
We're getting a lot of popularity.
Many students use our new, pretty bug-proof communication system."

She smiled.

"I think even WhatsApp and Facebook are noticing that by now.
In total, there are about 400,000 school classes in Germany.
This is what we are currently laying out our server capacities on.
In our team are some boys and girls from the top league in the hacker environment.
Marlene is also actively involved.
I don't know how she does it, but she's in.
They'll get us everything we need.
We'll get the best equipment there is."

Reporter: "If you now have intelligence services against you, won't you feel queasy?
Aren't you afraid?"

Kevin: "Of course we're afraid.
Plenty.
But we also have courage.
Marlene has courage.
She's still active, she's not using all her strength to hide.
She helps <em>us</em> actually more than we help her.
Well, with our courage, we can add a scoop.
And then we'll have you as a life insurance policy.
If you report about us, if you make us known, then secret services are more careful with us.
Our lives depend on what you send and write."

Reporter: "Your life?"

Kevin: "Sure.
Our lives."

From the other corner of the room someone shouted: "The American President is on TV!
This is the speech to the intelligence services."

Everyone turned to the big screen hanging at the back of the school class.

"Louder. Louder" called a student.

Kevin gave Sophie and the reporter a look: "Now I am curious what this will be."

"Rest," called another student.
Everyone looked spellbound at the screen.

President: "Dear compatriots, this is my weekly address on our situation here in the country and worldwide.
Today I want to talk mainly about a current topic, and that will be a little different than usual.
That's because even in the best family you can disagree, you can even argue.
And I don't mean Republicans and Democrats now, because there's traditionally a lot of fighting between us."
She smiled.
"No, I mean institutions that form the backbone of our great nation:
our global corporations, our intelligence services, and the U.S. political center: our government organization.
Our companies lead the technical development worldwide.
Our secret services are the best and most powerful there are, and have ever been.
They're putting their lives on the line so we can live here in peace in our own way.
You've done a great job over the last few years.
But terrorism has challenged us more and more in recent decades.
Much of this battle is now taking place on the Internet.
In order to keep the Internet a safe place for all of us, our secret services have built up a surveillance system in recent decades that reports us very early when terrorist attacks are being prepared, riots are taking place or attacks on America are being prepared.

But in these efforts we have also overstepped the mark in some areas.
Against the will of corporations, intelligence agencies have built surveillance software into programs like Facebook Messenger, WhatsApp, YouTube, Instagramm and so on, making the communications of billions of people more insecure.
Many people have lost confidence in our companies and have turned their backs on them.
We are of the opinion that some of these measures went too far.
We're just starting some programs to analyze and correct that.
I would like to present one of them today: a seal of approval like the "Energy Star", which is intended to restore confidence in our world-class products. The security star.

Everyone in the room laughed spontaneously, running, celebrating against each other.

One of them shouted: "NSA-free! Now 100% guaranteed."
And laughed out loud.

President: "Any product bearing this seal is free of rear doors and intentionally installed vulnerabilities."

More laughter in the room.

President: "Most of the American companies I've talked to about this are willing to do so.
They will subject their programmes to regular, rigorous state scrutiny.
The secret services, I can say frankly here, were not enthusiastic about the plan, but in the end they accepted it.
There is no legal way for them to intercept and decrypt the communication of such programs."

Lukas: "Then they'll make it illegal..."

Kevin: "Lukas, there is already something new about what she says.
This means that secret courts like FISA are no longer allowed to give permission.
The companies won't let her in anymore.
Google is pissed off at the secret services, if they don't have to anymore, they won't do it either."

President: "I know a lot of trust has been lost.
I am willing to fight for that trust to return.
Therefore I will bring in the next days a bill into the congress, which makes all reports of technical safety gaps, weak points, back doors exempt from punishment.
Wistleblowers who report such vulnerabilities are guaranteed by this law that they will not be prosecuted.
Even if they are company secrets."

It got quieter in the classroom.

President: "And today I signed an amnesty order for all Wistleblowers of technical security breaches that are currently in detention or on trial in our country.
That's about 240 of our toughness at the moment.
You will be dismissed as soon as possible and entitled to compensation."

It was quiet in the room.

"Wow," Oskar said in silence.

Sophie: "What is this?
I don't trust the roast."

Kevin: "They dismiss all ... Hello!
That's great. That's great.
You have to do it now.
The president will see to it
It's the weekly national address.
That's awesome!
That's really awesome!
Even if that doesn't mean they've gone from wolves to sheep now."

Sophie: "But then how can we trust them?"

Oskar: "We can't do that.
But the situation for the Wistleblowers has improved.
A few people I know will now dare to report vulnerabilities that have not dared to do so before."
He pointed to the TV.
"This, this is a disaster for the intelligence services."

Sophie: "What about Marlene?"

Oskar: "I don't know.
But that's good for her, too, I guess.
She's not actually a Wistleblower."

Kevin: "That sounds a bit like internal war."

Oskar nodded.

Oskar: "Yes, they argue internally everywhere and about everything."

The reporter approached Sophie and Anni.

Reporter: "Sophie, Anni.
May we interview you about it for the evening news?
I think this is gonna be a report."

Sophie: "Yeah, sure."


