

## **2023** One drop of blood

<span style="font-variant:small-caps;">Marwin looked at the door</span> and thought:
"Still no guards... It's been at least half an hour since Linda fled the room.
Yes, she escaped.
But from what?
Certainly from saying anything about Marlene ...
Shit.
Shit.
shitty
What about Marlene? ...
Linda knows where she is ...
Yes.
She doesn't want to find her.
She doesn't need me to find her because she knows where she is.
But then what does she want?
Why release me?
It doesn't make any sense."

"Damn it!" he said out loud.
"Fucking hell!"

He put his head on the table.
His nose started bleeding.
He crumpled a piece of handkerchief together and pressed it into his nostril.

"Marlene is forcing her to release me," he thought, smiling, "Yes, that would suit her."
He nodded.
"She has no limits.
Nice thought, but, no.
They won't let you blackmail them.
You can't.
Not on principle.
You are the greatest world power, the greatest country there is.
At all times has ever given.
They are all part of a miracle dream, the American dream, the land in which every person can bring it to the top.
No, there can't be an 18-year-old girl coming and forcing her to do anything."

He buried his head under his hands.
"Or was it really a change of heart?
Linda had indicated something: They want to dissolve the NSA ...
No, that's bullshit.
Even less likely.
Only if they have something better than NSA and they don't need it anymore.
Certainly not because they are recalling their ideals: On freedom, self-development, letting the other live.

Or are they starting to take their own ideals seriously?
To take yourself seriously?
To relate the ideals not only to their country, not only to a part of the people, but to the whole of humanity?

No.
Your goal is to subjugate the world.
It's the only thing that makes sense to them in a crazy world.
Order through power.
The strong can do his bidding, the weak his not.
That's what they get with mother's milk:
If the world is to continue well, we must control it, with our armies, with our financial industry, with the arms industry, with the Internet industry, with fast food, Coca Cola and with the mass media.
Control every aspect of life.
That's the target.
And once you've achieved that, you can guarantee people's freedom - at least from the part of people who are positive about them.
The maximum happiness for the maximum number of people."

He punched the table with his fist: "Fuck".
He thought, "That's the most demonic saying there is, because it sounds reasonable, and it's so hard to see through.
If one can exclude people, then the greatest country that has ever existed excludes all those who do not submit to its culture, its ideas of life.
Anyone who wants to live differently is an enemy of freedom, a danger.
For example, environmental activists, human rights activists, hackers, especially cypher punks."

He grabbed his head with both hands.
"By force to harmony ...
With oppression to freedom.
What makes people think such idiotic things?
What makes hundreds of millions of people, in governments, in state apparatuses, in companies, think such nonsense?
Why can they think that power can solve problems, concentration of power, in our time?
Power must fall into the hands of the people who actually do: program, build, plant, clean ...
Power must spread like water over a watering can.
Do not unfold as in a dam rupture."

He breathed heavily.

"And because of this error of thought I have been sitting here for two years now, completely senseless, without charge, not even an accusation, not even a false accusation.
If only ten, twenty percent of the population were real hackers, cypher punks, people with cypher punk hearts, things would look different."

He looked around the room and got stuck on the table with a little book and a pen that Linda had forgotten there.
He looked at her and then suddenly shouted, "Yah!
That's right.
A manifesto!
Like Eric Hughes in 1993.
I'm writing a hacker manifesto, a new cypher punk manifesto.
That's what it is.
And if no one ever reads it..."
He pulled the booklet towards him, tore out all the pages already written and arranged them in a pile on the table.
Then he opened the first page and took the pen.
He thought, "Eric Hughes ... Encryption" and began to write:

<div style="background-color: #f8f8f8; color: darkblue; padding: 20px; margin: 20px 0; width: 650px;">
Everything starts with encryption.
</div>
He looked at the first sentence, nodded and continued writing.

<div style="background-color: #f8f8f8; color: darkblue; padding: 20px; margin: 20px 0; width: 650px;">
1 -- Encryption like mathematics cannot be destroyed, not by armies, not by intelligence services and not by the best companies. It is the basis of our personal freedom. We need to be able to choose who gets our information and who doesn't. We must be able to hide, elude the gaze of those who want to destroy what we create. Every beginning needs protection.

And we need free software that is not manipulable because it is protected from change by encryption, and everyone,
can track any change to it.

The universe gave us the encryption.
</div>
A drop of blood fell from his nose onto the paper.
It looked at him with fascination and made sure that he did not walk, but dried as he was.


