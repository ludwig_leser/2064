

## **2023** Temple riddles

<span style="font-variant:small-caps;">Lasse galopierte in voll Ritterrüstung</span> with his armored horse towards the city gate.
He swung a big morning star and roared as loud as he could: "Attackeeee".
Around him, foot soldiers with spears, swords and shields ran towards the gate.
On the background, catapults shot burning bullets into the city.
Throughout the width of the city wall, soldiers in armour could be seen shooting arrows, throwing stones or trying to push back soldiers climbing up ladders with lances.
Above the gate, a group tried to pour a huge bowl of hot pitch over a group of soldiers with a battering ram who tried to smash the gate.

Lasse stopped his horse abruptly about 30 meters before the gate and pointed with his morning star towards the gate.
A large, light blue-green-violet ball bolt came from the top of the morning star, hit the city gate with force and exploded in a huge cloud of fire.
Parts of the gate and the city wall flew around.
When the cloud of fire dissipated, there was a big gap in the wall where the city gate was before.
Lasse pointed with his morning star in the direction of the gap.
A second ball lightning went off and exploded in the first house behind the former city gate.
From different sides, four knights galloped towards the gate, surrounded by hundreds of foot soldiers.
Lasse gave his horse his spurs and stormed into town with the others.

The foot soldiers climbed the city walls from the inside, penetrated into the houses and hunted the remaining soldiers of the city.
Lasse and the four other knights gathered and looked around, where their intervention seemed necessary.
There was nothing more to do, the foot soldiers would do the rest of the work alone.
And so they turned to the weapon tower that stood at the edge of the marketplace.
Each knight took his main weapon.
Lasse fired another bullet bolt, another knight from some sort of trident fired an ice beam, another threw a giant Thor hammer.
A knight raised his hands and a rain of steel chunks fell on the gun turret, the fifth knight shot edged steel balls that triggered explosions.
The weapon tower began to burn and finally exploded in a huge colorful cloud that spread all over the city.

The knights had already turned them around and trotted towards the temple that stood in the middle of the market place.
It was pentagonal, with a portico on each side.
The whole temple was protected by a blue cone of light, which embraced it completely and reached in its top up to the sky.
The knights jumped from their horses, took off their helmets, opened their armour and laid them on the ground.
Then everyone went to one of the five sides of the temple, each in front of an entrance, still outside the cone of light.
All stepped together into the light of the cone towards their entrance.
A knight was spraying sparks and you could hear a threatening crackle.
Then he was thrown out with a bang.
The other four knights left the cone of light and ran to the knight lying on the ground.
He rose and the others laid their hands on his shoulders.
You could see a yellow stream of energy flowing.
Then all went back to their positions in front of their entrances.
This time everybody came through the blue light veil without any problems.

Lasse entered the temple hall.
The hall consisted of a large dome-shaped room divided into 12 sections by double columns.
In front of each section stood a massive marble table.
In the middle stood a kind of round altar, with a large floating crystal ball above it, which shone matt in various colours.
The colours moved slowly, like currents in a sea.
One of the 12 sections shone brighter than the others.
Lasse went there and stood in front of the table.
He didn't see the other knights.
In magic temples one was always alone, but only five people could enter them.
The others were here too, only in their own dimension.
Lasse knew that what they were doing could have an influence on what he experienced, but he had not yet figured out how.
In higher levels here in the temple one even had to work together across the different dimensions.

But for Lasse it was only about solving the thought error riddle of the temple, his first ever.
He had already tried twice to solve this riddle, but had failed twice before even the actual thought error revealed itself.
Otherwise he would see him in a designated field in the middle of the wall.
The spot was still empty.
So far, he had only managed to find the first six thought steps and put them in the right order.

Each temple in the game was responsible for a cultural thought error from the beginning of the 21st century.
These were thought errors that were anchored in the culture that everyone learned from an early age and of which no one noticed that they were thought errors because almost all people thought so.
In the age of knighthood such a cultural thought error had been that the earth was a flat disc, surrounded by an ocean that ended somewhere in a giant waterfall.
And people who already knew at that time that the earth was a sphere and the people on the other side were standing on their heads were considered crazy.

Lasse took an orange stick and put it on the table in front of him.
In the six sections to the left of his, six sentences appeared in orange, slightly luminous writing.
He turned to the first sentence.

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
(1) How we live together in society is increasingly characterized by a kind of paternalism.
We want to change the people who don't behave "right".
We have less and less confidence in individuals.
The state must use its power to ensure security,
Politicians should always make better laws and regulations,
that serve security and to which all should submit.
</div>

Lasse thought: "That sounds like fear of loss of control.
The others are unpredictable and that's why you need rules.
Uh-huh.
I can think that too."

He turned one section to the right.

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
(2) That's because man,
even when he's born,
has a natural selfishness,
who needs to be restrained,
that you can't just let go,
without ending up in chaos and insecurity.
</div>

Lasse: "Yes, a world that you leave free to children ends in chaos.
Okay. Okay.
But are children selfish?
Not until they're self-aware, definitely.
Then they can't yet relate the world to themselves.
But you can see that children are selfish."

He went to the next section.

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
(3) Of course, the actual goal remains the same,
that everyone get as much freedom as possible.
But man can only be free in security,
when he feels safe.
</div>

Lasse: "As much freedom as possible ...
That sounds dangerous.
And then freedom gets tied to a feeling... that you feel safe.
Twice as dangerous.
Hmm.
But understandable.
You'd think so."

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
(4) There is a deeper meaning to it,
that the individual is protected by law and order,
but when people are punished for it,
who don't play by the rules,
will end up in a system of threat and fear.
And as humans, we don't really want to live in threat and fear.
</div>

Lasse: "Exactly!
We need laws and we need to protect them.
That's right, I did.
But not by punishment.
That was the main mistake here.
If someone messes up, then I punish him so hard that he decides next time not to do it anymore.
And others who see that he's being punished do it right from the start.
Then they do not do it because they understand what is right and what is crap, but because they submit.
People can't submit well.
This won't work in the long run.
But if you're punished, it only works for a short time and then the boomerang comes."

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
(5) People with whom we live in trust and openness,
we don't want to use punishment, education and other means to get it to happen,
to behave "right".
We'd better trust him on that one.
But when someone breaks the law in public life,
threatening the state or other people,
it's important to us to get him to,
to return to positive behavior or at least to abide by the law.
</div>

Lasse: "Yes, yes ...
It's hard to believe you can trust someone you don't know.
Then I'd rather not trust you.
How can you even trust people you don't know?
Difficult question ...
... on the other hand, you only have to look at the Open Source movement.
It's not about trusting someone not to screw up, it's about trusting us to be able to handle that when they screw up.
Linux has experienced millions of bugs and yet it is the most stable operating system of all."

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
(6) If we believe,
that someone is acting destructively,
because he's sick,
then it's the target,
that he will be cured.
I want him to have time for that.
But if he's not considered sick,
if he chooses to do so of his own free will,
to do desktructive things,
then he must be permanently forced through threat and control,
to obey the law.
</div>

Lasse: "Of my own free will ...
When do I want to be destructive?
If I consider anything absurd,
what to do,
if I don't see another way out,
when I get angry and don't know where to put it ...
But I don't want to do evil then just as an end in itself,
I want something to stop,
I want to get rid of a threat.
Unless I'm the Joker.
But he's really mentally ill."

He turned to the free sections, where now a sentence in white appeared.

Lasse read it.
Turned from one to the other.
And suddenly he said, "YES!"
He went to one of the sections, stood in front of the table and said out loud, "You're the next step."

The white sentences disappeared and the sentence he stood in front of turned orange.

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
But the price for this education,
for wanting to change the other,
is high:
This means that there is no autonomous freedom in social coexistence,
no freedom that is based on trust and where everyone decides for themselves,
what he can and can't do.
And that's how we want to live.
The fact that this did not happen is due to a cultural error of thought:
</div>

Lasse clenched a fist: "YESSSS!"

He breathed deeply.
That was the seventh step.
He turned to the crystal ball in the middle of the room.
It began to glow and the colours moved faster.
A beam appeared that shone out of the sphere into the room and moved to the section in front of which Lasse had stood at the beginning.
The beam moved to the free field in the middle of the section and began to draw a green font.

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
Man acts selfishly because he was born selfish.
</div>

Then the beam changed its question to blue and wrote underneath:

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
Man acts selfishly because he ends up in fear.
</div>

Lasse: "YES!"
That was his first revealed thought error.
That was the first half of what was to be done here in the temple.
The second was to solve in.
The corrected thought appeared by itself.
What was missing was the train of thought to understand the corrected thought.

The orange texts in the other sections had disappeared.
Lasse put the orange stick on the table and took the blue one for the first time.

Three blue texts appeared in three of the sections.

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
And that depends mostly on it,
if we had enough adults around us in our childhood,
from which we could learn,
how a good autopilot system works,
Adults who were observant,
have promoted us,
have shared their experiences with us,
in a familiar, secure atmosphere.
Or whether this atmosphere was already characterized by fear and autopilot reactions at that time.
</div>

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
Inner confusion, despair, unprocessed shock experiences,
Ideas that do not correspond to reality generate fear in different life situations.
</div>

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
Such a form of freedom therefore works wonderfully,
because we're perfectly prepared for it,
because we all had some primal experiences at the beginning of our lives,
that give and take freely,
invest the trust in ourselves and in our environment in us.
</div>

Lasse looked at her and said after a while: "Sure!
The beginning is always simple."

At that moment, Mostafa appeared.
He fluttered around a little in the dome until Lasse opened his flat hand.

Mostafa landed and said: "Playing!
Cocoa at the Cypherpunk Cafe."

Lasse: "Let me solve quickly.
I know the first blue sentence."

Mostafa: "Can you do it tomorrow?
Everything is stored here."

Lasse: "Saved?"

Mostafa: "Yes, of course, if you have revealed the thought, you can come here at any time.
You are now a candidate here in the temple."

Lasse: "Cool.
Okay. Okay.
Let's do it."

Mostafa: "Think your way to a cafe."

Lasse: "Okay!"

In a bolt of lightning the temple disappeared and Lasse sat at a cafe table under the open sky on top of the Cypherpunk Academy.
He marvelled at the view.
The next mountains were certainly 30 kilometers away.

Sigur's place opposite him was still free.
Lasse asked towards the free chair, "Well, where are you right now?"

