

## **2022**** Sharing is Caring

<span style="font-variant:small-caps;">Marlene and Fred</span> in a room in the attic of Hans' wooden house.
There were four laptops on the floor and the desk.
They both sat and typed.

Fred: "Good Internet here.
How do you conceal using Tor?
It might be noticeable if someone in the middle of nowhere suddenly sends a lot of goal data to the Internet, and Australia is not exactly a private-sphere friendly country."

Marlene: "Quite normal: via Tor Pluggable Transports.
There you can choose from a whole range of how you want to go into the goal net.
And change again and again.
I'm in the process of building my own entrance.
So far I mostly use the web or Skype option: Either it looks like I'm surfing normally or I'm skyyping with someone, but in reality I'm getting and sending gateways.
Teenagers abroad surf and Skype a lot."
<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
In some countries the use of Tor is not allowed.
For these cases, Tor offers a function called Pluggable Transports.
The user then no longer dials directly into the Tor network, but into a bridge server.
Communication with the bridge server is then not the Tor protocol, which tells you that someone is using Tor, but a different protocol.
That looks like some unsuspicious connection to network administrators, like a Skype call.
In reality, the connection contains the hidden Tor data.
</div>

Fred nodded with an afford smile and typed something on his laptop.

Fred: "Okay!
Data is ready.
I'm ready.
I can now look up which vulnerability exists for which computer or operating system.
Who's first?"

Marlene: "As I said: Tor, Tails, Cubes OS, GPG, the OMEMO projects and Veracrypt.
<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
GPG: Gnu Privycy Guard, the most common command line encrytion tool in the linux world, unbreakable encryption, active open source project.

OMEMO, Multi-End Message and Object Encryption: Encryption for messenger apps with forward secrecy (if one message is broken, all others remain save) and encrypted group messaging.

OMEMO Messenger: Signal, Conversations, Whatsapp ...
</div>
We send the weaknesses of all your projects to you.
I've already warned the core people:
There's an earthquake coming, and a deluge.
They know exactly what happens if the information goes to the wrong people right now.
And they also know how to prevent it."

Fred: "Are we sending everything?"

Marlene nodded: "Yes, of course, everything about her projects.
Everything from the database you made.
It is important that there are no more gaps in the programs before things get really big.
We must be able to communicate."

Fred: "How much time do we give them?"

Marlene: "Let them say that themselves.
Three days, I guess."

Fred: "Three days?!
Are you out of your mind?
How can they fill the gaps in three days?"

Marlene: "With the six projects there are not so many.
They know exactly what to look for.
They usually react immediately to security vulnerabilities and are highly active in their projects.
But we ask them.
You won't say much more than three days.
I'm still writing that we're under time pressure."

Fred: "And after that?
I have here an info package for different Linux variants, for BSD, one for Mozilla, especially Firefox and Thunderbird, KeePassX and a few others."

<div style="background-color: #dfd; color: black; padding: 10px; margin: 20px 0; border-radius: 5px;">
Thunderbird: E-Mail client with encryption plugin (enigmail)

KeePassX: Password Manager
</div>

Marlene: "Yes, that sounds good.
I now have a lot of current Ricochet addresses from all kinds of teams.
We will send them as soon as the vulnerabilities in the base programs are gone and we all have the new versions.
These teams need more time.
That's a lot of stuff.
And then let's get on with all the other Linuxe, Libre Office and hardware things."

Fred: "Lots of work."

Marlene: "Yes."

She's typing on her laptop.
"Okay... so... the messages to the first six teams are out.
I think we'll get a quick answer.
Now tell me, how was it with Anita and the terabyte?"

Fred: "Yes!
Anita and the terabyte.
It's a story.
So, after meeting everyone where you were in Boatly Hackerspace:
I get your pond message, which you also sent to Anita, and fall off the chair.
I didn't mean that seriously with the Raspberry Pi in the chat: rather so ... you could do.
Then I get up again, breathe deeply, sit upright and write the Raspberry program.
Not so difficult.
Three quarters of an hour.
Then I test it with a few thousand QR codes, it's okay, make an image of it and send it via Pond to Anita.
No answer, just a normal pond acknowledge.
She got it.
Good, good, good.
Next, 22 hours 10 minutes later, I get a message that the files are safe on two 1TB sticks with the question of how to deliver them.
I fall from my chair for the second time, crawl out again, sit upright again and continue reading.
She writes she may be followed by NSA people.
She's not safe, but if they have any leads, she'll be monitored.
Her suggestion is that she gives a stick in a restroom to the toilet attendant there who has been sitting there for 10, 15 years for sure.
She gives her 50 dollars and says that a man will come to her home who will give her another 50 dollars for the stick.
She gave me the woman's house address and said that she usually arrives at home at 6:20 pm.
Well, Anita must know that.
She's at NSA..."

Marlene: "Shit.
Did NSA surveillance help us with this operation now?"

Fred: "Never mind ...
So I'm going there at 6:30.
She opens the door, looks at me from top to bottom, turns around and gets the stick.
I give her 50 bucks and she says 100 bucks!
I close my eyes, wait two seconds and then get another $50 bill out of my pocket.
She makes a face at all this, as if she had just drowned a group of young poodles for fun.
She takes the two bills and gives me the stick.
I've got him in my hand.
My hand's starting to tingle.
I go back into my car and first have to breathe deeply and sit upright again.
And then I'm going straight home.
That's it."

Marlene laughed: "Geil.
Nice.
A toilet attendant."

"Pling," it came from Marlene's laptop.
"Arma answered: Three days!
I told you.
They're great boys!
It's so good that they exist."

Fred: "And girls.
Well, they're all at work now.
There's no lights going out for three days now.
You bet I will."

Marlene: "And they rejoice like snow kings ... finally some real NSA data."

Fred: "They dance in circles..."

Marlene: "Christmas and birthday in one day."

"Pling, it came back from Marlene's laptop.

Marlene: "And Werner Koch too.
Three days, too.
Look: a heart of Werner."
She pushed her laptop to Fred.
Marlene: "He's not so emoptional otherwise."

Fred: "Wow!
A heart of Werner..."


