

## **2064** My robot fighter, your robot fighter

<span style="font-variant:small-caps;">A police car</span> at normal speed on the highway.
The driver in uniform with black sunglasses looked into the rearview mirror.
Phoenix slowly disappeared on the horizon.
He smiled and then said out loud: "The air is clear! You can come out."

A loud knock came from behind the back seat of the car: "boom"
Then again: "Boom ... Boom!"
The driver whistled quietly a melody.
He looked over the rearview mirror to the rear seat and smiled.
"Boom!" the back seat folded forward and Lasse put his head through the opening.
He looked at the driver.
He turned around and grinned, "We're out!"

Lasse: "Geil!
Jesus, Trevor!
Great thing!
And this."

He climbed to the front on the passenger seat and looked around in the car.

Trevor: "What is it?"

Lasse: "Trevor, a cop car?
That's a cop car.
Like the Blues Brothers."

Trevor: "Sure!
With bull engine and bull tyres.
Do you like it?"

Lasse: "Super cool.
Hey, and a satellite radio operator, too."
He put his hand on a big device with antennas in the back seat.
"That's very good.
Then we don't have to take any detours ...
Hello, LBI, they're real snares.
It's as big a bureaucracy as the other secret shops.
They haven't learned a thing.
I used to think the American secret services were the best in the world ... Pah.
Such a simple trick.
With announcement.
And it works."

Trevor: "Nah, they're not the best.
They only have the most money.
We're the best.
Money and secrecy make you tired and stupid.
Especially in ultra-secret intelligence that no one knows.
It's as bad as an ultra-secret encryption program where no one ever controls what it really does."

Lasse: "And they're scared, they're so scared of everything they do.
Nothing can go wrong.
They need a superior force of people everywhere.
Not ten times more people, no, fifty times more.
I didn't think it was that gross.
They've responded to everything I've offered them.
They probably think they'll die if they don't have their bitcoins anymore.
Without money, they're dead.
It's so gross."

Trevor: "Where don't we have to take detours?"

Lasse: "We will turn the tables a little, I think.
You made a mistake.
They showed us one of their combat robots.
And Sigur might have found him by now.
We have an access code - game gift from TRON."

Trevor blinked with his eyes: "What's that over there?"
He pointed to a tiny dot in the sky about two kilometers ahead of them.

Lasse: "What?
Ah, yes.
I see it.
I don't know ... Oh... No ...Shiiiiit!"

Trevor: "What?"

Lasse is excited: "This is a Reaper.
Another reaper.
Probably MQ-11.
Oh, shit.
DEPARTURE!
Let's go!
I didn't transfer the Bitcoins.
We only have a few seconds.
Get off the highway."

Trevor: "Here?"

Lasse: "Drive!
Hurry!
Hurry!
Or no!
Get behind that truck over there.
Now.
Lasse grabbed his arm to the left, but Trevor prevented him from grabbing the steering wheel.

Trevor: "It's good, it's good, it's good.
I'll drive."
He drove the car behind the truck.
A few moments later a flying combat robot hissed past them in close proximity.
They both pulled their heads in.

Lasse jumped on the back seat and looked out of the back window: "Shit!
It flies at the speed of ten meters altitude.
Is he stupid?
This is complete insanity.
They've got crazy pilots."

The combat robot turned with a breathtaking maneuver and looked directly at her car, maybe 200 meters away.
Four missiles were clearly visible.

Leave excited: "GOD!
Four Hellfire missiles."

Trevor: "What now?"

Lasse: "Stop the car!
Side ditch and out."

Trevor made an emergency stop on the hard shoulder.
Both jumped out of the car and threw themselves over the crash barriers into a ditch.
The fighting robot flew over them, much slower this time, turned once more and then stopped in the air turned towards them.
Lasse and Trevor buried their heads under their arms and bit their teeth.
They trembled all over.
But nothing happened.
The combat robot stood there and floated.
Almost as loud as a helicopter.

"Shit!" Lasse shouted in Trevor's ear.
"What's that?
Why doesn't he shoot?"

Trevor shouted back: "Maybe that's Sigur!
You said you had a code."

"Nonsense!" Lasse shouted, "this is a Reaper MQ-11, this is not Sigur ...
That's not Sigur ... that's ... Hey, maybe that's Sigur!"

He jumped up and jumped back to the car, took the laptop and started the jabber chat program.
"Pling!" a message came in.

Sigur: "Hej, don't you recognize me?
Who makes such turns with a Reaper MQ-11?
A CIA pilot?
Forget it."

Lasse: "This is <em>your</em> fighting robot?
You stole it?"

Sigur: "Borrowed!
He was on a training flight, fully armed.
It's like an opportunity.
And then I hid it right away, so they can't track where I am.
I think they're looking for the wreck somewhere already."

Lasse: "Fucking awesome!
What are you gonna do with it?"

Lasse waved at Trevor to get him in the car.

Sigur: "I don't know yet.
It was so awesome to take him, and so easy.
All I had to do was grab it.
Hello, I have full access to the satellite, root.
It controls 29 combat robots as a hidden side function.
Otherwise, he'll be broadcasting TV stations and telephone conversations.
The planes are all in the hangar now, except two.
All they have to do is come out and I can take them.
I'm going to fly mine behind the hill now, maybe it's not so good here right on the highway."

The battle robot flew off the highway.
Trevor crashed into the car: "Police.
Police are coming in the back."

Lasse closed his door and looked back.
The police car came closer.
Then the blue lights came on and it drove out to the right and stopped behind them about 20 meters away.
Two policemen got out and moved towards Lasse and Trevor with guns drawn.

Trevor: "Oh, yes.
I turned off the police radio so we wouldn't be tracked.
That's suspicious."

Lasse: "Shit!
It doesn't look like we could talk to them.
Too dangerous.
Step on it!"
Trevor started the engine and drove away with screeching tyres.
The cops ran back, jumped into their car and followed them.

Lasse activated Jabber's speech recognition so that Sigur could also hear what was being said in the car.
He said, "Sigur, police.
They wanted to stop us.
We drove away."

Sigur replied, "Okay.
I'll look into it." After a short time the robot reappeared in the sky.
He flew about a hundred meters to the right behind the police car, which raced at high speed across the highway with blue lights.

Trevor: "30 miles to Cornville.
Roger gets there around 1:00 in the morning and you can wait in an empty house."

Lasse: "But we're not going there as long as the police drive behind us."

Trevor: "I can't get away from them.
I can't go any faster.
And maybe there's a roadblock coming up.
Shall I turn around?"

Lasse: "I don't know.
Shit!"

Sigur: "Tim here turned on the police radio.
They're already building the roadblock.
She's 25 miles away.
You have to get out of there."

Lasse: "But how do we get rid of him behind us?"

Trevor: "Sigur, can't you just shoot her?"
Lasse and Trevor laughed.

Sigur: "Okay.
You're pretty alone on the freeway right now."

Lasse screamed: "SIGUR! NOT!"
He looked at the combat robot and seconds later saw a fireball flickering and the police car behind them disappeared in a huge explosion.

Lasse and Trevor flinched and both looked at the burning wreck and then at each other.

Lasse, Trevor: "Shit! Sigur!"

Sigur: "Hej, you are alone now.
You are free!"

Lasse: "Get off the highway.
Right here."
Trevor left the motorway on the spot in the direction of a road that passed at some distance.
The car bumped over the grass and arrived on the other side without getting stuck.

Lasse: "Stop the car! Stop the car!
What do we do now?"

Trevor didn't stop and said: "I'll drive you about 20 kilometers from here somewhere into the pampas, we'll find a place protected by satellite, you take my folding bike, that's in the trunk, wait one or two hours and then drive over to Cornville at dusk.
That's the safest way.
They won't be coming right away with thermal cameras.
They're not prepared.
And then I try to get to the car I was going to drive back in anyway.
We're prepared.
Not you."

Lasse: "And, Sigur, let the combat robot crash somewhere controlled.
If they find out we kidnapped him, the whole wasp nest is after us."

Sigur: "They're already after us anyway.
You just ordered three helicopters on internal radio.
You'll soon know what's going on.
I mean, there's rocket parts all over the highway."

Lasse: "It takes them a while to figure out that it was a Reaper MQ-11, that none was missing in the Army and CIA arsenals, none was on the road in those days, and then someday someone from the LBI tells them that they lost one just in those days.
It's all above top secret.
The information doesn't flow that fast.
We'll have at least a few hours of rest.
So now this thing is banging on the ground somewhere in the desert!"



