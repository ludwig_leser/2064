

## **2064** Where's the LBI?

<span style="font-variant:small-caps;">Lasse and Sigur</span> sat again in the kitchen of the games lab.
Each again with a big cocoa in his hand.

Sigur: "And where are you right now?"

Lasse: "At the end of the game I was on a plane home to San Diego."

Sigur: "You got in?"

Lasse: "Yes.
In Caracas, everything is controlled by Fortunato.
He's got a hell of an overview.
And I don't trust him an inch anymore.
And then I can't do much there.
Better from home, from our beach apartment.
I don't even trust him anymore that he didn't know about the robbery."

Sigur: "Neee.
Of course, he's in this all the way.
That was played by him.
But really, see that there are such assholes: They'll cut your belly open and put a remote cyanide capsule in it.
I'd like to know if that really happened 50 years ago.
I really got angry."

Lasse: "Unbelievable, if it were actually like this.
Where are you now?"

Sigur: "Still in the room where the guards took me.
You've been ignoring me for a few hours.
Just stupid.
My skull is booming too and I feel sick.
I feel the pressure in my stomach, gag reflex.
I'm a little sick in really ... even here."

Lasse: "But what now?
What do we do with the LBI?
How do we keep looking?
Did we miss anything?
We don't know much that's new."

Sigur: "We'll try it through the TAO.
We've got two or three access points for robot-fighting hack.
I'm sure they have something to do with them."

Lasse: "Yes, really stupid that we have no idea where they are.
They don't even have buildings, no central servers, no space at all for us to go in."

Sigur: "They use Tor networks that have their data hash networks and use Tails or Cubes OS as their operating system.
They have nothing better of their own.
They need free software.
And then somehow we put a back door in tails and over there we go to their computers."

Lasse: "Are you crazy?
In Tails, a back door?
I think you're crazy.
Firstly, this is against the Cypherpunk ethics of incorporating backdoors into standard programs.
And besides, you wouldn't be able to.
The project is highly active, they look at everything new two, three times before they take it.
And you're not known there."

Sigur: "Hej, that was fun!
You know where I am right now.
How am I supposed to do anything?
Drugs, no computers, prussic acid in the stomach...
It's getting rather boring for me now."

Lasse: "Yes, it will be like Tim.
He once spent two whole match days in a psychiatric ward, in a mini-room without a window, in a straitjacket.
Couldn't do anything ... Oh, shit.
But I don't understand the logic.
Why is Marlene sending us to Fortunato to find the LBI?
And all that happens there is that we find out about it and then..."

Sigur jumped up: "Wait a minute!
Yeah, right!
Fortunato, <em>er</em> is at the LBI.
He works for them.
He's LBI.
Of course I do.
We were in an LBI house.
How does he make such a change from drug baron to information vendor?
Without help?
His system built the TAO, I'm sure of it.
And he's not TAO.
He's got a house to himself, all his computers run privately.
This is an LBI house.
I'm sure it is.
I think we found them."

Lasse: "Shit ... Yes.
That makes sense.
That makes a lot of sense.
And we were about to steal from her..."

He smiled.

Sigur: "Yes ... I'll steal from you.
Sweet!
The Bitcoins should be on their way by now.
I'm sure he looked up what happened in his shop.
If you're at home, after three days you'll get a pond message from me with the key to my raspberry cave.
It'll go off automatically if I don't hear from me for three days.
And from the looks of it, that's probably how it is."

Lasse: "Not too detailed.
Not that I then do anything in the game that I couldn't do out of the game."

Sigur: "You didn't expect me to have a dead-man switch?"

Lasse: "No, of course not.

Sigur: "I don't think you would search the Raspberry completely for clues ..."

Lasse: "Yes, that's what you do when a dead-man switch sends something.
Okay ... And then what happens?"

Sigur: "Then?"

Lasse: "What do we do then?"

Sigur: "That's a lot of money.
Probably over a billion, maybe even more ... Money outside the banking world.
They're Bitcoins.
I don't think they can do without it that easily.
They'll have to pay a lot of people past the normal payment channels.
And now they can't do that any more, or at least much less well.
They'll be after the money... how stupid.
Bitcoins are not anonymous.
We have to be careful how we spend them."

Lasse: "Don't worry, I don't spend it.
We use them as bait.
So we can lure the LBI to us..."

Sigur: "That sounds cool."

He took his cocoa and drank a big sip.



