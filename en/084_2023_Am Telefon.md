

## **2023** On the phone

<span style="font-variant:small-caps;">Luigi at a phone</span> in an internet café in Lecco at Lake Garda, about two hours by car from Luino.

Luigi: "They send someone to us who is in the public eye, someone familiar.
Best with the camera team.
No, definitely with the camera team.
Airport Bolzano.
Tomorrow morning at half past 10 ... Yes, that is necessary ...


Amelie? Yeah, that would be great!


She'll be there, at any rate, you can count on that ... call the Courage Foundation and get confirmation that this is real.
They're right around the corner from you in Berlin ... Yeah, I'm coming, too.
Nobody else ... See you tomorrow."


