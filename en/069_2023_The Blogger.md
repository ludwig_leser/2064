

## **2023** The blogger

<span style="font-variant:small-caps;">Amelie</span> took her cup out of the coffee machine and sat down behind the studio table.
There was a tablet there showing a photo of Marlene Farras in her class.
She looked at it and smiled.
"Joe!" she shouted to the cameraman, "what percentage do you have today?"
"102," he called back while checking the camera.
Amelie laughed: "I know exactly what the two are..."
Joe grinned at her.
The studio was a three-room apartment in Kreuzberg, on the 3rd floor of an old apartment building.
Amelie blogged from here for almost four years, beauty tips first, then other topics like jewelry, smartphones, sometimes yoga or vegan food.
Everything "Out of Berlin" that interested her.
With this she had built up a quite large fan community.

In the last year, hacks due to the publication of the vulnerability database were also a frequent topic.
Hardly any of her friends had had any problems: either with their Facebook accounts or with Instagramm or YouTube.
Once she couldn't broadcast for three weeks because a teenager from Bangladesh had taken over her YouTube account, despite 2-factor authentication.
Thank God she already had over 2 million subscribers and so she got her Google account back relatively quickly.
Others had had less luck.

"Where's my manager?" called Amelie into the room.
She looked around.
In the left corner Freddy, a slaggy Twen, worked on a spotlight, on a sofa in the middle sat Anni, behind a mixing desk on the right Sven, with a serious look and two giant headphones.

Anni shook her head: "You don't have one.
You mean the scripter."

Amelie: "I mean somebody who has a plan here for what comes next."

Anni: "No one here has a plan as to what comes next ...
Ameliiieee!
You know that."
She laughed.

Amelie: "Okay.
We don't have a plan.
Then it's all good.
I just wanted to make sure.
Then let's get started!"
Joe turned the camera to her.
Freddy aligned the spotlight.
Sven raised his thumb.

Amelie: "Welcome! Amelie's Out of Berlin.
Today for once we don't have a real plan what is coming today ...
Who just said it's always like that? ...
Okay. Okay.
But today we have a reason why we can't have a plan, couldn't even.
Because tonight something happened that's got to be on the agenda.
And that's for our big series: WeakENESS DATABASE, huaaahhh.
The big issue security of our data ... or how to keep assholes away from my smartphone.
The day before yesterday we showed you the fantastic video of Marlene's class and Anni explained the backgrounds to you.
Anni, from Marlene's class. You remember. Anni!"
Amelie pointed to Anni.
The cameraman panned to the sofa.
Anni waved briefly into the camera.
Amelie: "Geiles Video.
Important message.
Celebrate and strike.
Yes.
And tonight ... came ... THE ANSWER!
Yes ... of Marlene.
Marlene replied.
And we're sure it was her, not some fake.
Probably from a completely different time zone, because the answer came at 3 o'clock in the morning.
Nobody knows where she is right now.
I read them right away, we set alarms on something like that, and saw right away that we needed a real hacker to translate it for us.
Yes.
And that should be behind the front door now.
I told him 13:03 and 30 seconds.
Then let him come in.
28 ... 29 ... 30.
TA DAAAA."

The camera panned.
The door remained closed.

Amelie: "Hmm.
I thought hackers and programmers loved accuracy.
Anni?"

Anni toocket with her shoulders.
She stood up, went to the door and was about to open it when she suddenly jumped up and Oskar stood in the room.

Ameli: "Aha!
There you go!
Welcome Oskar!
Oskar from WikiLeaks.
Come over here."

Oskar walked towards Amalie with a broad smile and embraced her.

Amelie looked at Oskar, breathed in and out and said: "It makes you feel a bit safer when there's a hacker."

Oskar smiled again.

Amelie: "Oskar!
Something happened tonight, didn't it?"

Oskar nodded.

Amelie: "Marlene has written an answer to the video.
For the first time ever something for the public.
Why not a video?
And where in the world is it when the message arrives at 3:00 a.m.?"

Oskar: "When the message comes means nothing.
I'm sure Marlene sent them with a delay of 3 to 9 hours.
So you don't know where she is in the world right now.
That was their big decision not to go public after the hack.
It's a lot more dangerous, but it also gives her more freedom.
She's still looking for her brother."

Amelie: "It is more dangerous for her not to go public?
Why?"

Oskar: "Very simple.
Publicity, publicity, that's like a life insurance policy for hackers.
The more famous you are, the more people will look when you suddenly disappear.
Some hackers have simply disappeared or died in a strange way, such as Boris Floricic here in Berlin, Tron."

Amelie: "I don't know him."

Oskar: "Exactly.
And then the media are not interested in it either.
That was a big deal in hacker circles.
But today there are just a handful of people who know about it.
Tragic story.
After all, there's something about it in Wikipedia."

Amelie: "Okay ...
Then we bloggers can still contribute something important to this fight: "We can issue life insurance policies."

Oskar: "Absolutely.
Absolutely.
You could become a life insurance policy for Marlene if a few million people regularly tell you about her and they find it exciting.
But Marlene would need a huge amount of publicity to be sure by now.
So if perhaps all YouTube bloggers together would make a longer action.
Aaron Schwartz was already quite well known.
He still didn't make it.
But it was more expensive for the people who wanted him... gone:
He just says in WikiPedia that he committed suicide."

Amelie: "Wow.
It's a matter of life and death for you.
Wow."

Oskar: "Absolutely.
For Marlene: Surreal."

Amelie: "All the more funny that with a beauty blog I might be able to help save lives.
That's exciting.
Yes.
I imagine I do."
She turned to the camera.
"Girls!
Boys! Boys!
And everyone else!
We're going to start saving Marlene's life here and now ...
Together.
Okay.
If you disagree, find another blog..."

She took the tablet, started a photo editor and cut out Marianne's face.

Amalie: "Well, I have a picture of Marlene now and I share it here in the blog.
There it is!
And any of you who had any problems with the vulnerability database send it to a copy shop and have it printed 10 times.
10's good.
And you also send the file to two other people who were also involved with the vulnerability database.
We all had something to do with it anyway.
And then you all make yourselves free the day after tomorrow evening, that's Tuesday, that's Tuesday -- it's about saving a life here, you can make a time free there -- and then you find 10 nice places for the pictures.
Someplace a lot of people can see it.
And where it sticks for a while.

We're saving Marlene!
HEJ!
That's the least we can do for the woman who risked her life to show us how many assholes creep around in our smartphones, isn't it?"

Oskar laughed and held out his Raspberry Pi to Amalie: "I already sent it to the copy shop ..."

Amelie looked at the pis screen and said, "What is this?"

Oskar: "A kind of smartphone where I still know the inner workings to some extent.
With Linux.
Do you know that?"

Amelie: "This is one of the things Marlene recommended for her class?"

Oskar: "Yes, one can say.
This is one of the things Marlene's class already has because a few hackers are taking care of their digital well-being."

Amelie: "I want one of those too..."

Oskar: "Hmm. Bad resolution, pretty slow, no Android or iPhone apps..."

Amelie: "But freedom..."

Oskar: "Jo, more freedom!
That's right, I did.
And you can hack with it..."

Amelie: "Linux instead of Android or iOS.
And Linux wasn't affected by the vulnerability database, was it?"

Oskar: "Yes, I do.
But Marlene sent the Linux people the vulnerabilities four weeks before the release, then they could close the most important gaps before it really started."

Amelie: "Okay. Open source! Free Software! We need it! That's what I learned."
Oskar nodded.

Amelie: "Oh yes, and you can find Marlene's letter in the links.
There are more such tips in it.
What are the tips, Oskar?"

Oskar: "The tips come directly from the front.
They're good.
But that's what we're already doing in class."

Amelie: "I think I'll have to come over there sometime!"

"Yeah! Come by!" Anni shouted from behind.

Oskar: "Come by whenever you want!
We can also use life insurance policies there ...
Amelie, can I say something else here?"

Amelie: "Sure, your camera."

Oskar turned to the camera: "If now also a few pupils watch ..."

"Plenty," Amelie shouted in between.

Oskar: "When you go to school tomorrow, talk to the others about whether you don't want to support Marlene's class.
We're building a safer chat system that can also be used for video chatting.
Through the chats, some hackers offer courses on how to communicate more securely, or program, or a bunch of other things.
And if you'd rather do it than teaching, if you think it makes more sense to learn how the Internet works, then celebrate and go on strike with us.
We'll get the Internet back together!"

Amelie: "Wow! That was a lot of politics for this blog..."

Amelie and Oskar looked at each other.

Oskar spread his arms asking, "Hmm?"

Amelie turned to the camera: "Say in the comments what you think about it.
All I can tell you is that hackers are cute people."

She hugged Oskar and cuddled up to him.

Amelie breathed in and out again: "Mh mh.
You have to try it."

Oskar carefully put his arms around her.

Amelie: "And don't forget: Send the picture to the copy shop.
The day after tomorrow we'll go glueing!"

Amelie waved into the camera.
Oskar did the same thing.

Amelie: "See you soon."
Amelie made a kiss.


