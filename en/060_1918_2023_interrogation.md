

## **1918, 2023** Interrogation (1)

### SCENE 8

A dirty street cafe in a shopping street somewhere in Addis Ababa. Marlene was sitting in a corner, halfway behind a column, drinking tea.
Two native young men with black sunglasses looked over at her from a neighboring table.
She looked away, reached into her backpack and pulled out a paperback.
She was about to unfold it when suddenly a platform truck stopped and men in uniform and sunglasses jumped down.
Three of them stood in front of the café with their submachine guns in front of them.
On the other side of the street, something similar seemed to be going on.

Marlene proud.
She watched closely what was happening, put one hand on her backpack.

"No problem!" shouted one of the young men from the next table.
"It's just the Home Secretary, he wants a cup of coffee over there."

She nodded to him, took the book and opened it.
In the middle of the book she had cut herself a hole into which exactly one Raspberry Pi with screen fitted.
She typed on the screen, left some messages and closed it again.

Then she closed her eyes and leaned back.


/// BLENDE /// 1918, Hilde.

____
Hilde, tied up on a chair in a large vaulted cellar.
She's got a gag in her mouth.
Isolated flickering lights on the wall.
Around them, a group of young men in different uniforms.
A captain.

A man in the group takes two steps towards Hilde and hits her in the face with his hand.
She tries to avoid it, but the blow hits her so hard that she tilts the chair to the side and falls to the ground.
She hits her head and stays there.
Your lip is bleeding.
Her face is dirty from the wet ground.

Captain: "Pick it up!"

Two of the men grab her and raise her up.
The captain takes a chair and sits down in front of her upside down on it.
He puts both arms on the back of the chair and grins at them.

Captain: "Bitch, we warned you!
You messed with us.
Now you get the bill for it."

Hilde stammers something through the gag.

Captain: "Quiet!
It's not your turn now.
It's my turn now. We warned you several times.
You just moved on.
All this wouldn't be necessary if you'd backed off, if you'd acted reasonable.
But you leave us no choice."

Hilde stammers again.

Captain: "If we now ran with open arms towards the French, as you Spartacists want, we would all get a bayonet or a bullet in the chest.
And that's how you all want to hug: Englishmen, Americans, Russians, Chinese, Negroes, the whole world.
It's pure stupidity to trust everyone, madness.
This is suicide.
We can only trust in our own strength and nothing else.
The weak has no friends.
The weak becomes a slave."

He looks her in the eye.

"You help the weak.
You've always done that.
Back in school.
You helped those who couldn't make it because they didn't want to work.
And you still do.
Give them food, a place to sleep.
Take away the last reason they'd ever work.
They want to steal only from those who work hard and honest.
And you're helping them.
German nature works hard and is honest.
That's why everyone buys our goods, all over the world.
"Made in Germany", that has sound.
This is the future.
We are better than everyone else, we can work better, we can organize better.
We have the best inventors.
If we mix our culture with others, we weaken them.
Cultures cannot live side by side on an equal footing.
There must be a mission statement, a guiding culture.
And it's German."

He pulls out his gun, looks at it.
Weighs it in your hand.
Then he suddenly holds it against her forehead and stretched the trigger:
"We need a leading German culture.
We must remove the inferior elements from our society.
Wouldn't you agree?
I bet you think so, too.
We've prepared a statement for you to sign.
Are you ready?"
She's stammering something incomprehensible.
"Ahh? You do? I see.
You can't talk.
BERTHOLD!"

A man approaches from behind and opens the gag.
Hilde coughs.
The captain takes his pistol from her forehead and looks at her: "Well, are you ready for it, Hilde?"

Hilde in a rough voice: "Never!"

The captain grins, "I was kind of hoping you'd say that.
Your whitewashing with the scum was always repugnant to me.
It has its charm to kill someone.
Especially when it's an assignment, when it's a duty.
It's quite an experience.
Not many people can kill.
It'll be my pleasure.
And I'll even get a badge for it."
He's aiming the gun at Hilde's forehead again:
"Last chance, whore.
Ah.
Berthold, the tub.
I don't want this to be another mess."
Bertold comes back and pushes a metal tub with his foot behind Hilde's chair.

Hilde looks at the captain seriously: "You are responsible for what you do.
You personally.
You're an adult.
You can't hide behind an order or any group or leader.
What you do, you do it yourself."

The captain laughs: "Yes, I will.
Me alone.
Out of conviction."
He's grinning.
"Together with Bertold."
He presses the pistol harder against Hilde's forehead and shouts slightly excited: "Think your last thought!"
Then he pulls the trigger.
Hilde's face freezes, her neck twitches.
"Click!"

The captain laughs: "Boom! You're still alive.
Did you think I'd just shoot you here?
No, no, no, no, no, no, no.
We still need you.
You know a few things we want to know.
You won't get out of here that fast."

Hilde closes her eyes and breathes hastily.
She's gone completely pale.

Captain: "First of all I want you to give me all the names of the Spartacist Committee, right now.
I'll tell you one thing: Wilhelm Papenburg.
We already have it.
Now I need nine more.
We know three of them for sure.
If they're not on your list, I'll shoot you in the foot once for everyone who's missing."
He turned around.
"Bertold, do you have the bandages?"

Bertold: "Yes! Here.

Captain: "Or, hmm, I'll tell one of the boys here to give you a real hard run, just like a man.
You don't look so bad.
Well?
How's that?
Are there any volunteers?"

From the group: "Yes!", "Yes!", "Here!".

Captain comes close to her face: "Well?
Which would you prefer?
One of them or your foot?"
Hilde spits a big chunk of saliva in his face.
The captain straightens up, trembles and wipes his face.
He takes a deep breath and then hits Hilde in the face with his hand.

Captain: "You bitch! We can do it differently! Slut!"
He's walking around the room excited.
"Slut! Bertold!!"

Bertold: "Yes!"

Captain: "Bring him in.
We'll go right to level two."

Bertold: "Yes, Captain!"

He bends down to Hilde, goes very close to her ear with his mouth and whispers half-voiced: "Well? You know what's coming now, don't you?
Don't you want to spare yourself that? And Wilhelm, too?
I'll give you the price: You write down the names.
If the names are right, you can go home and cook yourself a plate of noodles.
Maybe take a shower first ...
Hmm? How's that? ...

Hilde shows no emotion.

"Klock!", the door jumps open and two of the soldiers drag in a man who pedals his legs powerlessly.
They put him on a chair about ten meters from Hilde, so that she can just see him from the corner of her eye.
He can barely hold himself upright on the chair.

Hilde looks over and thinks, "Oh, no. Wilhelm..."
____

/// BLENDE /// 2023, Marlene.

Marlene opened her eyes and moaned slightly, "Fuck!"

s "Pling" came quietly from the book.
She unfolded it, left it and smiled.

Marlene: "Lago Maggiore? Yeah. Why not..."

She took a book, put money on the table and set off.


