

## **2064** Back in the 20th century

<span style="font-variant:small-caps;">In Fortunato's beach house.</span> Fortunato was running around the beach apartment with his head high red.
Jonny was sitting in an armchair.
Two men in black suits stood to his side, overlooking Fortunato and on two life-size monitors showing the two agents Lasse had spoken to in San Diego.
A camera followed Fortunato automatically.

Fortunato: "This is a CATASTROPE!
I'll give you two super hackers on a silver platter, and you let them go.
Ha! You call the CIA Trantüten and the NSA Schnarchnasen and then two hackers escape you with a simple car interplay ...
The first one's already gone and we haven't had a signal from the second one in 35 minutes."
He showed a display on the wall that showed the names of Lasse and Sigur and behind each of them a map section with a question mark in the middle and a time.

First agent:
"He's at a mall in Phoenix.
It's our turn.
We now have 13 men on the ground in this one department store.
And more than 50 are on their way there as a backup.
The whole complex is surrounded by police barriers.
They can control every car, every pedestrian.
We know what he looks like.
He can't get away.
No problem."

Fortunato: "You guys still don't understand what this is about.
This is war.
We have the greatest network of information sources available around the world, live feeds, information about every person on earth who has ever been on the Internet, 24 hours a day, 7 days a week.
And the most important of them we get under camouflage.
Not only under camouflage from our enemies, no, also under camouflage from CIA, NSA, most of the government.
Maybe 11 or 12 people in Washington know about us.
That's why we have to protect our people's cover!
That's why we pay them with Bitcoins.
In the year 2028 such a thing does not cost dollars or Euros, that costs Bitcoins, many Bitcoins.
The reserves will be used up on Monday.
Get away!
Then we can no longer serve our contacts.
We can't just transfer money, we can't take cash anywhere, we can't buy Bitcoins for cash.
We need too much.
We hold over 12 percent of all Bitcoins there are.
Yes!
And 11.8 percent are now in the hands of a 20-year-old who walks around a department store with the sole purpose of escaping you."

Second agent: "We're breaking off pursuit.
We'll get him.
Take him to a suitable location and then I'll convince him personally to defuse the bombs and return the Bitcoins.
Maybe he dies in the process.
But not before we get the Bitcoins back.
I could be in Phoenix in three hours.
In six hours you'll have the money..."

Fortunato with a slightly red head: "... or the shop has flown around our ears.
I don't want to wake up and realize the Bitcoins are gone.
Then we can shut this thing down!
I just explained that!
Is somebody listening to me?
There must be another way.

Still can't get into their laptops?
You still don't know where the bombs are?
I mean, this is the gate net, we know the gate net, but we're blind here, completely blind.
We don't know what they're talking about, we don't know what they're doing, we don't know anything, we don't find the bombs, nothing, nothing, nothing.
And now both stations are silent, we don't even have a location.
Are we back in the 20th century now?"

First agent: "I just got a message in ...
He's not at the mall anymore.
They've searched everything now.
He must have gotten through the barriers somehow.
We've lost him!"

Fortunato: "WAAASSS?" He turned powder red on his face.
"NO! DEPPEN! INCAPABLE!
They'll neuter you!"

He closed his eyes and then suddenly screamed:
"S C H E I S S E !"
He stepped against one of the large screens, which fell backwards and smashed into thousands of splinters.
He yelled at the agents on the remaining monitor:
"Change of plans! Change of plans. I want him here, I want both of them here now!
We'll do this the old way.
Not that super-secret bureaucratic shit with hundreds of lame asses.
Access!
ACCESS!
Did you hear that? Get them! Get them!
Both of them.
Bring her here.
I want her in this room before sundown today.
I won't go through with this anymore.
We're gonna do this the way we used to do it."


