

## **2064**** We got him!

<span style="font-variant:small-caps;">Fortunato wore a </span> Brazilian carnival hat and had a beautiful, half clothed young woman in her arms.
His gaze was empty, his face grey.
Two agents were sitting on the sofa without jackets.
He looked towards the television set, where a ticker appeared under a news programme:

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
"2.7 million Bitcoins, for Turkmenistan, Afghanistan, Uzbekistan,
Iran, Pakistan..."
</div>
He got away from the woman, waved his arms and shouted as loud as he could: "HUAARRRKRMPLSIGURRR".
Then he pulled a gun out of his pants and fired twice at the monitor.
Splinters flew through the room.
He looked with hate at the big hole in the middle of the screen.

The woman had turned away in shock and covered her ears with two fingers.
She shouted, "Fortunato!
Don't do that!
Please, please.
Come on, let's get out of here.
I want to get out of here!"

Fortunato: "I can't.
No, not yet.
I have something to do..."

An agent: "It's over.
There's nothing left to do."

Fortunato: "Over?
Over?
Just like that?
Because of two teenage boys.
It's all over?
No, it's not over!
The story ends badly.
Good.
Check.
But I don't want her to end badly just for me."

Agent: "What are you up to?"

Fortunato: "I have a plan.
I know how to catch these guys.
I'll grill them.
I will tear out their fingernails one by one and then I get the finger forceps, then it really starts.
Sigur.
He shall see his own blood."

Agent: "What kind of plan?"

Fortunato: "I have a plan.
It'll work.
Not like the Fuck other plans we had.
I left the boys here in my apartment.
What a fucking plan.
They sat in here with their laptops and I was in the room as Sigur ... Sigur ..."
He breathed hard.
„...
Sig..."

One of the two video conference monitors went down from the ceiling and showed the agent from San Diego.

Agent on monitor: "Fortunato?"

Fortunato dry: "Yes."
He stood up in front of the monitor.

Agent: "We got him?"

Fortunato: "Who?
Sigur?"

Agent: "Yes.
He tried to hack into the satellite by some other means.
That's how we caught him."

Fortunato: "Do you have a house number?"

Agent: "Yes."

Fortunato grinned: "Yah!
It's all right, then.
Get him. Get him.
I want him."

Agent: "We will.
Half the crew is on their way.
This time we won't let him get away."
He hesitated a little.
"Fortunato ... there's something else."

Fortunato: "Yes?"

Agent: "Your unit will be disbanded."

Fortunato: "Dissolved?
What do you mean?"

Agent: "Dissolved.
Completely.
They clean up...
I have to go now.
Here we go.
Take care."
He disappeared from the screen.

Fortunato looked around confused: "Dissolved?
Clean up?
What does that mean?"
It looked around his room.
His gaze was drawn out to the sea, to the horizon, to a small black dot beneath the clouds.
He yelled, "ROSANNA! COME ON!"

He grabbed her, pulled her by force to the patio door, opened her and ran off with her towards the stone wall to the sea.
Shortly before he let her go and jumped on the wall.
"COME ON! SPRING!!"

He was diving into the sea.
Rosanna was about to climb the wall when a rocket hissed past her into the house.
Everything disappeared into a gigantic explosion.
It was thrown into the sea by the force of the blast wave.
A second missile hit.
Deeper into the foundation.
A big fireball stood over the place, where the beach apartment had stood before.

Fortunato was plunged four, five meters before he turned around.
He saw Rosanna just below the surface, drifting towards him.
She was bleeding.
Out of the head, on one arm and out of the mouth.
He swam to her, touched her.
Looked at the wounds, shook her.
Checking her artery on her neck.
She was dead.


