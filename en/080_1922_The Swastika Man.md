

## **1922** The Swastika Man

### SCENE 11: In the same vaulted cellar

____
The captain, Bertold, another uniformed man with a swastika bandage on his upper arm.
You're standing in the middle of the room.
Hilde and Wilhelm are no longer here.

Swastika man: "So!
So this is your room for advanced interrogation methods."
He looked around and nodded.
"Nice place.
I like it!
Advanced interrogation methods ... good name.
I like it too!
You have to know what they're called.
The people want to be seduced.
He wants to be seduced."

Captain: "If you choose the order of the advanced interrogation methods well, they talk pretty fast.
The Papenburgs were here, both together in this room, with both of us."

He grinned.

"It happened very quickly.
And in the end, we both told each other everything we wanted to know."

Swastika man: "And then you took her out of the game.
That was a big deed!
We even celebrated this in Munich.
The photo with the two swimming in the canal ... face down ...
The red Gesocks must be exterminated.
There's no other way.
They will not be converted."

Captain: "And first they have to be specially treated.
Information is important."

Hakenkreuzmann: "A technical question: You have to know in advance what they know, otherwise it's difficult, isn't it?"

Captain: "Then it will take longer.
"Someday they'll talk."

Hakenkreuzmann: "But then what are they talking about?
If a poor bird like that doesn't know anything, he'll start inventing something.
We have a serious problem with that.
The whole thing's missing something."

Captain: "What?"

Hakenkreuzmann: "We talk a lot about this in Munich.
Something he can exhale from time to time.
Gets a little strength.
We were thinking, maybe some good guy to feed him in between.
A friend.
Someone to catch his breath with.
Then he gets hope.
If you extend it over a few weeks or months, I think you can get quite a lot out of it."

Captain thoughtfully: "Hmmm. Yeah, that might be true."
Bertold nodded in agreement.

Hakenkreuzmann: "Illusions, one needs illusions everywhere.
Illusions of good, that's hope.
Hope is good.
That makes me weak.
Illusions of evil.
It's scary.
Hope and fear.
You can do a lot with these two things.
It's important how things look, how they're called.
We're setting up a powerful propaganda department in Munich right now.
Hitler does it himself.
Do you know him?"

Captain: "No."

Swastika man: "Great man.
Very good language.
Powerful voice.
And if something doesn't work out that way, he talked for a while, and in the end everyone thinks that it went well anyway.
He gets everyone around, everyone ...
Stays out of all the fights, but with his mouth he flattens everyone.
This is an important thing.
Words!

Captain: "I see.
Sounds good to me.
I wish we had a real leader here."

Hakenkreuzmann: "We will soon be coming to Berlin.
We need people like you."

Captain nodded and looked at Bertold, "We're in."
Bertold nodded back.
____


