

## **2064** Intimidate!

<span style="font-variant:small-caps;">Marlene and Lilly</span> sat together over a cup of tea in the stone hut.
A campfire was burning in front of them.

Lilly: "Did this really happen with the cyanide capsule?"

Marlene: "I haven't experienced this myself.
When Ali Steffens and Eduardo Lampresa cracked the LBI-Bitcoin safe, I was in prison.
I didn't get any of that right away.
But Ali Steffen had something in his body to take care of.
He didn't get a chance to write his story anymore.
But it can be good, it has already been very brutal back then.
But I never had anything in my body myself.
Just a laser tattoo on his forehead that you can read from satellites.

What there certainly were were real torture centers where many people died.
First in East Africa and the Middle East, but later more and more in the central cities such as Berlin, Moscow, San Francisco, Quito and Paris.
They were mainly for intimidation.
It wasn't so much about punishing people or getting information, it was about the message: "Be quiet and nothing will happen to you.
And if you don't shut up, then it's gonna be bad...

And that worked.
Many bowed, many were afraid, some panicked, even hackers.
But not all of them.
Some hackers can't bend.
Actually, if you bend, you'll stop being a hacker.
And the intelligence services have said that if they show enough of their superiority, everyone will bow.
You were wrong."

Lilly: "Like your brother..."

Marlene nodded, "Yeah, like my brother.
Mm-hmm."
She shook her head slightly.
"That was unfair.
... I feel a shiver running down my spine now when I think of him.
He didn't bend.
He's got her figured out.
He could talk to them.
I liked him so much.
And he had to die so early."

Lilly: "Yes, that was the beginning for you, wasn't it?
Sort of.
That's how I understand your book."

Marlene: "Yes, it was a beginning.
That was where it was serious to me for a while.
I used to play a lot before.
I just did what I had to do.
Also the NSA database.
After that, it's harder and darker.
That's why I got caught once.
And I've been limping my lips a little ever since."

Lilly looked surprised at Marlene's leg, "That's why?"

Marlene: "That was in prison.
They're really bad guys, not people, at least if they're soldiers.
They may be people in their private lives.
This is really schizophrenic.
Absolutely ice-cold and brutal at work and soft and nice at home to partners and children.
This was also the case for many people in the companies at that time.

But even that can be understood.
If you know their story.
I got to know a soldier better when I was in prison.
Pretty much.
That was a turning point for me.
His story blew my mind.
That's when I learned how important it is never to be against anyone, but always to keep an eye on your own path and to try to understand each other as well as possible.
Even if he wants to kill you.
Marwin could do it.
He could always look beneath the surface.
He knew what was going on right now, just knew it.
And he kept telling me.
But I didn't understand right away.
It's in the eleven basic ideas Marwin wrote down in prison.
I read them again and again, but I didn't really understand them until I was in prison myself.
Same as him, by the way.
It's perhaps ironic of fate that I ended up there.
Or it was just intimidation again.
It's so much about intimidation, they want to look omnipotent.

Ali and Eduardo were great.
It was completely impossible to intimidate her.
They had just somehow decided not to be intimidated and they went through with it.
And now Lasse and Sigur are playing this one.
Who knows, maybe they will manage the whole mission and then there will be a meeting between Ali and Eduardo and Lasse and Sigur.
This is gonna be exciting.
But a virtual one, real is unfortunately no longer possible."

Lilly: "The mission was so awesome.
I had TRON show them to me.
And Fortunato?
Do you know anything about him?"

Marlene: "Fortunato was really called that.
I even met him once.
But it wasn't much of an encounter.
He's nice.
That's funny.
And always at least two women around him.
But I couldn't talk to him well.
He's just like the game.
A sympathetic asshole."

Lilly: "What's the next step with the Bitcoins?
Are they on the right track?"

Marlene: "Absolutely.
And what happens next?
Hmm.
That depends on what they're doing now.
Whether they find what Eduardo did.
Or something equivalent."

She smiled at Lilly.


