

## **2023** The arrest warrant

<span style="font-variant:small-caps;">In the classroom of Marlene's old class</span>, two years after her escape.

Anni ran with a red face and her smartphone in her hand behind the last row of tables in the classroom.
Sophie followed her, put a hand on her shoulder.
Anni got loose.

Anni: "What a fucker!"
She stamped her foot on the ground.
"I can't believe it!
He did it again!
I thought that was over for good.
I thought they stuffed the holes now.
I have the latest version of iOS."
She stretched out her smartphone to Sophie.

Sophie took it and looked at the display: "Shit!
What a bummer!
He's posting back through your Facebook account.
Shit.
To all your friends.
And such bullshit!
He's got so much shit in his brain.
You really should never have gotten involved with him.
He was an asshole before.
After all there are no photos this time ... Oh shit ...
Nah."

Anni: "What?"

Sophie: "A photo.
Right now."

Anni: "Give me!"

Sophie kept the phone away:
"No.
Better you don't watch this."

Anni: "But not naked again?"
She pulled the smartphone out of Sophie's hand, looked at it and leaned against the wall.
Then she slowly let herself slide down: "Oh, no ...
no ...
No. No.
She yelled at the smartphone, "You fucker, ass, birdbrain."

She put her head in her hands and cried.
Sophie carefully put her hand on her back: "Come on, now we're really going to do it.
We'll report him!
You know, like, police and stuff.
Everyone knows it was him.
He's done it before and boasted about it.
We'll get him."

Anni sobbed, "That could have been anyone.
This is another fucking weak spot from some fucking tutorial on YouTube.
And even if we could get him, the photos are outside.
Nobody's gonna bring them back for me."
Sophie stroked her back.

Anni sobbed, "Why?
Why me again?
Why still?
I thought it was over.
I didn't pick up my smartphone for three months last year.
Until everyone said there were no more back doors in there."

Kevin joined the two from behind: "Yes, yes, that's what they said:
The most private and secure iPhone ever ... super transparent and safe Android Phones, like never before.
No more back doors for the first time.
Not at all ...
Never again ...
Apple and Google celebrate their liberation from the intelligence agencies.
Big show.
All thumbs up.
But then they installed new back doors again.
You can't leave it alone.
Maybe they'll be forced.
Throw away your iPhone!
As long as it's not open source, you can forget it."

Sophie pushed him with her elbow: "Hey, you idiot, can't you see: She's in a bad way.
She doesn't need a lecture now."

Anni wiped tears from her eyes: "Let him.
He's got a point.
That's what I'm doing now.
Get rid of it.
Marlene said that a few years ago.
She used to say there were back doors and all kinds of things in there and that they were watching us with them.
Oh, we could use it so much right now.
She'd know what we could do now."

Kevin: "Yes.
She'd hack the stinker.
He would constantly see his picture on every screen at school, for weeks, and never get into his own Facebook account anywhere.
He wouldn't have a good life anymore."

Sophie laughed: "Yes!
Marlene.
That would be cool right now!
...
That's funny.
She never contacted me again.
We were actually good with her, weren't we?
Is she still traveling around the world with her brother?"

Kevin: "I don't know.
The last Twitter messages are more than one year old.
And they were pretty weird.
Only beach and sun, no laptops ...
If she was there with her brother, I don't know."

Sophie: "The parents said that they had problems with secret services and wanted to get used to hacking."

Kevin: "Do you believe that?
Marlene?
You want to give up hacking?
A cat becomes vegan?
Marlene repentantly admits that hacking is not good, that one does not do this in our world and becomes "reasonable".
They'd have to torture her or something, brainwash her..."

Sophie: "Mhmh.
You're right, I did.
But then what does she do?
Then where is it?
What's the matter with her?
We really need her."

Kevin shrugged his shoulders: "I don't know ... Wait a minute.
He turned to a boy in the second row: "Tobias!"

Tobias: "Yep!"

Kevin: "You were at a young hacker's day at the CCC a few months ago, weren't you?"

Tobias: "Yep!"

Kevin: "Did they say anything about Marlene?"

Tobias: "No, nothing new."

Kevin: "Nothing?"

Tobias printed a little: "One of the CCC mentioned her.
But nothing special."

Kevin: "Tobias! What's that? Information..."

Tobias hesitantly: "There's something about an argument.
Someone said she wasn't ticking right anymore.
I asked someone who had defended her what that was, and he said Marlene was still looking for her brother.
But she wouldn't have only friends among the hackers."

Kevin: "Why didn't you tell me?
Here in class and all that.
Where is she now?"

Tobias: "The hacker told me to keep this to myself.
She wouldn't be in public right now."

Kevin: "Where is she?"

Tobias grumbled.

Kevin went up to him in the second row, "Where is she?"
Sophie followed him.

Tobias: "I don't know.
I asked him too, but he didn't tell me anything.
But I had a feeling he knew that.
I'm not supposed to talk about it!"

Sophie: "Then why are you talking about it?"

The door blew open.
Luke stormed into the classroom and positioned himself in the middle: "HEY!
HAMMER!
HAMMER!
Have you heard?
Marlene's wanted internationally on a warrant.
She's supposed to have done that, hacked the vulnerability database!!
It was her!!!"
He waved his tablet.

The three of them stormed Luke: "Show!
Show me!" They bent over the tablet and read:

____

* German hacker exposed in NSA spy scandal*

The Berlin hacker Marlene Farras (18) is said to have played a decisive role behind last year's heavy revelations about vulnerabilities in computer systems.
She's already been on the run for over two years on other charges.
Yesterday evening, the United States government issued an international warrant for their arrest.
The US accuses her of betraying military secrets that have led to millions of thefts, fraud, copyright and personal rights violations and, in three proven cases, murder.
She's to be charged under the 1917 Espionage Act.
Should she be sentenced, she expects a life sentence or the death penalty in the USA.

Federal Justice Minister Lamprecht comments: "We reject the death penalty and will only extradite it if we get a guarantee from the USA.
Unfortunately, however, the legal situation is otherwise clear in this case.
She didn't work for NSA, so she's not a Wistleblower.
It appears to have hacked into the NSA from outside, supported by a larger group of hackers, and stolen the vulnerability database.
This is computer violence, a definite case of espionage.
As far as we know, she was blackmailing money with it first.
When this did not lead to success, she published the database in a completely irresponsible way.
We all know the consequences.
An infinite number of violations of personal rights, property rights, industrial espionage, some companies had to file for bankruptcy. There's evidence of three deaths."
____

Anni: "Woah!
Whoa!
Woawoawoaa!
What kind of idiots are they?"

Sophie: "They want to blame Marlene for murders.
Espionage?
You're freaking her out?
What's that?"

Anni: "Sure, I also thought about killing the asshole who posted my pictures.
But it wouldn't be Marlene's fault...
Are they all there?
I don't believe this.
What the fuck is this?"

Kevin: "Shit all over my brain.
Marlene? Blackmail money?
You're way off.
They just write it like that, they don't know anything about her.
They're so stupid ... Marlene's not putting herself in danger for money, is she?
Marlene, for money?
They're so blind."

Sophie: "This is propaganda.
There's something bigger going on.
They're shooting themselves at her.
I can well imagine she hacked the database.
Also that she published it.
It's only good we know that.
She probably did it to give them a real squeeze.
The ones who put all the weaknesses in there in the first place: Secret services, companies.
And the message seems to have arrived.
She's got her all busted up.
And she's going to jail for that now?
Or worse.
The intelligence chiefs, the corporate bosses, the American president in prison, yes, that would be fair.
But Marlene?"

Kevin: "Kill the Messenger!"

Anni clenched a fist: "Hey, guys!
We have to do something!
We can't just put up with this.
That needs an answer.
A loud one.
This can't stop like this!
Marlene's from our class.
I know a YouTube blogger ...
She's blogging from the apartment next to me."

Kevin decided: "Yes!
They can't get away with this.
No way."

Sophie: "We have to piss on the wankers' legs!"

Anni and Kevin looked a little surprised at Sophie and then nodded.


