

## **2023** More wistleblowers.

<span style="font-variant:small-caps;">Kevin stormed into the classroom.</span>

Kevin: "Quick! Google. Facebook. Amazon. Apple."

Luke sat in front of his computer and typed.

Lukas: "What is it?"

The Google website appeared with a black background.

Kevin: "They did it. You really did it."

Anni shouted from further back: "Apple is also black."

Another student: "And Facebook."

Lukas: "Shit.
Everything on Google is black.
Not just the home page."

Kevin put a hand on Luke's shoulder: "Geil.
There, click on the button!"


<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
AMAZON - APPLE - FACEBOOK - GOOGLE - MICROSOFT - TWITTER - DROPBOX

JOINT OPINION

As a technology company, we have focused so far on the
centers what we do best: As many users as possible
the best technology experience there is. At the same time
we have always endeavoured to comply with the legal framework conditions.
which apply in the respective countries. These are very
different. The rules in China are different from those in Russia,
in the U.S.A. other than in Europe.


In our home country, we naturally had a closer bond
to government agencies. We had the confidence that this wouldn't be
would be exploited. We have direct access to
User data set up. Hundreds of thousands of enquiries were received
specific accounts. We never analyzed them.


After the recent events, we have done this now. What
did we find out? Only a very small percentage of them
was actually Homeland Security. The bulk was as
requests for criminal cases. We knew that.
already. What we didn't know was that the vast majority of criminals
the new cases were merely suspicious. There's been no indictment
and a trial was still in progress. The mere statement of a
Suspicion was enough reason for a request. This is a
Misuse of the access systems that we made available to you
have. We've shut down these systems for good.
deleted.


As of today, we will no longer obey instructions that
again requires direct access. Requests must be made in writing
by mail. We'll also be back six weeks after each request.
automatically inform the affected user about the access.


We have activated a website:

www.nsl-and-gag-orders.com

There everyone can see which instructions we are currently receiving from governmental
I got this page. NSL means National Security Letter. In it
to find the actual instructions, for example, a weak-
to the new position. GAG orders are instructions for secrecy. you
threaten the disclosure of NSLs with imprisonment. We do
anyway, because we are convinced that we have to do this now.


We hope for your support.
</div>
In the meantime Anni, Sophie and Oskar had come to the computer of Lukas and had read along.
Other students joined them.

Oskar: "This is real.
This isn't open source and transparent insight into their server systems yet, but it's real."

Kevin: "Wow."

Lukas: "I'm flat."

Sophie: "I find it understandable that they do that.
What are they supposed to do, according to the NSA director?"

Annie: "Companies become Wistleblower! That's awesome."

Oskar: "Right, that's all that matters here:
They publish live.
And because they still have Wistleblower in their ranks, they have to publish everything.
If they report something new without anything moving on this website ..."

Kevin: "If our boys and girls report anything new at all ... You've said that CEOs are more likely to go to jail than continue to do what the intelligence agencies want."

Annie giggled: "Right now I would like to go to prison as CEO.
That'd be nice.
Imagine that: Larry Page in handcuffs for publishing a National Security Letter."

Oskar: "And ten minutes later pardoned by the president."

Everybody laughed.


