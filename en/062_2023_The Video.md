

## **2023** The video

<span style="font-variant:small-caps;">All students sat</span> together in a large circle of chairs in the middle of the classroom.
The tables were stacked on top of each other on the wall.
Everyone had a note in their hand.

Kevin: "Can everyone make their own sentence?"

Everybody nodded.

Kevin: "Well, let's go!"

Lukas held a smartphone with a stabilizer and microphone on Kevin.

Luke interpreted with his fingers: "Three, two, one..."

Kevin: "Dear Marlene!"

Lukas went with the camera to Anni.

Anni: "A few hours ago, we learned that you had been working with
on an international warrant for your arrest because you're
"you're supposed to have hacked into the vulnerability database."

Sophie: "That it was you is something we can all well imagine here."
She pointed her thumb into the camera.

Aischa smiled: "Thank you for that!"

Moritz: "Thank you for having the courage and risking your life for it, for us."
He put his hand on his chest.

Viviane: "That's you!
That's how we know you."

Berem: "No one has ever intimidated you, even though some teachers have tried."

Jannick: "We're all very proud of you here, all of us, without exception."

Lena: "And nothing will change that!"

Felix: "Nothing!"

Antonia: "Not the nonsense that is spread in the media about you..."

Janis: ".... not any promises, like those of the companies, not to install any more back doors ..."

Tom: "...and no threats from our teachers or anyone else."

Aylin: "Thank you for not being intimidated!"

Leonie: "We won't be intimidated anymore!"
She clenched a fist.

Lisa: "No matter what we do here, you've been braver than us..."

Jonas: "and that's why we're going to strike here in class until your image is corrected in public so that we recognize you in it."

Antonia: "We miss you very much."

Marwin: "We might as well use your skills right now."

Jannik: "All this time we didn't know that you were on the run.
We were told you were traveling the world with your brother.
The teachers and your family told us so."

Nils: "None of this was right.
Just as it is not true what is written about you in the newspaper today, what even Mr Brunner defended in our room."

Philip: "We threw him out."

Devin: "Even the headmaster, with whom he came back afterwards."

Marie: "We know that you have not changed and that the things you do are because you are convinced that they must be done.

Johanna: "Just like we do now what we think we have to do."

Nils: "We hope so much that you are well out there somewhere in the world."

Emilie: "That they won't find you and that you can go on doing what you want to do."

Tatjana: "We're with you no matter what you're up to."

Kevin: "Marlene! We'll celebrate you here!"

Lukas: "Take!
Okay. Okay.
Got it.
Now everybody wave and yell together again."

Lukas filmed everyone again in a circle.

Lukas: "Great!
Two takes.
Now get it out!"

Kevin: "Wait!
One more time to me."

Luke held the camera on Kevin.

Kevin: "We are happy about all the other school classes who want to celebrate with us.
We celebrate that there are people who can really do something for our freedom on the Internet.
And maybe we can help as school classes."

Everybody was hooting again.

Lukas: "Geil.
Final take.
Now get it out of here."

Kevin: "Does everyone agree?"

Anni wiped a tear out of her eyes: "Yes, damn it.
Send that out!"

Kevin: "Then off with it.
I just opened a new Twitter account: @MarlenesClass."

Lukas: "Great. I sent you the video."

Kevin pressed his smartphone a few times: "Okay.
It's gone.
WikiLeaks has it.
Let's see when they retweet it."


