

## **2064** Taskforce Marlene

<span style="font-variant:small-caps;">Amelie sat behind her studio table</span> and stretched a Marlene mask in front of her face.

Amelie a little muffled behind the mask: "Boys, girls and others, are you ready?"

Joe and Sven raised their thumbs.

Freddy: "Always ... B. Ride!"

Amelie: "Go!"

The lights went on, the camera turned to her.

Amelie: "Hello there! Welcome to Amelie Out of Berlin.
All out of Berlin.
Today ... today I have a real surprise for you.
This is gonna be fun.
All live!
That's why this is half an hour earlier than usual.
The real Out of Berlin starts at 13 o'clock, after breakfast, as always.
But we need to prepare something."

She took off the mask and held it in the camera.

Amelie: "Of course it's about Marlene.
During our great campaign last Tuesday, more than 20,000 people all over Germany hung the Marlene pictures.
20 000!
You guys are horny!
12,000 more than the week before.
And that's why we're going to ignite stage two.
KAWUMM!
You want to know what level two is?
Then... comes ... with..."

She got up, went to the door, put on a jacket and stormed out.
Joe, Freddy and Sven after him.

Joe: "Wait, I'll take you from the front."

He passed her down the stairs.

Amelie in the camera: "Yes, you are curious, right?
Just this much: It's about Marlene!
Oh, yeah, you already know that."

She giggled.

She looked around the front door and a large black Hummmer limousine turned the corner.
She stopped and everyone got in.

Oskar sat at the wheel with a Marlene cap and turns back.

Oskar: "Where's the party?"

Amelie: "Am Kotti!"

Oskar: "Kottbuss Gate."

Amelie in the camera: "We registered a demo today and that's why everything is already locked there."

Oskar from the front: "But if you now think that the demo is the surprise ... No, no."

Amelie imitated Oskar: "No, no."

Amelie: "That way we wouldn't get so many TV channels together."

The car stopped at a police barrier in front of the roundabout around the subway station at Kottbusser Tor.
Oskar turned down the window and spoke to a policeman.

Amelie: "Back there I see N-TV, ZdF, Zeit online is also there.
Well, that looks pretty good.
You want to know how we managed to get them all here?
Definitely...
But I'm not telling you this yet."

She giggled again.

The police waved through the car.

In the closed area of the roundabout there were three more limousines.
Oskar was steering his way.

"All there!" called Amelie into the camera.
"Big question: who ... is... there... in it?
Please make suggestions in the comments below the video."

She opened the door and got out.
She went over to the rather unkempt lawn in the middle of the roundabout.
There were twelve colorful chairs arranged in a semicircle.
She looked for the center and looked around.
Six television teams were kept about 10 meters apart by folders.

At the same moment the doors of the other limousines opened.
At first you could see an umbrella coming out of every door, with Marlene's picture on it, but you couldn't see who was hiding behind it.
Men and women, very different clothes. 11 umbrellas moved toward the chair circle.

"Krooooonck!" called a spectator.

You could see a hand waving from one of the umbrellas.

Everyone found her chair.
Apparently, the order was important.

Amelie in a dark voice: "So!
The hour ... of accounting ... is here!"

She giggled.

Amelie normal: "No billing, no!
Life insurance!
... not quite.
Just love, greetings and thank you!
I'll start:
Liiieeeeee Marlene..."

Amelie pointed to the first umbrella.

Julien, a well-known YouTuber took the umbrella down.
Two spectators screamed.

Julien: "Dear Marlene: My YouTube account was closed for two weeks a year ago.
At first I hated you for it, as did the hacker who hijacked my account ... until I have understood ... which means we all have these holes on the internet.
Where anyone with the keys can get in.
All you did was hand out the keys.
Edward Snowden didn't do that.
That's why his documents haven't changed anything.
I'm eternally grateful to you for doing this.
And I'm here today to tell you this.
And I invite you to join me on my blog.
If we have an appointment, then I will also invite the Chancellor, she likes us bloggers yes.
And when she comes, maybe you can ask her what she means about the holes."

Julien looked to the left to the second umbrella.
He went downstairs.

Another famous YouTuber: Kronck.
Screeching again from the audience.

Kronck bowed.

Kronck: "Hmmm.
Joah.
Marlene.
Cool thing!
Also from me: Girl, keep it up!
And survive.
And I don't want to bore you all with a story like Julien's.
Only they flooded my canal with commercials, the morons."

He looked to the left.

The next known blogger took down her umbrella.

Bibbi: "Yo, there!
We could go on like this and tell one story after another.
But then we just talk about us.
I'd rather be like this:
MARLENE!
From now on I will show a small picture of you in the bottom right corner of my blog.
In every episode.
Until you're free to roam around here in Berlin."

She looked to the left.
The next umbrella went down.

Timo: "I finish every one of my shows from now on:
And then I'm of the opinion that the secret services should open up."

He looked to the left.

Dagi: "Marlene, I rent the giant billboard at Potsdamer Platz, show your picture and write on it what you send me."

She looked to the left.

Luka: "I rent perimeter advertising in Fifa 24 with the same picture and the same slogan."

Simon: "I do a program with what politicians answer the question: "What do you think of Marlene Farras? "Brave not?" answer.
I'm looking forward to it like a child..."

Paddy: "I'm doing a Minecraft game where you have to try to hack into the NSA vulnerability database.
Then we'll play it together on my channel."

LaFluid: "I've been covering you for a while anyway.
I'm gonna open up a new side channel and cover everything the girls and boys around me are doing."

Ibali: "Yes, cool!
And I'll be on the channel..."

The last two umbrellas stayed up.

Amelie: "Joo. You thought it would go on like this.
No. We only had 10 seats for Blogger, first come first serve.
Now comes Timo.
Timo's not a blogger.
You don't know him?
No.
But maybe his work.
They don't know for sure.
He's also been working undercover so far.
But you know them ... probably... I'd say."

Under the 11th umbrella there was a seesawing movement of the hand.

Amelie: "But ... what you know is the TOY CREW!
The bravest sprayers in Berlin.
Live here today ... for the first time."

The umbrella went down.
Timo: "Hi guys.
I'm a little excited.
Of course, I didn't do any of what Amelie implied now.
There were always others.
But I intend to do something like this:
I find myself a crew together and then we will spray one or if possible also several crates.
Crates, subway crates. Wagon.
Jo, complete and in color.
With Marlene and a few spells next to her.
Let me see."

Amelie played excitedly: "Timo!
But that's illegal!
You can't spray a train of the Berliner Verkehrsgesellschaft, can you?"

Timo pointed casually with his thumb to the left to the last umbrella.
The umbrella went down and the mayor of Kreuzberg appeared.

Mayor: "Exactly.
Before you take the walls of houses in Kreuzberg for it again, I have organized three subway cars for you.
And they'll be up and running as normal when you've finished spraying.
I have reached an agreement with the BVG.
They'll leave that on for at least three months."

She turned to the cameras.

Mayoress: "And, Marlene, to you!
Cool!
Cool!
Come here to Kreuzberg as soon as it's safe for you.
We'll find a nice place where you can live.
Kreuzberg is good for brave people."

Amelie: "Yes!
That's right.
Yes!
That's what we need!
Courage!
Come on, guys ...
There are even courageous politicians.
Let's do more.
The U.S.A. wants Marlene.
But we're not giving them out!"

Amelie went to the chairs and hugged everyone, one by one.

Amelie in the camera: "That's it for today!
That was Out Of Berlin.
And we're going to celebrate now.
You do.
And hang up some more pictures..."


