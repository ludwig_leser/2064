

## **2023** Spaghetti Milanese

<span style="font-variant:small-caps;">Marlene and Luigi</span> sat on the terrace in front of their laptops and listened intently to the President's last words.
They looked each other in the eye for a moment.
Then they both pulled up their arms and laughed.

Marlene: "Yeah!
Yes!
That's really awesome!
Luigi!
The mountain has moved!
It's our turn, it's our turn..."

Luigi: "Oh, oh, the secret services won't like that.
But it was inevitable.
They mess with the top companies, America needs them at least as much as the secret services.
And the companies are now really sore through our girls and boys.
Julian Assenge had already predicted this at the beginning of the 2000s: Whistleblowing makes the breakthrough."

Marlene: "And the secret services also get Amelie ...
She's really picking up speed.
Joan of Arc is back!
Yeah, something's possible now.
Now we have to stay tuned.
What the president said will cause confusion.
Total chaos in the intelligence community.
They won't just take it.
They'll react.
You're getting careless.
That's where we'll get our chances.
We must provide Amelie with everything we know."

Luigi: "In any case, there will now be a bunch of new vulnerability reports!
We might get real, regular vulnerability updates from internal people at the central companies.
How awesome..."

Marlene: "Yes, that's awesome!
The big companies, the governments, they're so dependent on us little people.
They need us as consumers, as workers, as accomplices.
Nothing works without us.
We can dictate what they have to do.
That's what Marwin used to say.
You can't do anything against free programmers, free admins and free hackers.
This is becoming so clear now.
So awesome!"

She put her face in her hands.
"Geil ... awesome ... Marwinian If only he knew. - - Shit.
Maybe he won't even notice."

She closed her eyes.
After a while she got up and walked towards the railing.

Luigi: "Marlene!!"

Marlene: "Good, good.
I'm not going up front."
She sat cross-legged on the floor.
"Where is he now?
Now I want <em>finitely</em> to know where he is."

Luigi: "You went through all your contacts for the last time three days ago.
He's in America, somewhere in America.
Most likely in a CIA prison.
There's nothing new about it.
But try it again."

Marlene: "Someday I'll find a trail."
She got up, sat down on her chair, took her laptop and started typing.

Luigi: "I make spaghetti Milanese.
With Parmigiano-Reggiano and a hint of pepper.
And a Valpolicella rossa."

Marlene didn't react.
She typed and looked spellbound at her screen.
Luigi disappeared into the apartment.

Marlene said to herself after a while, "No wine for me."


