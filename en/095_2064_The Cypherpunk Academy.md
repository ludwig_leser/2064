

## **2064** The Cypherpunk Academy

<span style="font-variant:small-caps;">After a one-hour flight</span> the plane had landed at a regional airport.
Sigur and Tim got out.
Next to the tarmac, the huge white balloon hovered about 20 metres above the ground.
He covered the whole scene in shadows.
Its outer skin seemed to be made of harder material than normal balloons, maybe hard plastic or even metal.
He looked heavy and didn't seem to be able to fly.
He didn't have an actual basket on the bottom either, just a kind of weight.
Sigur was amazed again at his sheer size.
He discovered a shaft beside the weight, which seemed to lead into the balloon and from which two blue ropes hung down to the ground.
Below, Lasse waited with a broad grin.

Sigur ran off and fell into Lasse's arms.

Lasse: "Hej!
So good to see you!
You cyberterrorist!"

Sigur: "Sure!" They laughed and slapped each other.
Tim came along and Lasse hugged him.

Sigur: "That was an action!
Unbelievable!
I've never sweated so much and so many times on any mission.
I didn't expect us to meet again in the game.
And the ride over here, with Tim, the full madness..."

Lasse exhaled.
"And then we'll meet here." He pointed to the balloon.
"You see the balloon, don't you?"

Sigur: "Yeah, sure.
It's big enough.
How am I supposed to miss him?
What's that, anyway?
Tim didn't see him before..."

Lasse turned to Tim and pointed to the balloon: "Tim, do you see the balloon?"

Tim: "Which balloon again?
There's an airport, a tarmac, a few meadows behind it.
Are you starting now?
What's the point?"

Sigur surprised.
"But don't you see we're in the shadows?
Where did he come from?"

Tim: "Hej, stop it now!
There's no shadow here.
I'm blinded by the sun."
And he actually closed his eyes a little.

Sigur looked astonished at Lasse: "What is this?
Is he blind?"

Lasse shook his head: "No, not at all.
The balloon is the Cypherpunk Academy.
You can't see them unless you're cypher punk."

Sigur: "_This is the Cypherpunk Academy?
Sweet!
But I'm not..."

Lasse: "Yes!
You are.
You can call it anything you want."
He proudly hit him on the shoulder: "Congratulations.
Come on, grab a rope.
We must get to the Sphinx."

Sigur shook his head and took one of the ropes: "And if I don't want to be a Cypherpunk..."

Lasse: "Yes, you do.
Trust me on this.
Put your foot down in the loop like this.
And then touch here."

Sigur: "First say goodbye to Tim."

Lasse: "You don't have to!
That's what TRON does.
Everything we do with the ropes Tim doesn't see anymore.
TRON makes us say goodbye normally or something else.
Probably already happened.
You can watch it later."

Sigur looked at him in astonishment.
Lasse winked at him, stuck his foot in the rope and rushed up.
After a few seconds he disappeared into the shaft of the balloon.
Sigur looked at Tim again, but he had already turned away and went back to the plane.
He was talking to somebody invisible.

Sigur carefully put his foot into the loop, looked up and held on.
Then the rope made a jerk by itself and he rushed up.
His hands were stuck, his legs pushed through by some strange force.
He was getting faster and faster.
It was pitch dark in the shaft.
He hurried further up.
After some time he saw individual dots of colour appear on the shaft wall around him, then they began to move, becoming more and bigger and finally everything turned into a sea of colourful leaves, small fish and other things that floated and flowed in confusion.
He shot out of the tube.
The rope disappeared from his hand and he landed on a platform in the middle of a huge dome.
The platform had a diameter of about five meters and fell down vertically on all sides.
He didn't see the floor.
To the right of him stood Lasse on a similar plateau.
Sigur lifted his thumb towards him, but Lasse looked back only briefly and did not answer.
Lasse looked concentrated at a spot far in front of him on the dome wall, as if something would come out there at any moment.
He went to his knees slightly and spread his arms out.

"Welcome to the Academy, Sigur," said a voice next to Sigur and he turned to her.
Mostafa, the mosquito fly, fluttered in front of his face and looked at him friendly.
"I am Mostafa!
We met at the introductory event."

Sigur nodded.

Mostafa: "Lasse is already waiting for the sphinx.
It's always dangerous.
With the Sphinx, you never know what's gonna happen.
You really have to be careful about that."

They looked at Lasse for a while.

Mostafa: "You're the one who did the first LBI mission with Lasse, aren't you?"

Sigur: "Exactly! And you can be asked anything about the Cypherpunk Academy, right?"

Mostafa: "Not only that. I am also the teacher for reading and writing.
Of course, reading and writing code, programs.
Cypherpunks write programs.
It's like the once-in-a-lifetime here.
That's why we have the Cypherpunk computer playground."

Sigur: "I can program."

Mostafa: "I know.
That's why we're not going to the playground.
But there are some things you don't know.
That'll come later for you."

Sigur looked over to Lasse again, who made strange evasive movements: "Do I have to go to the Sphinx too?"

Mostafa: "You don't have to.
You never have to do anything here in the academy."
He grinned.
"You'll have to if you want to get to the interesting places.
The Sphinx guards all exits and entrances to the Academy.
You must pass her again and again, otherwise you won't even get into the academy."

Sigur: "How?
This isn't in there yet?"

He pointed to the huge dome hall, which in his eyes had to be about as big as the whole balloon.
He looked to the right and was startled.
Lasse had suddenly disappeared and where his platform was before, there was now a huge boulder with an oversized beehive in the middle.
Wim the bear sat on top of it and looked over at them with calm eyes.

Sigur was amazed: "What...?
Where's...?"

"Come with me," Mostafa said and flew off.
"Wim is waiting for you."

"How am I supposed to get over there?
I can't fly and it's too far to jump," Sigur called after him and looked down the edge of the platform.

Mostafa shouted from the other side: "In the Cypherpunk Academy everything goes with thinking.
Think your way there!"

Sigur nodded to Mostafa: "I understand."
He took a step back, put his hands to the side of his forehead and said quietly, "To the bear, to the bear."
Nothing moved.

Mostafa: "Words are not thinking.
You have to let go.
You gotta think straight.
Trust!

Sigur tried it a few more times and again nothing happened.

Mostafa flew over to him again: "Well?
Misfire?
You don't have to think you want to go, you have to think you're there."

Sigur's ears ploped, a sting went through his eyes, he pinched them, and when he opened his eyes again, he stood right in front of the bear's rock.
He got dizzy.
He stumbled to the right and could barely catch himself.
Mostafa flew under his arm and supported him.

Mostafa: "No problem, no problem.
It's always at the first teleport.
You're about to get sick.
Your brain's pretty slow registering that you're somewhere else now without going there.
It doesn't know that yet."

Sigur grabbed his stomach.
He kneeled down.

Wim jumped down from the beehive and sat down cross-legged in front of Sigur on the floor.
After a while he straightened up and sat down.
A pleasant atmosphere emanated from Wim.
He looked at Sigur with alert, interested eyes and an invisible smile.
Sigur's body relaxed.
He felt lighter.
The efforts of the last days fell away from him.

"Welcome to the Academy, Sigur," the bear hummed in a dark voice.
"This stone marks the entrance to the Plain Islands.
You'll see why they're called that.
There are eleven of them."

Sigur: "Eleven? As many as basic ideas?"

Wim: "Exactly.
And above the eleventh level floats my garden.
The Garden of Epicurus.
That's the target.
It's gonna be a while before you get there.
The task is to find the way there, or actually, to build it.

On the way across the 11 levels you will find many fortified cities, each with a temple in the center.
You play a knight in the early Middle Ages.
The temples should actually be churches, but today's churches are no longer what they were at the time.
The temples are the guardians of the ways, they watch over who can go which way to the next city, or to a forest or a river.
They all have a riddle, each one his, that is a thought error that can be solved in different ways.
A thought defect that's preventing you from becoming cypher punk.
You'll have to solve it.
And not just in one way, but in four.
You can also add your own solutions later, with a pull request, and they can become part of a temple puzzle.
This way you can become a candidate, a helper and, in the end, a servant of a temple.
This will help you in many situations.

If you do not yet have city rights, you must first conquer a city, with weapons of that time and not alone.
You can't do that.
There are always attacks from passing armies that you can join.
And you need abilities to be taken in by an army.
There's forests for that.
In the woods there are programming and hacking tasks.
I know that's not entirely conclusive for this time.
But cypher punks program and hack.
These are her main activities.
Mostafa, the tiger mosquito, is the lord of the woods.
They can ask you what's right for you and where you can find it.
When you arrive at the 11th level, you will be able to program very well.
and hack it into fear and anxiety for the Command Center Taurus."

Sigur: "I can program and hack."

Wim smiled: "Yes and no.
You'll see."

Sigur looked at Wim skeptically.

Wim: "Now the other three teachers of the academy will meet.
The first one is waiting at the entrance to the islands.
That's here!"

Wim showed himself beside him.

Sigur nodded. He looked at the place where Wim pointed.

Sigur: "Where?"

Wim: "Are you ready?"

Sigur: "To go inside?
Yes!

Wim: "Are you ready for what's coming."

Sigur: "What's coming?"

Wim: "What's coming, what's coming.
Look around you!
Is there anything dangerous around you?
Something's threatening your life."

Sigur looked around, shook his head.

Wim: "If anything feels insecure, focus on it."

Sigur closed his eyes.
She opened it again: "Nothing."

Wim: "Ask yourself: You got everything you need?
Is there something you're waiting for that's necessary before you can move on?
Anything you need to know? Or clarify?
Or can you just be here the way you are?"

Sigur closed his eyes again and breathed calmly.
He nodded.

Wim: "Go with your consciousness to a place behind your eyes."

Wim: "Stay there for a moment."

Wim: "No, stay there.
Don't go away."

Wim: "Now go a little deeper."

Wim: "There's nothing dangerous around you."

Wim: "Slowly go down further and see where you feel resistance.
throaty thoracic ventral Legs.

Wim would stay silent for a while and look at Sigur.

Wim: "Then back to the chest and through the arms.
Back to the head at the starting point."
He waited a few moments.
"Now go where you felt the most resistance."

Wim: "Stay there for a minute."

One minute later, Wim said, "Okay."

Sigur opened his eyes and looked at Wim.
"What's the point?"

Wim: "Don't leave.
Stay in that feeling.
That's preparation for what's coming now."

Sigur: "What's next?"

But Wim had already disappeared, as had the beehive, and Sigur's platform.
The whole dome changed at breathtaking speed.
Sigur got up instinctively and took a step back.
He looked to all sides.
He now stood on a rocky plain that reached all the way to the edge of the dome.
Everything got darker, got sharper edges and outlines and suddenly you could hear a dark rumbling in the whole dome.
The whole room trembled, then suddenly a huge, circular area of the dome stood in flames.
Sigur stared there.
Some of the flames hit far into the dome.
All of a sudden a dark figure with a bird's head and enormous wings shot out of the middle of the circle at high speed and flew up to the dome roof, then from there directly towards Sigur.
She let about a dozen egg-shaped, burning formations fly in the direction of Sigur.
He threw himself on the ground, turned to the side and could barely avoid being hit.

Sigur: "Shit, the Sphinx!"

The Sphinx landed on the ground about twenty meters in front of him and became bigger and bigger and bigger until it occupied about a third of the height of the dome.

Sigur said quietly, "The Sphinx!"

She bent down to him and asked with a loud, rumbling voice:

"WHO ARE YOU TO DARE COME BEFORE ME?"

Sigur stood up and answered without hesitation: "A man!"
He knew he had to respond quickly.
A third of a second to beat the Sphinx.

The Sphinx let her wings flutter up and screamed in pain.
Then she gathered her strength and put on her wings.
She had shrunk a little and looked at Sigur darker and more severely.

Sphinx: "What do you want to do behind this porch?"

Sigur: "Learning.
I want to learn."

For the second time, the Sphinx cried out in a marker-shattering manner.
Sigur looked at her with great concentration.
Whenever he answered "right," she would lose strength and get angrier.
"Right" meant, from her point of view, right.
That was important, that's what Lasse had told him.
The sphinx thought it possessed all the wisdom of the world, the complete truth.
To defeat the Sphinx, you had to understand how she thought.

The Sphinx calmed down again, bent over to Sigur and threateningly turned her head back and forth:

Sphinx: "WHAT DOES YOU NEED HERE MOST?"

Sigur: "Love of justice"

"That was simple," Sigur thought, "The enemies of the Internet in 2028 were out to divide the world into people who lived in prosperity and those who were supposed to work for them."

The cry of the sphinx became more and more a howl.
Her eyes became darker, she began to lose strength, her wings became stiffer, she continued to shrink.

Sphinx: "What's stronger than all the secret services and poor and business in this world?"

Sigur took a moment to cut it short.
It had once known.
It had slipped his mind.
But it had been so easy.
What the hell was that?
He was concentrating.
The sphinx began to cluster.

Sigur: "Youth!" He breathed out.
That was a close one.
You had to answer within three seconds, otherwise it was too late.

The Sphinx roared with its wings.

Sphinx: "What do you do when you fail?"

Sigur: "I cannot fail.
When I make mistakes, I learn, when I do something right, I rejoice."

The Sphinx sighed, loud and miserable.
Her voice became fragile.
She trembled slightly.

Sphinx: "YOU WILL LOST YOUR HERE!"

Sigur nodded clearly.

"And that's exactly what I want," he said with a big grin.

A dark, warm voice sounded from the background:

"Say it differently again."

Sigur more seriously, "I want to lose myself to find myself."

A miserable cawing came from the beak of the sphinx.
She cowered on the floor and turned in pain.
Then she straightened up with her last strength.

Sphinx: "You can't win against me."

That was a trick question.
The word "against" was the decisive one.
Sigur said triumphantly, "But I can win. Again and again!"

He jumped off his platform, ran towards the Sphinx, which had solidified to stone, and gave her a kick.
Everything crumbled to dust and was gradually carried out by an emerging wind.

The whole hall changed again.
Sigur saw a medieval town emerging around him.
He stood on the main square in the middle of the city, in front of him a large temple made of finely worked red sandstone and marble.
Everywhere there were people going about their business.
By their clothes you could tell what they were doing.
At the edge of the square one could see a row of houses and shops.
To the right, a little further away a market place and further back a city gate.

From behind, a clear, soft voice said, "Sigur!" He turned around and before him sat on a wooden bench, Tin, the owl.

Sigur bowed.

Tin: "I also welcome you warmly."

Sigur: "Is this one of those temples that Wim talked about?
Why am I here now?
I wanted to go to the entrance, cities must be conquered."

Tin: "You're in the practice room.
This is a city where you are a temple servant, the highest you can become in a city.
Here you can come and see what you can achieve.
But you also have to figure out how to find this place.
He's all there.
But not everything obvious.
There's guns, clothes, other equipment, maps up ahead.
Everyone who walks by here knows something or can do something.
At the market you can buy remedies and gems, special metals.
You'll understand when you need them.
But the most important thing is this."

Tin nodded towards the temple.

Sigur nodded too.
Sigur: "Sure.
What can I do here?"

Tin: "As a temple servant you don't have to go into the temple to do everything with it.
You can open a window to the temple from anywhere, even in other cities.
"Think of the temple and strike with your hand through the air."

Tin painted a semicircle in the air with his hand.
Sigur imitated the movement.

A picture frame about three meters wide and 1.5 meters high appeared in front of them at a height of about one meter.
He showed the sentence:

<div style="background-color: #222; color: lightgreen; padding: 20px; margin: 20px 0; font-family: 'Courier New'">
You have to adapt to achieve something in life.
to be successful. And everything you can't do because
you adapt, you have to balance that with something else.
For example, by having a lot of money, the most beautiful house,
the most interesting partner to make the most exciting trips
or anything else exciting or beautiful.
</div>

At the same time the same sentence lit up at the front of the temple.

Sigur smiled: "Or the best computer ...
But there's something right about that.
You have to adapt."

Tin looked at him questioningly.

Sigur: "A little.
You can't do everything your way."

Tin: "That couldn't happen in the game.
In the game, that would be how you formulate the thought error yourself.
You would have written that yourself.
And this one's wrong, too.
All thought errors have parts that are believed to be true.
And even parts you think are wrong.
But that's not the point.
It depends on whether people in life live as if this sentence were correct.
And in the year 2028 many people lived as if the sentence was correct.
They wanted a lot of money, the most beautiful house and so on.
And they have given up a lot to be successful.
You have to remember: This is 2028 and a lot of things are different today.
But you still mean that you have to adapt to achieve what you want to achieve."

Sigur: "Come on! A little ...
You've got to be right here.
I'm a programmer."

Tin: "No, not at all."

Sigur looked at her in disbelief.

Tin: "Sigur, it is not exciting to think: a little.
That washes the whole thing away.
Of course, there can be thought errors in thought errors.
There are bugs in every software.
But you will get further in thinking if you assume that this is completely right.
Most people avoid thinking by saying: Something is wrong.
Instead of seeking the perspective from which it is completely true."

Sigur nodded slowly.

Sigur: "Woouuhaa.
Then it gets complicated."

Tin: "Exactly.
Complicated and interesting.
Everything gets wrong if you just look at it from your own perspective.

But on the other hand, everything you understand in foreign words dies.
That's why you'll find your own words for things here at the Academy.
It's not enough for you to repeat what someone else said.
You will learn to see everything from different angles and come to thoughts that are alive instead of just having words.
There's no truth, there's only truth.
The most contradictory things can be true."

Sigur: "That's wisdom, isn't it?"

Tin said, "Wisdom is the past.
Love is the future."

Tin looked at Sigur.

Tin: "I see you want to move on fast now ...
But you're not ready yet.
...you'll also have to pass the plane sphinxes."

Sigur: "I know, Tin.
The plane sphinxes.
I'm ready."

Tin: "You're ready?"

Sigur: "I'm ready!"
He stood up straight in front of her.

Tin looked at him with anxious eyes.

Tin: "On what level do you want?"

Sigur: "What level is Lasse on?"

Tin: "Level 4."

Sigur: "Then to level 4!"

Tin nodded, "So be it.
Level 1: Free software, encryption, distributed services and everything else that any power in the world can withstand."

Sigur nodded back: "I know the 11 basic ideas of the Cypherpunks."

Tin: "I know, I know.
Level 1 is given to you.
You can always go there and visit the cities."
She threw a brightly coloured glass ball into the air, which floated away and after a while disintegrated into thousands of small dusts of all colours.
Sigur marveled at her laughing.

She looked at Sigur: "Level 2: Life ideals: justice, openness, equality, brotherhood, and everything else that elevates us as human beings."

Sigur nodded, "Freedom, letting others share..."

Tin: "Level 2 ... is also given to you.
It's very rare to get two levels for free. You can of course visit and explore them at any time."
She threw a different colored bullet into the air, which also destroyed.

Sigur smiled.
His eyes were beaming.

Tin: "Level 3: Overcoming opposition and everything else that overcomes the walls between people."

Sigur nodded.

Tin: "Level 3: The level Sphinx is waiting for you.
Are you ready?" She looked at him insistently.

Sigur: "Yes."

Tin fluttered her wings and in a flash the temple with the main square and the whole city disappeared and Sigur found himself in a dungeon without a window.
Everywhere roughly hewn, damp stones, held together by mortar, much moss and a straw-covered stone floor.
It smelled hot of feces.
Sigur covered his nose.
He looked around in amazement.
He didn't know that.
Lasse hadn't told him that.
He thought, walked around, looked at the walls.
The only light came from a tiny lattice door with massive iron bars at one end of the room.
It was just so big he could crawl out through it if it was open.
He checked the massive hinges, but found no weakness, even the lock was missing.
The grid was welded.
He grasped the sticks, shook them.
Impossible to move them even a millimeter.
He shouted loudly down the adjacent corridor.
No answer.
He tried to think his way back to the Cypherpunk café, back to the bear, to the practice city, but nothing happened.
Then he grabbed his head with both hands: "Oh no!
Not miraculous, not miraculous."

"Yes!" said a voice behind him.
Sigur turned around.
A little rat looked at him with big, shy eyes.

He held both hands in front of his face and thought, "Oh, no!
I'm barely ten minutes here and already at Wunderlich.
Lasse has only been with him twice in all his time!"

Wunderlich: "Yes, yes.
You fucked up, you cypher punk!"


