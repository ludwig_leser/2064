

## **2023** The Manifesto (2)

<span style="font-variant:small-caps;">Marwin took the pen</span> between his teeth, stood up and started walking around the room again.
Every now and then he stopped, bounced his feet.
After a while he looked at the mirror wall again.
He smiled warmly in her direction.

"What may be going on in them now?" he thought.
"Why do they do such a boring job of observing him even though they have no interest at all in him as a human being?
They look hours and hours at something they don't care about.
This is a waste of time.
Five days a week, that kind of work?
No, I couldn't do that.

And they certainly think of themselves on the better, more pleasant side of history, on the freer side.
But that's an illusion.
Victor Frankl experienced the greatest moments of his own freedom in a German concentration camp - as a prisoner, not as a guard.
Yes, I can understand that now," Marwin thought and stretched out his arms in the air.
"Yes ... to understand the other.
Think yourself into him.
That's it."

He took the pencil out of his mouth, sat down again, turned the pages and wrote.

"6 -- Empathy is the art of thinking and feeling oneself so far into another that one understands why he does what he does.
So much so that you say to yourself that you would do the same if you were in his place and had his story.
No child is born with an urge to kill another human being.
We'll buy him.
Children do not know a superior race, people, nation or company where some people are worth more than others.
Making such differences between people is alien to us as a child.
We'll learn.
That's why we can unlearn it again."

"Okay," he thought, "what do we got?
1st encryption: The technical basis of our freedom.
2. ideas instead of power: a rethinking of the basic ideas.
3. to overcome the antagonism
4. keep the initiative sacred
5. align our words with our thoughts
6. to think oneself into the others

Hmm.
If I have it all, what else do I need?
What is it that makes us possible?
Ahh!
Here comes WikiLeaks."
He turned the page.

"7 -- We want transparency, openness, visibility of everything in common, of everything that concerns us, of everything that is not private.
When I use or consume something, I have a basic right to know what happened to it before:
Who produced my electricity, which I take out of the socket, how was it produced?
Who helped build my computer, who wrote my software and what did he or she do?
What do companies do to develop a product, to build it, to sell it?
How does the state ensure my safety?
What's he doing for it?
Does he kill other people for it?
How many? Who's that?
Does he torture for it?
Is he suppressing?
It's my natural right to know that by using something.
And others have a natural right to know everything about what I do, not just for myself.
Open, free software took up a fundamental right before it was formulated.
That's a general rule."

He smiled and breathed through it.
"Uhh... That changes a lot.
And that would probably go wrong without the first six steps:
A truly open, transparent society.
Uh-huh.
Then we need..."

He turned the page and went on writing.

"8 - We need courage, much courage, to overcome fear.
We get scared when we lose contact with the world and other people ...
and to ourselves, to what we can do.
Then we begin to imagine all sorts of things, what is or what will happen.
And the more scared people are, the easier it is to manipulate and control them.
Intimidation makes slaves.
The answer to fear is courage: seize life, approach strangers and look at yourself - in peace and quiet.
Courage is the strength to overcome any fear.
And courage is contagious.

And then anger.
She is the warm, powerful force in our body.
Anger is not aggression.
When anger meets fear, it becomes aggression, otherwise it doesn't.
We need a lot of anger to overcome crippling hope.
Hope made from the outside paralyzes you.
We don't need this.
We always have enough hope of our own in us.
We just lose sight of them sometimes.
Hope dies last."

He turned the page.

"9-- people can't be numbers anymore.
A person cannot be recorded in a statistic.
It kills him.
The sentence "Eleven people died in a plane crash" kills people a second time.
In reality it was Anke, Helmut, Tim, Trevor, Silke, Antje, Lisa, Jochen, Leon, Aischa and Finn.
They all died in different ways, all at a different point in their lives.
They all have other connections to other people.
They were just together in the same place when they died.
People can't be numbers anymore, people are people."

He put the pencil on the table and looked at the text.

"Okay, "People are people.
That's a nice last word.
I think that's all," he thought.
"There's nothing more to it."

He took the pen and wrote:

"Encrypt,

real ideals,

to overcome the opposition,

free development,

Thoughts instead of words,

I can empathize with you,

Openness,

Courage and anger,

man as man."

He put the pencil back on the table and stretched out.
He breathed deeply.

Marwin: "And who's reading this now?"

He closed his eyes.
After a while, he opened it again and said, "Nobody."

"Hmmm.
We always have enough hope of our own in us," he says out loud.
"It's written here!
Okay. Okay.
Then..."

He raised his hands, looked at the ceiling with big eyes: "I need magic!"
Then he intensively looked at the booklet, rolled his eyes, spread his fingers over it in strange shapes and said mysteriously as he moved his hands up and down:
"Abura kadabarura, find your way to ... Marlene ... Oskar ... Julian ... Tobi".

He looked astonished at his fingers and said to them, "Hmm?
What do you want?
What do you want now?"
He nodded.
"Yes, I can imagine.
I'd die for a keyboard.
And if it were just a trouser keyboard on a Raspberry Pi or Olimex board.
I miss it so much!
Typing.
Keys.
Shit! Shit! Shit!
That would be even better than a mate on a Berlin roof at sunset."
He took the pen and wrote:

"10 - love of technology"

He looked at the list and nodded.
"Now it's beautiful.
Exactly, we must develop a broad love of technology.
Technology is the frozen intelligence of all people, frozen knowledge, frozen spirit, always at my service.
I want to use it as much as I can..."

He formed his fingers again in the most absurd way and moved them over the full-written sheet: "Abrakadubradabra," he said out loud, "fly to Marlene, fly to Oskar, fly to Julian and Tobi."


