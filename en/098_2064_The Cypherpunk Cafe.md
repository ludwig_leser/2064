

## **2064** The Cypherpunk Café

<span style="font-variant:small-caps;">The Cypherpunk-Café was lying </span>right on top of the balloon, in the open air.
It looked in little like an Italian café terrace, with marble floors and columns, in soft colours, small tables and a stone wall all around.
Behind it you could see mountains, a big lake, a whole miniature landscape.

Sigur sat with Lasse.
In front of him stood a large glass with an ice shake.
Next to it lay a thick, leather-covered book, which he ignored.
Wim, the bear sat on the stone wall of the café with his legs dangling outside a few metres away from them.
Wonderfully lively chatted with Mostafa at the neighbouring table, both in front of them a large, elaborate cocktail cup with several straws and many colorful glitter ribbons.
Wunderlich occasionally showed his thumb to Sigur, said something and grinned at Mostafa.

Sigur looked around: "Whoa, who's all here!
This is madness ... So many Cypherpunks in one place ... Hey, Totaot2!" He waved to a table on the other side.
Totaot2 waved back.

Lasse: "Yeah, this is nice.
And you only see up to level 4 and the teachers, of course..."

Sigur: "I'm not even past the Sphinx of level 3."

Lasse: "It doesn't matter, you'll get it tomorrow.
You're always here on the level you want to be.
If you vote too high, you'll be at Wunderlich more often.
On the planes themselves you can only do something when you have passed him and the sphinxes.
You can always come to the café, it's always open."

Sigur trocken: "Wunderlich was extremely annoying.
The Sphinx doesn't mind."

Lasse nodded: "There are also some annoying sphinxes.
You can get stuck there too.
But Wunderlich is special.
And you can meet him in so many places.
I had to negotiate a money loan with him once in a bank.
He was the bank manager.
I'm telling you, he drove me crazy.
I hit him! I hit him!
I'm serious.
I gave him one ...
Boom!
I woke up days later in the real world in the middle of the night ..."

Wunderlich prostete over and grinned.
Then he burped out loud.

Let Sigur know quietly: "I think he can read minds.
I'm pretty sure."

Sigur: "Really! Shit..." He looked at Wunderlich.

Lasse: "Hej!
You were too close to the end of the game.
That's why you didn't come to the Sphinx anymore.
The third Sphinx is good, very exciting.
Today she almost caught me."

Sigur: "You always have to go back to her?"

Lasse nodded: "Often.
Sometimes totally surprising.
It's refreshment, practice, staying agile.

Come on, let's sit over to Wim's for a while.
He can give you a few more tips about Wunderlich.
If I know you, you'll meet him again tomorrow."
He grinned at him.

Sigur moaned.
He looked at the table and pointed to the book, "What's that, anyway?"

Lasse: "Ah, I almost forgot.
Your log.
There's a new medal in it for what we did today and the three Wunderlich balls.
Not a bad start.
It's got our new mission in it, too."

Sigur: "Our new mission?
What's that?"  He tried to open the book, but didn't make it.
It was like being taped up.

Lasse: "Just think what you want.
You're at the Cypherpunk Academy."

Sigur looked at the book and it opened on one side with a shiny medal.

Sigur: "Wow!"

Lasse: "A Steffens-Lampresa medal.
These are the two who in reality destroyed the NSA main building with stolen combat robots.
That was 36 years ago.
I got them, too."

Sigur: "She looks really good!
Is that them two down there?"
He pointed to two pictures.

Lasse nodded.

Sigur: "The one looks Brazilian.
Boa, and the other one's got a bodybuilder figure.
Hej."

Lasse: "Both are Brazilians."

Sigur: "Okay. Are they still alive?"

Lasse: "No ... they didn't make it."

Sigur: "Shit.
The world was like that back then. Awesome."

Lasse: "They died just before the end of the war, Secret Service."
"I'd rather live in a room with Wunderlich than work in a secret service that murders people and stuff."

Sigur exhaled. "Miraculous..."
He looked at the book again and it opened and closed.
Wunderlich grinned at him on the open side.
Sigur shook his head in shock.

Sigur: "Let's go to Wim."

The book closed. They stood up, climbed the stone wall and sat down to the left and right of Wim.
He put his arms around both of them.


