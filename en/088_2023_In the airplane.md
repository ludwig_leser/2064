

## **2023** On the plane

<span style="font-variant:small-caps;">Marlene sat in a wide armchair</span>, opposite Linda.
Both tried to have a halfway friendly conversation.
Linda asked Marlene about her childhood.
On the other hand, Amelie spoke into a microphone.
In front of her, Joe with a camera.
Through the window you could see a carpet of clouds and bright sunshine.
Further behind them, the president gave an interview to a television team at a conversation table.

Robert came by and quietly said to Linda, "She wants to talk to you.
In her cabin."

Linda nodded, got up and disappeared behind a door at the end of the cabin.
The president followed her.

Inside, they both looked at each other for a long time.

Linda: "I know, I know.
It's a risk.
But it'll work.
Actually, everything fits in very well with our plan right now.
Just like the pardon thing.
We already had them on the plan."

President: "But ... But how could it happen that everything was suddenly so airy, so unplanned, completely insecure?
How could the Farras suddenly show up?
Why didn't we know?
Why do they dare to do that?
I mean, they haven't said a word before, or do I know something?
I don't want this to happen anymore."

Linda: "You didn't say a word.
That won't happen again.
But our plan will work.
The head of Virginia Prison is a Rangers colonel.
He was already scheduled for LBI anyway.
So is the NSA director.
They're scheduled for the same area.
That's really a coincidence.
But we can use that.

Officially, they both lose their CIA jobs, and everyone's gonna do it for us.
Then they get their camouflage in the arms industry.
Two different companies, I think one was Boeing.
No one will be surprised that this happens.
They'll like the new job at LBI, you can bet on that.
They get more powers and less control.
Everything's finally really classified again."

President: "That sounds good.
I still don't like the risk in this case, those ad hoc decisions in twenty minutes.
I was on one of those European talk shows last time.
The LBI is too important to risk that much.
We're gonna need it more and more.
NSA's burned.
The public knows too much about her.
We need something that's really all ours again.
Where we can act freely.
Where millions of people don't look at every little move.
And we spend most of our time keeping the important things under the covers."

Linda: "The good thing is that with Marlene Farras the public in Europe will relieve us of the many changes in the NSA and the CIA.
This gives us the freedom to kick out a large number of people now and make them free for the LBI.
There are certainly some foreign secret services biting on and enticing people away, which we officially drop.
Then we get LBI in FSB or LBI in Mossad or better, the Chinese..."

President: "Yes.
I might.
But we can't make any mistakes now."

Linda: "Don't worry.
Marlene is flying back to Berlin tomorrow with her brother.
The two become icons for the conversion of CIA and NSA to positive.
You'll see them both.
It gives people hope.
And that gives us room to maneuver."


